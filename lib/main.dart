import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/add_to_cart_animation/add_to_cart_animation.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/utils/firebase_options/firebase_options.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/delete_account_bloc/delete_account_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/forget_password_bloc/forget_password_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/login_bloc/login_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/mange_account_bloc/manage_account_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/profile_bloc/profile_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/register_bloc/register_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/update_password_bloc/update_password_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/update_profile_bloc/update_profile_bloc.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:jasmingmbh/features/home_category/presentation/bloc/category_bloc/category_bloc.dart';
import 'package:jasmingmbh/features/home_category/presentation/pages/home_tabs.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';
import 'package:jasmingmbh/features/notification/firebase_api.dart';
import 'package:jasmingmbh/features/notification/presentation/bloc/notification_bloc.dart';
import 'package:jasmingmbh/features/offers/presentation/bloc/offer_bloc.dart';
import 'package:jasmingmbh/features/order/presentation/bloc/cart_ids_bloc/cart_ids_bloc.dart';
import 'package:jasmingmbh/features/order/presentation/bloc/order_bloc.dart';
import 'package:jasmingmbh/features/our_team/presentation/bloc/employees_bloc/employees_bloc.dart';
import 'package:jasmingmbh/features/products/presentation/bloc/products_bloc/products_bloc.dart';
import 'package:jasmingmbh/features/search/presentation/bloc/search_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:jasmingmbh/features/terms_of_service/presentation/bloc/privacy_bloc.dart';
import 'package:jasmingmbh/features/update_app/presentation/bloc/update_app_bloc.dart';
import 'package:jasmingmbh/features/update_app/presentation/pages/update_app_page.dart';
import 'package:jasmingmbh/simple_bloc_observer.dart';
import 'package:jasmingmbh/splash_page.dart';
import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = const SimpleBlocObserver();
  await LocalStorageService.getInstance();
  await di.init();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  if (LocalStorageService().isLoggedIn == true) {
    await FirebaseApi().initNotifications();
  }
  runApp(const MyApp());
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
final GlobalKey<CartIconKey> cartKey = GlobalKey<CartIconKey>();

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => LocalCubit()..getSavedLanguage(),
        ),
        BlocProvider(
          create: (_) => CartBloc(),
        ),
        BlocProvider(create: (_) => di.sl<LoginBloc>()),
        BlocProvider(create: (_) => di.sl<CategoryBloc>()),
        BlocProvider(create: (_) => di.sl<ProductsBloc>()),
        BlocProvider(create: (_) => di.sl<EmployeesBloc>()),
        BlocProvider(create: (_) => di.sl<ProfileBloc>()),
        BlocProvider(create: (_) => di.sl<RegisterBloc>()),
        BlocProvider(create: (_) => di.sl<OrderBloc>()),
        BlocProvider(create: (_) => di.sl<OfferBloc>()),
        BlocProvider(create: (_) => di.sl<NotificationBloc>()),
        BlocProvider(create: (_) => di.sl<SearchBloc>()),
        BlocProvider(create: (_) => di.sl<UpdatePasswordBloc>()),
        BlocProvider(create: (_) => di.sl<UpdateProfileBloc>()),
        BlocProvider(create: (_) => di.sl<DeleteAccountBloc>()),
        BlocProvider(create: (_) => di.sl<ManageAccountBloc>()),
        BlocProvider(create: (_) => di.sl<ForgetPasswordBloc>()),
        BlocProvider(create: (_) => di.sl<UpdateAppBloc>()),
        BlocProvider(create: (_) => di.sl<PrivacyBloc>()),
        BlocProvider(create: (_) => di.sl<CartIdsBloc>()),
        BlocProvider(create: (_) => di.sl<CartBloc>()),
      ],
      child: BlocBuilder<LocalCubit, LocalState>(
        builder: (context, state) {
          if (state is ChangeLocaleState) {
            return MaterialApp(
              locale: state.locale,
              localizationsDelegates: const [
                AppLocalizations.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale("de"),
                Locale("ar"),
                Locale('en')
              ],
              localeResolutionCallback: (deviceLocale, supportedLocales) {
                for (var local in supportedLocales) {
                  if (deviceLocale != null &&
                      deviceLocale.languageCode == local.languageCode) {
                    return deviceLocale;
                  }
                }
                return supportedLocales.first;
              },
              theme: ThemeData(
                  fontFamily: state.locale.languageCode == 'ar'
                      ? 'Cairo'
                      : 'Open Sans'),
              debugShowCheckedModeBanner: false,
              title: 'Jasmin App',
              navigatorKey: navigatorKey,
              home: LocalStorageService().isLoggedIn != null &&
                      LocalStorageService().isLoggedIn == true
                  ? const UpdateAppPage(child: HomeTabs())
                  : const SplashPage(),
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
