import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:jasmingmbh/features/auth/data/datasources/delete_account_data_sources.dart';
import 'package:jasmingmbh/features/auth/data/datasources/forget_password_data_sources.dart';
import 'package:jasmingmbh/features/auth/data/datasources/login_datasources/login_remote_data_source.dart';
import 'package:jasmingmbh/features/auth/data/datasources/login_datasources/user_local_data_source.dart';
import 'package:jasmingmbh/features/auth/data/datasources/profile_remote_data_sources.dart';
import 'package:jasmingmbh/features/auth/data/datasources/register_remote_data_sources.dart';
import 'package:jasmingmbh/features/auth/data/datasources/update_password_data_source.dart';
import 'package:jasmingmbh/features/auth/data/datasources/update_profile_data_sources.dart';
import 'package:jasmingmbh/features/auth/data/repositories/delete_account_repository_impl.dart';
import 'package:jasmingmbh/features/auth/data/repositories/forget_password_repository_impl.dart';
import 'package:jasmingmbh/features/auth/data/repositories/login_repository_impl.dart';
import 'package:jasmingmbh/features/auth/data/repositories/profile_repository_impl.dart';
import 'package:jasmingmbh/features/auth/data/repositories/register_repository_imol.dart';
import 'package:jasmingmbh/features/auth/data/repositories/update_password_repository_impl.dart';
import 'package:jasmingmbh/features/auth/data/repositories/update_profile_repository_impl.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/delete_account_repositories.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/forget_password_repositories.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/login_repositories.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/profile_repositories.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/register_repositories.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/update_password_repositories.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/update_profile_repositories.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/delete_account_usecases.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/forget_password.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/login_usecases.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/profile_usecases.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/register_usecases.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/update_password_usecase.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/update_profile_usecases.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/delete_account_bloc/delete_account_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/forget_password_bloc/forget_password_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/login_bloc/login_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/mange_account_bloc/manage_account_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/profile_bloc/profile_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/register_bloc/register_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/update_password_bloc/update_password_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/update_profile_bloc/update_profile_bloc.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:jasmingmbh/features/home_category/data/datasources/category_remote_data_source.dart';
import 'package:jasmingmbh/features/home_category/data/repositories/category_repository_impl.dart';
import 'package:jasmingmbh/features/home_category/domain/repositories/category_repositories.dart';
import 'package:jasmingmbh/features/home_category/domain/usecases/category_usecases.dart';
import 'package:jasmingmbh/features/home_category/presentation/bloc/category_bloc/category_bloc.dart';
import 'package:jasmingmbh/features/notification/data/datasources/notification_remote_data_source.dart';
import 'package:jasmingmbh/features/notification/data/repositories/notification_repository_impl.dart';
import 'package:jasmingmbh/features/notification/domain/repositories/notification_repository.dart';
import 'package:jasmingmbh/features/notification/domain/usecases/notification_usecase.dart';
import 'package:jasmingmbh/features/notification/presentation/bloc/notification_bloc.dart';
import 'package:jasmingmbh/features/offers/data/datasources/offers_remote_data_source.dart';
import 'package:jasmingmbh/features/offers/data/repositories/offer_repository_impl.dart';
import 'package:jasmingmbh/features/offers/domain/repositories/offer_repositories.dart';
import 'package:jasmingmbh/features/offers/domain/usecases/offer_usecases.dart';
import 'package:jasmingmbh/features/offers/presentation/bloc/offer_bloc.dart';
import 'package:jasmingmbh/features/order/data/datasources/cart_ids_remote_data_source.dart';
import 'package:jasmingmbh/features/order/data/datasources/order_remote_data_sources.dart';
import 'package:jasmingmbh/features/order/data/repositories/cart_ids_repository_impl.dart';
import 'package:jasmingmbh/features/order/data/repositories/order_repository_impl.dart';
import 'package:jasmingmbh/features/order/domain/repositories/cart_ids_repository.dart';
import 'package:jasmingmbh/features/order/domain/repositories/order_repositories.dart';
import 'package:jasmingmbh/features/order/domain/usecases/cart_ids_usecases.dart';
import 'package:jasmingmbh/features/order/domain/usecases/order_usecases.dart';
import 'package:jasmingmbh/features/order/presentation/bloc/cart_ids_bloc/cart_ids_bloc.dart';
import 'package:jasmingmbh/features/order/presentation/bloc/order_bloc.dart';
import 'package:jasmingmbh/features/our_team/data/datasources/employees_remote_data_sources.dart';
import 'package:jasmingmbh/features/our_team/data/repositories/employees_repository_impl.dart';
import 'package:jasmingmbh/features/our_team/domain/repositories/employee_repositories.dart';
import 'package:jasmingmbh/features/our_team/domain/usecases/employees_usecases.dart';
import 'package:jasmingmbh/features/our_team/presentation/bloc/employees_bloc/employees_bloc.dart';
import 'package:jasmingmbh/features/products/data/datasources/products_remore_data_sources.dart';
import 'package:jasmingmbh/features/products/data/repositories/products_repository_impl.dart';
import 'package:jasmingmbh/features/products/domain/repositories/products_repositories.dart';
import 'package:jasmingmbh/features/products/domain/usecases/products_usecases.dart';
import 'package:jasmingmbh/features/products/presentation/bloc/products_bloc/products_bloc.dart';
import 'package:jasmingmbh/features/search/data/datasources/search_remote_data_source.dart';
import 'package:jasmingmbh/features/search/data/repositories/search_repository_impl.dart';
import 'package:jasmingmbh/features/search/domain/repositories/search_repositories.dart';
import 'package:jasmingmbh/features/search/domain/usecases/search_usecase.dart';
import 'package:jasmingmbh/features/search/presentation/bloc/search_bloc.dart';
import 'package:jasmingmbh/features/terms_of_service/data/datasources/privacy_remote_data_sources.dart';
import 'package:jasmingmbh/features/terms_of_service/data/repositories/privacy_repository_impl.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/repositories/privacy_repositories.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/usecases/privacy_usecases.dart';
import 'package:jasmingmbh/features/terms_of_service/presentation/bloc/privacy_bloc.dart';
import 'package:jasmingmbh/features/update_app/data/datasources/update_app_data_sources.dart';
import 'package:jasmingmbh/features/update_app/data/repositories/update_app_repository_impl.dart';
import 'package:jasmingmbh/features/update_app/domain/repository/update_app_repositories.dart';
import 'package:jasmingmbh/features/update_app/domain/usecases/update_app_usecases.dart';
import 'package:jasmingmbh/features/update_app/presentation/bloc/update_app_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'core/network/network_info.dart';

final sl = GetIt.instance;

Future<void> init() async {
//! Features - login

// Bloc

  sl.registerFactory(() => LoginBloc(loginUseCase: sl()));

  sl.registerFactory(() => CategoryBloc(categoryUseCase: sl()));

  sl.registerFactory(() => ProductsBloc(productsUseCase: sl()));

  sl.registerFactory(() => EmployeesBloc(employeesUseCase: sl()));

  sl.registerFactory(() => ProfileBloc(profileUseCase: sl()));

  sl.registerFactory(() => RegisterBloc(registerUseCase: sl()));

  sl.registerFactory(() => OrderBloc(orderUseCase: sl()));

  sl.registerFactory(() => OfferBloc(offerUseCase: sl()));

  sl.registerFactory(() => NotificationBloc(notificationUseCase: sl()));

  sl.registerFactory(() => SearchBloc(searchUseCase: sl()));

  sl.registerFactory(() => UpdatePasswordBloc(updatePasswordUseCase: sl()));

  sl.registerFactory(() => UpdateProfileBloc(updateProfileUseCases: sl()));

  sl.registerFactory(() => DeleteAccountBloc(deleteAccountUseCase: sl()));

  sl.registerFactory(() => ManageAccountBloc(
      deleteAccountUseCase: sl(), updateProfileUseCases: sl()));

  sl.registerFactory(() => ForgetPasswordBloc(forgetPasswordUseCase: sl()));

  sl.registerFactory(() => UpdateAppBloc(updateAppUseCase: sl()));

  sl.registerFactory(() => PrivacyBloc(privacyUseCase: sl()));

  sl.registerFactory(() => CartIdsBloc(cartIdsUseCase: sl()));

  sl.registerFactory(() => CartBloc());

// Usecases

  sl.registerLazySingleton(() => LoginUseCase(sl()));

  sl.registerLazySingleton(() => CategoryUseCase(sl()));

  sl.registerLazySingleton(() => ProductsUseCase(sl()));

  sl.registerLazySingleton(() => EmployeesUseCase(sl()));

  sl.registerLazySingleton(() => ProfileUseCase(sl()));

  sl.registerLazySingleton(() => RegisterUseCase(sl()));

  sl.registerLazySingleton(() => OrderUseCase(sl()));

  sl.registerLazySingleton(() => OfferUseCase(sl()));

  sl.registerLazySingleton(() => NotificationUseCase(sl()));

  sl.registerLazySingleton(() => SearchUseCase(sl()));

  sl.registerLazySingleton(() => UpdatePasswordUseCase(sl()));

  sl.registerLazySingleton(() => UpdateProfileUseCases(sl()));

  sl.registerLazySingleton(() => DeleteAccountUseCase(sl()));

  sl.registerLazySingleton(() => ForgetPasswordUseCase(sl()));

  sl.registerLazySingleton(() => UpdateAppUseCase(sl()));

  sl.registerLazySingleton(() => PrivacyUseCase(sl()));

  sl.registerLazySingleton(() => CartIdsUseCase(sl()));

// Repository

  sl.registerLazySingleton<LoginRepository>(() => LoginRepositoryImpl(
      loginRemoteDataSource: sl(),
      userLocalDataSource: sl(),
      networkInfo: sl()));

  sl.registerLazySingleton<CategoryRepository>(() => CategoryRepositoryImpl(
      categoryRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<ProductsRepository>(() => ProductsRepositoryImpl(
      productsRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<EmployeesRepository>(() => EmployeesRepositoryImpl(
      employeesRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<ProfileRepository>(() => ProfileRepositoryImpl(
      profileRemoteDataSource: sl(),
      userLocalDataSource: sl(),
      networkInfo: sl()));

  sl.registerLazySingleton<RegisterRepository>(() => RegisterRepositoryImpl(
      registerRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<OrderRepository>(() =>
      OrderRepositoryImpl(orderRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<OfferRepository>(() =>
      OfferRepositoryImpl(offersRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<NotificationRepository>(() =>
      NotificationRepositoryImpl(
          notificationRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<SearchRepository>(() =>
      SearchRepositoryImpl(searchRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<UpdatePasswordRepository>(() =>
      UpdatePasswordRepositoryImpl(
          updatePasswordRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<UpdateProfileRepository>(() =>
      UpdateProfileRepositoryImpl(
          updateProfileDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<DeleteAccountRepository>(() =>
      DeleteAccountRepositoryImpl(
          deleteAccountRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<ForgetPasswordRepository>(() =>
      ForgetPasswordRepositoryImpl(
          forgetPasswordRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<UpdateAppRepository>(() => UpdateAppRepositoryImpl(
      updateAppRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<PrivacyRepository>(() =>
      PrivacyRepositoryImpl(privacyRemoteDataSource: sl(), networkInfo: sl()));

  sl.registerLazySingleton<CartIdsRepository>(() =>
      CartIdsRepositoryImpl(cartIdsRemoteDataSource: sl(), networkInfo: sl()));

// Datasources

  sl.registerLazySingleton<LoginRemoteDataSource>(
      () => LoginRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<CategoryRemoteDataSource>(
      () => CategoryRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<ProductsRemoteDataSource>(
      () => ProductsRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<EmployeesRemoteDataSource>(
      () => EmployeesRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<ProfileRemoteDataSource>(
      () => ProfileRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<RegisterRemoteDataSource>(
      () => RegisterRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<OrderRemoteDataSource>(
      () => OrderRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<OffersRemoteDataSource>(
      () => OffersRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<NotificationRemoteDataSource>(
      () => NotificationRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<SearchRemoteDataSource>(
      () => SearchRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<UpdatePasswordRemoteDataSource>(
      () => UpdatePasswordRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<UpdateProfileDataSource>(
      () => UpdateProfileDataSourceImpl(client: sl()));

  sl.registerLazySingleton<DeleteAccountRemoteDataSource>(
      () => DeleteAccountRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<UserLocalDataSource>(
      () => UserLocalDataSourceImpl(sharedPreferences: sl()));

  sl.registerLazySingleton<ForgetPasswordRemoteDataSource>(
      () => ForgetPasswordRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<UpdateAppRemoteDataSource>(
      () => UpdateAppRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<PrivacyRemoteDataSource>(
      () => PrivacyRemoteDataSourceImpl(client: sl()));

  sl.registerLazySingleton<CartIdsRemoteDataSource>(
      () => CartIdsRemoteDataSourceImpl(client: sl()));

//! Core

  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

//! External

  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
