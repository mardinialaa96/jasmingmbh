import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static late LocalStorageService _instance;
  static late SharedPreferences _preferences;

  final String _isLoggedIN = "isLoggedIN";
  final String _accessToken = "accessToken";
  final String _refreshToken = "refreshToken";
  final String _userId = "userId";
  final String _locale = "LOCALE";
  final String _cartItems = "cartItems";
  final String _totalPrice = "totalPrice";
  final String _firstName = "firstName";
  final String _lastName = "lastName";

  static void _applyPatch(SharedPreferences preferences) {
    if (preferences.getBool("isLoggedIN") == null) {
      preferences.setBool("isLoggedIN", false);
    }
  }

  static Future<LocalStorageService> getInstance() async {
    _instance = LocalStorageService();
    _preferences = await SharedPreferences.getInstance();
    _applyPatch(_preferences);
    return _instance;
  }

  bool? get isLoggedIn => _preferences.getBool(_isLoggedIN);
  String? get token => _preferences.getString(_accessToken);
  String? get refreshToken => _preferences.getString(_refreshToken);
  String? get userId => _preferences.getString(_userId);
  String? get currentLang => _preferences.getString(_locale);
  List<String>? get getCartItems => _preferences.getStringList(_cartItems);
  double? get getTotalPrice => _preferences.getDouble(_totalPrice);
  String? get firstName => _preferences.getString(_firstName);
  String? get lastName => _preferences.getString(_lastName);

  set isLoggedIn(bool? value) {
    if (value == null) {
      _preferences.remove(_isLoggedIN);
    } else {
      _preferences.setBool(_isLoggedIN, value);
    }
  }

  set isToken(String? value) {
    if (value == null) {
      _preferences.remove(_accessToken);
    } else {
      _preferences.setString(_accessToken, value);
    }
  }
  set isFirstName(String? value) {
    if (value == null) {
      _preferences.remove(_firstName);
    } else {
      _preferences.setString(_firstName, value);
    }
  }

  set isLastName(String? value) {
    if (value == null) {
      _preferences.remove(_lastName);
    } else {
      _preferences.setString(_lastName, value);
    }
  }

  set isCurrentLang(String? value) {
    if (value == null) {
      _preferences.remove(_locale);
    } else {
      _preferences.setString(_locale, value);
    }
  }

  set isRefreshToken(String? value) {
    if (value == null) {
      _preferences.remove(_refreshToken);
    } else {
      _preferences.setString(_refreshToken, value);
    }
  }

  set isUserId(String? value) {
    if (value == null) {
      _preferences.remove(_userId);
    } else {
      _preferences.setString(_userId, value);
    }
  }

  set isCartItems(List<String>? value) {
    if (value == null) {
      _preferences.remove(_cartItems);
    } else {
      _preferences.setStringList(_cartItems, value);
    }
  }

  set isTotalPrice(double? value) {
    if (value == null) {
      _preferences.remove(_totalPrice);
    } else {
      _preferences.setDouble(_totalPrice, value);
    }
  }
}
