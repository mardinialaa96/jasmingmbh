import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';

class AppTextStyle {
  static TextStyle titleExtraBoldDark = const TextStyle(
      fontSize: 25,
      fontWeight: FontWeight.w700,
      color: AppColors.blackColor,
      fontFamily: 'Cairo');

  static TextStyle titleDark = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      color: AppColors.blackColor,
      fontFamily: 'Cairo');

  static TextStyle titleBoldDark = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w700,
      color: AppColors.blackColor,
      fontFamily: 'Cairo');

  static TextStyle subtitle = const TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w400,
      color: AppColors.grayColor,
      fontFamily: 'Cairo');

  static TextStyle subtitleBold = const TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w700,
      color: AppColors.grayColor,
      fontFamily: 'Cairo');

  static TextStyle offerBold = const TextStyle(
      fontSize: 13,
      fontWeight: FontWeight.w700,
      color: AppColors.grayColor,
      fontFamily: 'Cairo');

  static TextStyle buttonText = const TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w700,
      color: AppColors.whiteColor,
      fontFamily: 'Cairo');

  static TextStyle subtitleDarkBold = const TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w700,
      color: AppColors.blackColor,
      fontFamily: 'Cairo');

  static TextStyle subtitleGrayBold = const TextStyle(
      fontSize: 15,
      fontWeight: FontWeight.w700,
      color: AppColors.grayColor,
      fontFamily: 'Cairo');

  static TextStyle searchTextFieldHint = const TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w700,
      color: AppColors.searchHintTextField,
      fontFamily: 'Cairo');

  static TextStyle tabBarTextStyle = const TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w700,
      color: AppColors.tabBarBackgroundColor,
      fontFamily: 'Cairo');

  static TextStyle discountTextStyle = const TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w700,
      color: AppColors.redColor,
      fontFamily: 'Cairo');

  static TextStyle subtitleLightTextStyle = const TextStyle(
      fontSize: 13,
      fontWeight: FontWeight.w700,
      color: AppColors.whiteColor,
      fontFamily: 'Cairo');

  static TextStyle cartText = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w700,
      color: AppColors.whiteColor,
      fontFamily: 'Cairo');

  static TextStyle drawerText = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w700,
      color: AppColors.drawerTextColor,
      fontFamily: 'Cairo');

  static TextStyle notificationTitleText = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w700,
      color: AppColors.secondColor,
      fontFamily: 'Cairo');
  static TextStyle notificationSubText = const TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w700,
      color: AppColors.drawerTextColor,
      fontFamily: 'Cairo');
  static TextStyle notificationDateText = const TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.w700,
      color: AppColors.dateColor,
      fontFamily: 'Cairo');

  static TextStyle miniText = const TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.w700,
      color: AppColors.whiteColor,
      fontFamily: 'Cairo');
}
