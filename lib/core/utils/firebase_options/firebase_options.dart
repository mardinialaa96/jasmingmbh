import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      throw UnsupportedError(
        'DefaultFirebaseOptions have not been configured for web - '
        'you can reconfigure this by running the FlutterFire CLI again.',
      );
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions android = FirebaseOptions(
      apiKey: 'AIzaSyC288cjZI6VFXGIS1swNd-qeODh8rlxWq4',
      appId: '1:622773056545:android:211cc36822472f5a81df60',
      messagingSenderId: '622773056545',
      projectId: 'vision-sales',
      storageBucket: 'vision-sales.appspot.com');

  static const FirebaseOptions ios = FirebaseOptions(
      apiKey: 'AIzaSyBf7TT7Fz0fmBrI8jEkWC0eCwrcteb0bgk',
      appId: '1:622773056545:ios:7ba3713f7692f1cf81df60',
      messagingSenderId: '622773056545',
      projectId: 'vision-sales',
      storageBucket: 'vision-sales.appspot.com',
      iosBundleId: 'com.vision.jasmingmbh');
}
