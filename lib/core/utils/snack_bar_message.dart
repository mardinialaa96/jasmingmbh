import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_text_style.dart';

class SnackBarMessage {
  void showSuccessSnackBarMessage(
      {required String message, required BuildContext context}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        style: AppTextStyle.cartText,
      ),
      backgroundColor: AppColors.appColor,
    ));
  }

  void showErrorSnackBarMessage(
      {required String message, required BuildContext context}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(
        message,
        style: AppTextStyle.cartText,
      ),
      backgroundColor: AppColors.redColor,
    ));
  }
}
