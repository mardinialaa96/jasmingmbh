import 'package:universal_io/io.dart' show Platform;

import 'package:device_info_plus/device_info_plus.dart';

class Agent {
  Future<String> agents() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      return ('${androidInfo.model}  ${androidInfo.version.baseOS}');
    }
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      return ('${iosInfo.utsname.machine} ${iosInfo.utsname.version} ');
    }

    return '';
  }
}
