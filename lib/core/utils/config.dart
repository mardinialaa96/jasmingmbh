import 'package:intl/intl.dart';
class Config {
  static NumberFormat oCcy = NumberFormat("#,###", "en_US");

  static String formatPrice(num price) {
    final formatter = NumberFormat("#,##0.0000", "en_US");

    String formattedPrice = formatter.format(price);

    formattedPrice = formattedPrice.replaceAll(RegExp(r'0*$'), '');

    formattedPrice = formattedPrice.replaceAll(RegExp(r'\.$'), '');

    return formattedPrice;
  }

  static int daysBetween(DateTime dateNow, DateTime endDate) {
    dateNow = DateTime(dateNow.year, dateNow.month, dateNow.day);
    endDate = DateTime(endDate.year, endDate.month, endDate.day);
    return (endDate.difference(dateNow).inHours / 24).round();
  }

  static bool daysAfter(DateTime dateNow, DateTime startDate) {
    dateNow = DateTime(dateNow.year, dateNow.month, dateNow.day);
    startDate = DateTime(startDate.year, startDate.month, startDate.day);
    return startDate.isAfter(dateNow);
  }

}