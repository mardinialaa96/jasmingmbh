class Assets {
  //images
  static String logo = 'assets/images/logo.png';

  static String pizza = 'assets/images/pizza.png';

  static String person = 'assets/images/person.png';

  static String appIcon = 'assets/images/app_icon.jpg';

  static String blockUser = 'assets/images/block_user.png';

  //icons
  static String cart = 'assets/icons/cart.png';

  static String cartText = 'assets/icons/cartText.png';

  static String chat = 'assets/icons/chat.png';

  static String home = 'assets/icons/home.png';

  static String offers = 'assets/icons/offers.png';

  static String user = 'assets/icons/user.png';

  //lottie
  static const String sadFaceLottie = "assets/lottie/sad-face.json";

  static const String emptyCartLottie = "assets/lottie/empty_cart.json";

  static const String emptyCartGif = "assets/lottie/empty_cart_gif.gif";

  static const String noItemFoundLottie = "assets/lottie/noItemFound.json";

  static const String emptyBoxLottie = "assets/lottie/empty-box.json";

  static const String wifiErrorLottie = "assets/lottie/wifierror.json";

  static const String downloadUpdate = 'assets/lottie/downloadUpdate.json';
}
