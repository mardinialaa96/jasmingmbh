import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_text_style.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField(
      {super.key,
      this.controller,
      this.label = '',
      this.action = TextInputAction.next,
      this.type = TextInputType.text,
      this.validator,
      this.value,
      this.hint = '',
      this.width = double.infinity,
      this.height,
      this.initValue,
      this.isObscureText = false,
      this.fontSize = 10,
      this.borderRadius = 5,
      this.icon,
      this.minLines = 1,
      this.maxLines = 1,
      this.textInputFormatter,
      this.readOnly = false,
      this.onSaved,
      this.focusNode});

  final Widget? icon;
  final double? height;
  final double fontSize;
  final double borderRadius;
  final String label;
  final double width;
  final TextInputAction action;
  final TextInputType type;
  final String hint;
  final TextEditingController? controller;
  final String? value;
  final String? initValue;
  final String? Function(String?)? validator;
  final bool isObscureText;
  final int maxLines;
  final int minLines;
  final List<TextInputFormatter>? textInputFormatter;
  final bool readOnly;
  final FormFieldSetter<String>? onSaved;
  final FocusNode? focusNode;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      initialValue: initValue,
      onSaved: onSaved,
      obscureText: isObscureText,
      validator: validator,
      maxLines: maxLines,
      minLines: minLines,
      cursorHeight: 20,
      readOnly: readOnly,
      inputFormatters: textInputFormatter,
      controller: controller,
      keyboardType: type,
      textInputAction: action,
      decoration: InputDecoration(
          border: UnderlineInputBorder(
              borderSide: const BorderSide(color: AppColors.grayColor),
              borderRadius: BorderRadius.circular(10)),
          enabledBorder: UnderlineInputBorder(
              borderSide: const BorderSide(color: AppColors.grayColor),
              borderRadius: BorderRadius.circular(10)),
          focusedBorder: UnderlineInputBorder(
              borderSide: const BorderSide(color: AppColors.grayColor),
              borderRadius: BorderRadius.circular(10)),
          contentPadding:
              const EdgeInsets.symmetric(vertical: 5.0, horizontal: 5),
          hintText: hint,
          labelText: label,
          labelStyle: AppTextStyle.subtitleBold),
    );
  }
}
