import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_text_style.dart';

class ErrorDataWidget extends StatelessWidget{
  final String text;
  final Null Function() onTap;
  const ErrorDataWidget({super.key, required this.text, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(text,
              style: AppTextStyle.subtitleBold),
          const SizedBox(height: 10),
          InkWell(
            onTap: onTap,
            child: const Icon(
              Icons.refresh,
              color: AppColors.appColor,
              size: 40,
            ),
          )
        ],
      ),
    );
  }
}