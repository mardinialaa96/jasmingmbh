import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LottieWidget extends StatelessWidget {
  final double size;
  final String lottiePath;

  const LottieWidget({super.key, required this.lottiePath, this.size = 75});

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Lottie.asset(
      lottiePath,
      width: size,
      height: size,
    ));
  }
}
