import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';

class NoInternetRetryWidget extends StatelessWidget {
  final bool isNoInternet;
  final Null Function() onTap;
  const NoInternetRetryWidget(
      {super.key, required this.isNoInternet, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (isNoInternet)
            Text('no_internet'.tr(context), style: AppTextStyle.subtitleBold),
          const SizedBox(height: 10),
          InkWell(
            onTap: onTap,
            child: const Icon(
              Icons.refresh,
              color: AppColors.appColor,
              size: 40,
            ),
          )
        ],
      ),
    );
  }
}
