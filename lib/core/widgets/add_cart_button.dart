import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/core/enum/page_type.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/main.dart';

class AddCartButton extends StatelessWidget {
  final ProductElement product;
  final GlobalKey<State<StatefulWidget>>? widgetKey;
  final Function(GlobalKey<State<StatefulWidget>> p1) runAddToCartAnimation;
  final Function(GlobalKey<State<StatefulWidget>> p1)
      runRemoveFromCartAnimation;
  final PageType pageType;
  const AddCartButton(
      {super.key,
      required this.product,
      this.widgetKey,
      required this.runAddToCartAnimation,
      required this.runRemoveFromCartAnimation,
      required this.pageType});

  @override
  Widget build(BuildContext context) {
    final cartState = context.watch<CartBloc>().state;

    bool productInCart = cartState.cartItems
        .any((element) => element.product == product.product);

    int productQuantity = productInCart
        ? cartState.cartItems
            .firstWhere((element) => element.product == product.product)
            .quantity
        : 0;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () {
                if (product.available != 'Y') {
                  return SnackBarMessage().showErrorSnackBarMessage(
                      message: 'product_unavailable_stock'.tr(context),
                      context: context);
                } else {
                  BlocProvider.of<CartBloc>(context)
                      .add(AddToCart(product: product, quantity: 1));
                  runAddToCartAnimation(widgetKey!);
                  cartKey.currentState!.runCartAnimation();
                }
              },
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                      color: product.available == 'Y'
                          ? AppColors.appColor
                          : AppColors.unAvailableBackgroundColor,
                    ),
                    color: product.available == 'Y'
                        ? AppColors.appColor
                        : AppColors.unAvailableBackgroundColor,
                    shape: BoxShape.circle),
                child: const Icon(
                  Icons.add,
                  color: AppColors.whiteColor,
                  size: 23,
                ),
              ),
            ),
            if (productQuantity != 0) const SizedBox(width: 15),
            if (productQuantity != 0)
              InkWell(
                onTap: () {
                  if (productQuantity == 1 &&
                      pageType == PageType.cart) {
                    cartKey.currentState!.runCartAnimation();
                    runRemoveFromCartAnimation(widgetKey!);
                    Future.delayed(const Duration(milliseconds: 400), () {
                      BlocProvider.of<CartBloc>(context)
                          .add(RemoveFromCart(product: product));
                    });
                  } else {
                    BlocProvider.of<CartBloc>(context)
                        .add(RemoveFromCart(product: product));
                    runRemoveFromCartAnimation(widgetKey!);
                    cartKey.currentState!.runCartAnimation();
                  }
                },
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColors.appColor),
                      color: AppColors.whiteColor,
                      shape: BoxShape.circle),
                  child: const Icon(
                    Icons.remove,
                    color: AppColors.appColor,
                    size: 23,
                  ),
                ),
              ),
          ],
        ),
        const SizedBox(height: 8),
        Center(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Center(
                child: Image.asset(
                  Assets.cartText,
                  height: 40,
                  color: product.available == 'Y'
                      ? AppColors.appColor
                      : AppColors.unAvailableBackgroundColor,
                  alignment: Alignment.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Text(
                  productQuantity == 0 ? "" : productQuantity.toString(),
                  style: AppTextStyle.cartText,
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
