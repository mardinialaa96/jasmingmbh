import 'package:url_launcher/url_launcher.dart';

class GlobalFunction{
   static launchURL() async {
    String? siteUrl = 'https://eduvision-ilearn.com/';
    try {
      print(Uri.parse(siteUrl));
      await launchUrl(Uri.parse(siteUrl));
    } catch (e) {
      print(
          'Could not open url in the browser: $e');
    }
  }

   String removeDoubleZero(String phoneNumber) {
     if (phoneNumber.contains("00")) {
       return phoneNumber.replaceAll("00", "");
     } else {
       return phoneNumber;
     }
   }

   bool isTextLong(String text, int maxLengthForMarquee) {
     return text.length > maxLengthForMarquee;
   }
}