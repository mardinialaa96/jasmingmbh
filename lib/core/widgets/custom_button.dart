import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_text_style.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function()? onPressed;
  final MaterialStateProperty<Color?>? backgroundColor;
  final EdgeInsetsGeometry? padding;
  const CustomButton(
      {super.key,
      required this.text,
      required this.onPressed,
      this.backgroundColor,
      this.padding});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor: backgroundColor ??
                MaterialStateProperty.all(AppColors.buttonBackgroundColor)),
        onPressed: onPressed,
        child: Padding(
          padding: padding ?? const EdgeInsets.symmetric(vertical: 10.0),
          child: Text(text,
              style: AppTextStyle.buttonText, textAlign: TextAlign.center),
        ));
  }
}
