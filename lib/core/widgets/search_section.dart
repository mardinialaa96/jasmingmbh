import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/features/search/presentation/pages/search_page.dart';

class SearchSection extends StatefulWidget {
  final bool isHomePage;
  const SearchSection({super.key, this.isHomePage = false});

  @override
  State<SearchSection> createState() => SearchSectionState();

}

class SearchSectionState extends State<SearchSection>{
  late TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
  }
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if(widget.isHomePage)
          IconButton(icon: const Icon(Icons.menu,color: AppColors.appColor,size: 35,), onPressed: () {
            Scaffold.of(context).openDrawer();
          }),

        Expanded(
          child: TextField(
              controller: controller,
              cursorColor: AppColors.appColor,
              onSubmitted: (s) {
                Navigator.push(context, MaterialPageRoute(builder: (context) => SearchPage(searchWord: s)));
              },
              style: AppTextStyle.subtitleDarkBold,
              decoration: InputDecoration(
                  contentPadding:
                  const EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                  prefixIcon: const Icon(
                    Icons.search,
                    color: AppColors.searchTextFieldIcon,
                  ),
                  fillColor: AppColors.searchColor,
                  filled: true,
                  hintText: "search".tr(context),
                  hintStyle: AppTextStyle.searchTextFieldHint,
                  border: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.searchColor),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  enabledBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.searchColor),
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide(color: AppColors.searchColor),
                      borderRadius: BorderRadius.all(Radius.circular(20))))),
        ),
        const SizedBox(width: 10),
        Container(
            decoration: BoxDecoration(
              color: AppColors.appColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => SearchPage(searchWord: controller.text)));
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 8.0),
                child: Icon(
                  Icons.search_sharp,
                  color: AppColors.whiteColor,
                  size: 35,
                ),
              ),
            )
        ),
      ],
    );
  }
}
