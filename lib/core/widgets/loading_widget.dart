import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(8.0),
      child: Center(
        child: SizedBox(
          height: 30,
          width: 30,
          child: CircularProgressIndicator(
            color: AppColors.appColor,
          ),
        ),
      ),
    );
  }
}
