import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/core/enum/page_type.dart';
import 'package:jasmingmbh/core/utils/config.dart';
import 'package:jasmingmbh/core/widgets/add_cart_button.dart';
import 'package:jasmingmbh/core/widgets/global_function.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:marquee/marquee.dart';

class CustomCard extends StatelessWidget {
  final PageType pageType;
  final ProductElement product;
  final GlobalKey widgetKey = GlobalKey();
  final Function(GlobalKey<State<StatefulWidget>> p1)? runAddToCartAnimation;
  final Function(GlobalKey<State<StatefulWidget>> p1)?
      runRemoveFromCartAnimation;
  final int? indexTab;
  CustomCard(
      {super.key,
      required this.pageType,
      required this.product,
      this.runAddToCartAnimation,
      this.indexTab,
      this.runRemoveFromCartAnimation});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Stack(
        alignment: TranslateX.alignment(context),
        children: [
          SizedBox(
            height: pageType == PageType.cart
                ? 180
                : product.offerCount != null && product.offerCount! > 0
                    ? 180
                    : 160,
            child: Card(
              elevation: 8,
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              margin: TranslateX.isRtlLanguage(context)
                  ? const EdgeInsets.only(right: 55)
                  : const EdgeInsets.only(left: 55),
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Container(
                      color: AppColors.whiteColor,
                      child: Padding(
                        padding: TranslateX.isRtlLanguage(context)
                            ? const EdgeInsets.only(
                                right: 60, left: 5, top: 10, bottom: 10)
                            : const EdgeInsets.only(
                                left: 60, right: 5, top: 10, bottom: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: pageType == PageType.offer ||
                                      pageType == PageType.cart
                                  ? MainAxisAlignment.start
                                  : MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: GlobalFunction().isTextLong(product.name ?? "", 20)
                                      ? Padding(
                                          padding: TranslateX.isRtlLanguage(
                                                  context)
                                              ? const EdgeInsets.only(left: 8.0)
                                              : const EdgeInsets.only(
                                                  right: 8.0),
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            height: 20,
                                            child: Marquee(
                                                text: product.name ?? "",
                                                style: product.available == 'Y'
                                                    ? AppTextStyle.titleBoldDark
                                                    : AppTextStyle.titleBoldDark
                                                        .copyWith(
                                                            color: AppColors
                                                                .unAvailableBackgroundColor),
                                                pauseAfterRound:
                                                    const Duration(seconds: 1),
                                                blankSpace: 5.0),
                                          ),
                                        )
                                      : Text(
                                          product.name ?? '',
                                          style: product.available == 'Y'
                                              ? AppTextStyle.titleBoldDark
                                              : AppTextStyle.titleBoldDark.copyWith(
                                                  color: AppColors
                                                      .unAvailableBackgroundColor),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                ),
                                if (pageType == PageType.product)
                                  Padding(
                                    padding: TranslateX.isRtlLanguage(context)
                                        ? const EdgeInsets.only(
                                            left: 10.0, top: 5)
                                        : const EdgeInsets.only(
                                            right: 10.0, top: 5),
                                    child: Center(
                                      child: Transform.flip(
                                        flipX: TranslateX.isRtlLanguage(context)
                                            ? true
                                            : false,
                                        child: Image.asset(
                                          Assets.offers,
                                          height: 33,
                                          color: product.offerCount != null &&
                                                  product.offerCount! > 0
                                              ? product.available == 'Y'
                                                  ? AppColors.appColor
                                                  : AppColors
                                                      .unAvailableBackgroundColor
                                              : Colors.transparent,
                                          alignment: Alignment.center,
                                        ),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                            if (pageType != PageType.product)
                              const SizedBox(height: 5),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      if (pageType == PageType.offer)
                                        const SizedBox(height: 5.0),
                                      product.offerCount != null &&
                                              product.offerCount! > 0 &&
                                              pageType != PageType.cart &&
                                              pageType != PageType.product &&
                                              (Config.daysAfter(
                                                      DateTime.now(),
                                                      product.offer
                                                              ?.startDate ??
                                                          DateTime.now()) ==
                                                  false)
                                          ? Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                product.taxIncluded == 'Y' &&
                                                        product.taxValue != 0
                                                    ? Container(
                                                        decoration:
                                                            BoxDecoration(
                                                                borderRadius:
                                                                    const BorderRadius
                                                                        .all(
                                                                  Radius
                                                                      .circular(
                                                                          10),
                                                                ),
                                                                color: product
                                                                            .available ==
                                                                        'Y'
                                                                    ? product.taxIncluded ==
                                                                            'Y'
                                                                        ? AppColors
                                                                            .redColor
                                                                        : AppColors
                                                                            .greenColor
                                                                    : AppColors
                                                                        .unAvailableBackgroundColor),
                                                        child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(4.0),
                                                            child: Text(
                                                              "${'fee'.tr(context)} ${product.taxValue}%",
                                                              style: AppTextStyle
                                                                  .subtitleLightTextStyle,
                                                              overflow: TextOverflow.ellipsis,
                                                              maxLines: 2,
                                                            )),
                                                      )
                                                    : Container(),
                                                Flexible(
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                          Radius.circular(10),
                                                        ),
                                                        color: product
                                                                    .available ==
                                                                'Y'
                                                            ? AppColors.appColor
                                                            : AppColors
                                                                .unAvailableBackgroundColor),
                                                    child: Padding(
                                                        padding:
                                                            const EdgeInsets.all(
                                                                4.0),
                                                        child: Config.daysBetween(
                                                                    DateTime
                                                                        .now(),
                                                                    product.offer
                                                                            ?.endDate ??
                                                                        DateTime
                                                                            .now()) ==
                                                                0
                                                            ? Text(
                                                                "last_day"
                                                                    .tr(context),
                                                                style: AppTextStyle
                                                                    .subtitleLightTextStyle,
                                                          overflow: TextOverflow.ellipsis,
                                                          maxLines: 2,
                                                              )
                                                            : Config.daysBetween(
                                                                        DateTime
                                                                            .now(),
                                                                        product.offer
                                                                                ?.endDate ??
                                                                            DateTime
                                                                                .now()) >=
                                                                    1
                                                                ? Text(
                                                                    buildText(
                                                                        context,
                                                                        product.offer
                                                                                ?.endDate ??
                                                                            DateTime
                                                                                .now()),
                                                                    style: AppTextStyle
                                                                        .subtitleLightTextStyle,
                                                          overflow: TextOverflow.ellipsis,
                                                          maxLines: 2,
                                                                  )
                                                                : Text(
                                                                    "offer_is_over"
                                                                        .tr(context),
                                                                    style: AppTextStyle
                                                                        .subtitleLightTextStyle,
                                                          overflow: TextOverflow.ellipsis,
                                                          maxLines: 2,
                                                                  )),
                                                  ),
                                                ),
                                              ],
                                            )
                                          : product.taxIncluded == 'Y' &&
                                                  product.taxValue != 0
                                              ? Container(
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .all(
                                                        Radius.circular(10),
                                                      ),
                                                      color: product
                                                                  .available ==
                                                              'Y'
                                                          ? product.taxIncluded ==
                                                                  'Y'
                                                              ? AppColors
                                                                  .redColor
                                                              : AppColors
                                                                  .greenColor
                                                          : AppColors
                                                              .unAvailableBackgroundColor),
                                                  child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              4.0),
                                                      child: Text(
                                                        "${'fee'.tr(context)} ${product.taxValue}%",
                                                        style: AppTextStyle
                                                            .subtitleLightTextStyle,
                                                      )),
                                                )
                                              : Container(),
                                      RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                          text: "1/",
                                          style: product.available == 'Y'
                                              ? AppTextStyle.subtitleGrayBold
                                              : AppTextStyle.subtitleGrayBold
                                                  .copyWith(
                                                      color: AppColors
                                                          .unAvailableBackgroundColor),
                                        ),
                                        const WidgetSpan(
                                            child: SizedBox(width: 5.0)),
                                        TextSpan(
                                          text:
                                              "${Config.formatPrice(product.price ?? 0.0)} €",
                                          style: product.available == 'Y'
                                              ? AppTextStyle.subtitleGrayBold
                                              : AppTextStyle.subtitleGrayBold
                                                  .copyWith(
                                                      color: AppColors
                                                          .unAvailableBackgroundColor),
                                        ),
                                      ])),
                                      RichText(
                                          text: TextSpan(children: [
                                        TextSpan(
                                          text: "${product.packageAmount}/",
                                          style: product.available == 'Y'
                                              ? AppTextStyle.subtitleDarkBold
                                              : AppTextStyle.subtitleDarkBold
                                                  .copyWith(
                                                      color: AppColors
                                                          .unAvailableBackgroundColor),
                                        ),
                                        const WidgetSpan(
                                            child: SizedBox(width: 5.0)),
                                        TextSpan(
                                          text: _getPrice(),
                                          style: product.available == 'Y'
                                              ? AppTextStyle.subtitleDarkBold
                                              : AppTextStyle.subtitleDarkBold
                                                  .copyWith(
                                                      color: AppColors
                                                          .unAvailableBackgroundColor),
                                        ),
                                        if (product.offer != null &&
                                            product.offer?.offer == 'percent')
                                          const WidgetSpan(
                                              child: SizedBox(width: 5.0)),
                                        if (product.offer != null &&
                                            product.offer?.offer == 'percent')
                                          TextSpan(
                                            text:
                                                "${Config.formatPrice(product.packagePrice ?? 0.0)} €",
                                            style: product.available == 'Y'
                                                ? AppTextStyle.subtitleBold
                                                    .copyWith(
                                                        decorationThickness:
                                                            1.5,
                                                        decoration:
                                                            TextDecoration
                                                                .lineThrough,
                                                        decorationColor:
                                                            AppColors.grayColor)
                                                : AppTextStyle.subtitleBold.copyWith(
                                                    color: AppColors
                                                        .unAvailableBackgroundColor,
                                                    decorationThickness: 1.5,
                                                    decoration: TextDecoration
                                                        .lineThrough,
                                                    decorationColor: AppColors
                                                        .unAvailableBackgroundColor),
                                          ),
                                      ])),
                                    ],
                                  ),
                                ),
                                if (pageType == PageType.offer)
                                  Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5.0),
                                      child: Transform.flip(
                                        flipX: TranslateX.isRtlLanguage(context)
                                            ? true
                                            : false,
                                        child: Image.asset(
                                          Assets.offers,
                                          height: 30,
                                          color: product.available == 'Y'
                                              ? AppColors.appColor
                                              : AppColors
                                                  .unAvailableBackgroundColor,
                                        ),
                                      )),
                                if (pageType == PageType.product ||
                                    pageType == PageType.cart)
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0, vertical: 10),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        AddCartButton(
                                            pageType: pageType,
                                            product: product,
                                            widgetKey: widgetKey,
                                            runAddToCartAnimation:
                                                runAddToCartAnimation!,
                                            runRemoveFromCartAnimation:
                                                runRemoveFromCartAnimation!),
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                            if (product.offer != null &&
                                pageType != PageType.cart)
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width,
                                      height: 20.0,
                                      child: GlobalFunction().isTextLong(
                                              product.offer?.offerDesc ?? "",
                                              30)
                                          ? Marquee(
                                              text: product.offer?.offerDesc ??
                                                  "",
                                              style: product.available == 'Y'
                                                  ? AppTextStyle.offerBold
                                                  : AppTextStyle.offerBold.copyWith(
                                                      color: AppColors
                                                          .unAvailableBackgroundColor),
                                              pauseAfterRound:
                                                  const Duration(seconds: 1),
                                              blankSpace: 5.0)
                                          : Text(
                                              product.offer?.offerDesc ?? "",
                                              style: product.available == 'Y'
                                                  ? AppTextStyle.offerBold
                                                  : AppTextStyle.offerBold.copyWith(
                                                      color: AppColors
                                                          .unAvailableBackgroundColor),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                    ),
                                  ),
                                ],
                              ),
                            if (pageType == PageType.cart)
                              const Divider(
                                color: AppColors.grayColor,
                              ),
                            /* if (pageType == PageType.cart)
                            Padding(
                                                            padding: TranslateX
                                                                .isRtlLanguage(
                                                                context)
                                                                ? const EdgeInsets
                                                                .only(
                                                                left: 5.0)
                                                                : const EdgeInsets
                                                                .only(
                                                                right: 5.0),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "${'total_before_tax'.tr(context)}: ",
                                                                  style: AppTextStyle
                                                                      .subtitleBold,
                                                                ),
                                                                Text(
                                                                  "€ ${state.cartItems[index].packagePrice * state.cartItems[index].quantity}",
                                                                  style: AppTextStyle
                                                                      .subtitleBold,
                                                                ),
                                                              ],
                                                            ),
                                                          ),*/
                            if (pageType == PageType.cart)
                              Padding(
                                padding: TranslateX.isRtlLanguage(context)
                                    ? const EdgeInsets.only(left: 5.0)
                                    : const EdgeInsets.only(right: 5.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "${'total_after_tax'.tr(context)}: ",
                                      style: AppTextStyle.subtitleBold,
                                    ),
                                    Text(
                                      "€ ${Config.formatPrice(product.subtotal)}",
                                      style: AppTextStyle.subtitleBold,
                                    ),
                                  ],
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              _showImageDialog(context, product.image ?? "");
            },
            child: Container(
              key: widgetKey,
              height: 110,
              child: product.available == 'Y'
                  ? CachedNetworkImage(
                      imageUrl: product.image ?? "",
                      fit: BoxFit.cover, // Adjust fit as per your requirement
                      errorWidget: (context, url, error) =>
                          const Icon(Icons.error),
                      placeholder: (context, url) => Stack(
                        alignment: TranslateX.alignment(context),
                        children: const [
                          CircularProgressIndicator(
                            color: AppColors.appColor,
                          )
                        ],
                      ),
                    )
                  : Container(
                      foregroundDecoration: const BoxDecoration(
                        color: Colors.white,
                        backgroundBlendMode: BlendMode.saturation,
                      ),
                      child: CachedNetworkImage(
                        imageUrl: product.image ?? "",
                        fit: BoxFit.cover, // Adjust fit as per your requirement
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                        placeholder: (context, url) => Stack(
                          alignment: TranslateX.alignment(context),
                          children: const [
                            CircularProgressIndicator(
                              color: AppColors.appColor,
                            )
                          ],
                        ),
                      ),
                    ),
            ),
          ),
        ],
      ),
    );
  }

  String _getPrice() {
    num originalPrice = product.packagePrice ?? 0.0;
    num discountPercentage = 0.0;
    num discountedPrice = 0.0;
    num taxWithPresentAmount = 0.0;

    if (product.offer != null &&
        product.offer?.offer == 'percent' &&
        product.offer?.offerValue != null) {
      discountPercentage = product.offer!.offerValue! / 100.0;

      discountedPrice = originalPrice - (originalPrice * discountPercentage);

      taxWithPresentAmount =
          discountedPrice * ((product.taxValue ?? 0.0) / 100);
    }

    num taxAmount = originalPrice * ((product.taxValue ?? 0.0) / 100);

    // after tax with offer >> indexTab = 1
    if (pageType == PageType.offer &&
        indexTab == 1 &&
        product.offer != null &&
        product.offer?.offer == 'percent' &&
        product.offer?.offerValue != null) {
      return "${Config.formatPrice(discountedPrice + taxWithPresentAmount)} €";
    }
    // after tax  without offer >> indexTab = 1
    else if (pageType == PageType.offer &&
        indexTab == 1 &&
        product.offer != null &&
        product.offer?.offer == 'added') {
      return "${Config.formatPrice(originalPrice + taxAmount)} €";
    } else if (product.offer != null &&
        product.offer?.offer == 'percent' &&
        product.offer?.offerValue != null) {
      return "${Config.formatPrice(discountedPrice)} €";
    } else {
      return "${Config.formatPrice(originalPrice)} €";
    }
  }

  void _showImageDialog(BuildContext context, String imageUrl) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: EdgeInsets.zero,
          backgroundColor: AppColors.appColor.withOpacity(0.1),
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              CachedNetworkImage(
                imageUrl: imageUrl,
                fit: BoxFit.contain, // Adjust fit as per your requirement
                errorWidget: (context, url, error) => const Icon(Icons.error),
                placeholder: (context, url) => const Center(
                  child: CircularProgressIndicator(
                    color: AppColors.appColor,
                  ),
                ),
              ),
              IconButton(
                icon: const Icon(
                  Icons.close,
                  color: AppColors.whiteColor,
                  size: 35,
                ),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          ),
        );
      },
    );
  }


  String buildText(BuildContext context, DateTime endDate) {
    int daysRemaining = Config.daysBetween(DateTime.now(), endDate);

    String daysString =
        daysRemaining == 1 || daysRemaining == 2 || daysRemaining > 10
            ? "day".tr(context)
            : "days".tr(context);

    String stayText = "stay".tr(context);
    String daysRemainingText = daysRemaining.toString();

    String plainText = "$stayText $daysRemainingText $daysString";

    return plainText;
  }
}
