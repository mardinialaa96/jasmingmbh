import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'lottie.dart';

class EmptyWidget extends StatelessWidget {
  final String text;
  const EmptyWidget({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Center(
            child: LottieWidget(
          lottiePath: Assets.noItemFoundLottie,
          size: 150,
        )),
        Center(
          child: Text(
            text,
            style: AppTextStyle.subtitleBold,
          ),
        ),
      ],
    );
  }
}
