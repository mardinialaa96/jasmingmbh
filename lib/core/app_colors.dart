import 'package:flutter/material.dart';

class AppColors {
  static const Color appColor = Color(0xffdaa715);

  static const Color secondColor = Color(0xffb58344);

  static const Color backgroundColor = Colors.white;

  static const Color buttonBackgroundColor = Color(0xffdaa715);

  static const Color tabBarBackgroundColor = Color(0xffdaa715);

  static const Color blackColor = Colors.black;

  static const Color grayColor = Color(0xffafafaf);

  static const Color iconColor = Color(0xffe8c569);

  static const Color whiteColor = Colors.white;

  static const Color secondaryTextColor = Color(0xffe8c569);

  static const Color selectedNavBarIcon = Colors.white;

  static const Color unSelectedNavBarIcon = Color(0xffdaa715);

  static const Color searchColor = Color(0xffe8e8e8);

  static const Color searchTextFieldIcon = Color(0xffa4a4a4);

  static const Color searchHintTextField = Color(0xffbebebe);

  static const Color redColor = Colors.red;

  static const Color greenColor = Colors.green;

  static const Color drawerBackgroundColor = Color(0xfff9f9f9);

  static const Color drawerIconColor = Color(0xff848484);

  static const Color drawerTextColor = Color(0xff7f7f7f);

  static const Color dateColor = Color(0xffd0d0d0);

  static const Color unAvailableBackgroundColor = Color(0xffeaeaea);
}
