import 'package:flutter/material.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/change_password_page.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/login_page.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/profile_page.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/signup_page_info_one.dart';
import 'package:jasmingmbh/features/home_category/presentation/pages/home_tabs.dart';
import 'package:jasmingmbh/features/notification/presentation/pages/notification_page.dart';
import 'package:jasmingmbh/features/products/presentation/pages/products_page.dart';
import 'package:jasmingmbh/splash_page.dart';
import 'package:page_transition/page_transition.dart';

class Routes {
  Routes._();

  //static variables
  static const String splash = '/splash';
  static const String signUpPageInfoOne = '/signUpPageInfoOne';
  static const String signUpPageInfoTwo = '/signUpPageInfoTwo';
  static const String login = '/login';
  static const String home = '/home';
  static const String products = '/products';
  static const String notification = '/notification';
  static const String profile = '/profile';
  static const String changePassword = '/changePassword';

  static Route? getRoutes(settings) {
    switch (settings.name) {
      case splash:
        return PageTransition(
          child: const SplashPage(),
          type: PageTransitionType.fade,
          settings: settings,
          duration: const Duration(milliseconds: 550),
        );

      case signUpPageInfoOne:
        return PageTransition(
          settings: settings,
          child: const SignUpPageInfoOne(),
          type: PageTransitionType.fade,
          duration: const Duration(milliseconds: 550),
        );

      case login:
        return PageTransition(
          settings: settings,
          child: const LogInPage(),
          type: PageTransitionType.fade,
          duration: const Duration(milliseconds: 550),
        );

      case home:
        return PageTransition(
          settings: settings,
          child: const HomeTabs(),
          type: PageTransitionType.fade,
          duration: const Duration(milliseconds: 550),
        );
      case products:
        return PageRouteBuilder(
            settings: settings,
            pageBuilder: (BuildContext context, _, __) {
              final CategoryProductArguments args = settings.arguments;
              return ProductsPage(
                categoryId: args.categoryId,
              );
            });

      case notification:
        return PageTransition(
          settings: settings,
          child: const NotificationPage(),
          type: PageTransitionType.fade,
          duration: const Duration(milliseconds: 550),
        );
      case profile:
        return PageTransition(
          settings: settings,
          child: const ProfilePage(),
          type: PageTransitionType.fade,
          duration: const Duration(milliseconds: 550),
        );
      case changePassword:
        return PageTransition(
          settings: settings,
          child: const ChangePasswordPage(),
          type: PageTransitionType.fade,
          duration: const Duration(milliseconds: 550),
        );

      default:
        return null;
    }
  }
}

class CategoryProductArguments {
  final int categoryId;
  CategoryProductArguments({required this.categoryId});
}
