import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';


class LanguageCacheHelper {
  Future<void> cacheLanguageCode(String languageCode) async {
    LocalStorageService().isCurrentLang = languageCode;
  }

  Future<String> getCachedLanguageCode() async {
    final cachedLanguageCode = LocalStorageService().currentLang;
    print('cachedLanguageCode $cachedLanguageCode');
    if (cachedLanguageCode != null) {
      return cachedLanguageCode;
    } else {
      LocalStorageService().isCurrentLang = "de";
      return "de";
    }
  }
}
