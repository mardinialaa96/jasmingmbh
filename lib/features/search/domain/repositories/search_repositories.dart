import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

abstract class SearchRepository {
  Future<Either<Failure, List<ProductElement>>> getAllSearch(
      int page, String locale, String searchWord);
}
