import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/search/domain/repositories/search_repositories.dart';

class SearchUseCase {
  final SearchRepository repository;

  SearchUseCase(this.repository);

  Future<Either<Failure, List<ProductElement>>> call(int page,String locale,String searchWord) async {
    return await repository.getAllSearch(page,locale,searchWord);
  }
}
