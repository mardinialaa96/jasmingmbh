import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/search/data/datasources/search_remote_data_source.dart';
import 'package:jasmingmbh/features/search/domain/repositories/search_repositories.dart';

class SearchRepositoryImpl implements SearchRepository {
  final SearchRemoteDataSource searchRemoteDataSource;
  final NetworkInfo networkInfo;

  SearchRepositoryImpl(
      {required this.searchRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, List<ProductElement>>> getAllSearch(
      int page, String locale, String searchWord) async {
    if (await networkInfo.isConnected) {
      try {
        final productInfo =
            await searchRemoteDataSource.getAllSearch(page, locale, searchWord);
        return Right(productInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
