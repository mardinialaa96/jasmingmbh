import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/products/data/models/product_by_category_model.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/main.dart';

abstract class SearchRemoteDataSource {
  Future<List<ProductElement>> getAllSearch(
      int pageNumber, String locale, String searchWord);
}

class SearchRemoteDataSourceImpl implements SearchRemoteDataSource {
  final http.Client client;

  SearchRemoteDataSourceImpl({required this.client});

  @override
  Future<List<ProductElement>> getAllSearch(
      int page, String locale, String searchWord) async {
    print('page $page ........ locale $locale');
    final queryParameters = {
      'page': page.toString(),
      'searchWord': searchWord,
      'locale': locale,
    };
    final token = LocalStorageService().token;
    if (token == null) {
      throw ServerException();
    }
    final url = Uri.parse('$baseUrl/search-product');
    final response = await client
        .get(url.replace(queryParameters: queryParameters), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    print('token ${LocalStorageService().token}');
    print('response.statusCode ${response.statusCode}');
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      ProductByCategoryModel productByCategoryModel =
          ProductByCategoryModel.fromJson(decodeJson);
      print('response.body $productByCategoryModel');
      print(
          'length ${productByCategoryModel.dataProductByCategoryInfo.products.length}');
      if (productByCategoryModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
            (route) => false);
        return productByCategoryModel.dataProductByCategoryInfo.products;
      } else {
        return productByCategoryModel.dataProductByCategoryInfo.products;
      }
    } else {
      throw ServerException();
    }
  }
}
