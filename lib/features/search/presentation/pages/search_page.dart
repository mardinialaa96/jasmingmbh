import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/add_to_cart_animation/add_to_cart_animation.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/enum/page_type.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_card.dart';
import 'package:jasmingmbh/core/widgets/empty_widget.dart';
import 'package:jasmingmbh/core/widgets/no_internet_retry_widget.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/search/presentation/bloc/search_bloc.dart';
import 'package:jasmingmbh/main.dart';

class SearchPage extends StatefulWidget {
  final String searchWord;
  const SearchPage({super.key, required this.searchWord});

  @override
  State<SearchPage> createState() => SearchPageState();
}

class SearchPageState extends State<SearchPage> {
  late TextEditingController controller;
  final ScrollController _scrollController = ScrollController();
  int _pageNumber = 1;
  late List<ProductElement> _products;
  bool _isLoading = false;
  bool _hasMoreData = true;
  bool _isLoadingFirst = false;
  bool _isEmptyProducts = false;
  bool _noInternet = false;
  bool _retry = false;
  late final Function(GlobalKey) runAddToCartAnimation;
  late final Function(GlobalKey) runRemoveFromCartAnimation;
  @override
  void initState() {
    super.initState();
    controller = TextEditingController(text: widget.searchWord);
    controller.text = widget.searchWord;
    _products = [];
    _fetchProducts();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !_isLoading &&
        _hasMoreData) {
      _pageNumber++;
      BlocProvider.of<SearchBloc>(context).add(LoadMoreSearchEvent(
        locale: '${LocalStorageService().currentLang}',
        page: _pageNumber,
        searchWord: controller.text,
      ));
    }
  }

  void _fetchProducts() {
    BlocProvider.of<SearchBloc>(context).add(GetAllSearchEvent(
      locale: '${LocalStorageService().currentLang}',
      page: _pageNumber,
      searchWord: controller.text,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return AddToCartAnimation(
      cartKey: cartKey,
      height: 30,
      width: 30,
      opacity: 0.85,
      jumpAnimation: const JumpAnimationOptions(
          active: false,
          curve: Curves.linear,
          duration: Duration(milliseconds: 0)),
      dragAnimation: const DragToCartAnimationOptions(
        curve: Curves.linearToEaseOut,
        duration: Duration(milliseconds: 400),
      ),
      createAddToCartAnimation: (unAddToCartAnimation) {
        runAddToCartAnimation = unAddToCartAnimation;
      },
      createRemoveFromCartAnimation: (unRemoveFromCartAnimation) {
        runRemoveFromCartAnimation = unRemoveFromCartAnimation;
      },
      child: Scaffold(
        backgroundColor: AppColors.backgroundColor,
        body: SafeArea(
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  IconButton(
                    icon: const Icon(
                      Icons.arrow_back_ios,
                      size: 20,
                    ),
                    onPressed: () => Navigator.pop(context),
                    color: AppColors.iconColor,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 12),
                      child: TextField(
                        autofocus: false,
                        style: AppTextStyle.subtitleDarkBold,
                        autocorrect: false,
                        onTap: () {
                          if (controller.selection ==
                              TextSelection.fromPosition(TextPosition(
                                  offset: controller.text.length - 1))) {
                            setState(() {
                              controller.selection = TextSelection.fromPosition(
                                  TextPosition(offset: controller.text.length));
                            });
                          }
                        },
                        onSubmitted: (s) {
                          FocusScope.of(context).unfocus();
                          _pageNumber = 1;
                          _products = [];
                          _fetchProducts();
                        },
                        controller: controller,
                        cursorColor: AppColors.appColor,
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: AppColors.backgroundColor,
                            hintText: "search".tr(context),
                            hintStyle: AppTextStyle.searchTextFieldHint,
                            contentPadding:
                                const EdgeInsets.symmetric(horizontal: 8),
                            border: const OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors.appColor, width: 1)),
                            focusedBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors.appColor, width: 1)),
                            enabledBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors.appColor, width: 1)),
                            disabledBorder: const OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColors.appColor, width: 1)),
                            suffixIcon: IconButton(
                              onPressed: () {
                                FocusScope.of(context).unfocus();
                                _pageNumber = 1;
                                _products = [];
                                _fetchProducts();
                              },
                              icon: const Icon(
                                Icons.search,
                                color: AppColors.iconColor,
                              ),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
              BlocConsumer<SearchBloc, SearchState>(
                builder: (context, state) {
                  return Expanded(
                      child: _isLoadingFirst
                          ? const Center(
                              child: CircularProgressIndicator(
                                color: AppColors.appColor,
                              ),
                            )
                          : _isEmptyProducts
                              ? EmptyWidget(
                                  text: 'products_isEmpty'.tr(context))
                              : _noInternet || _retry
                                  ? NoInternetRetryWidget(
                                      isNoInternet: _noInternet,
                                      onTap: () {
                                        _products = [];
                                        _pageNumber = 1;
                                        _fetchProducts();
                                      })
                                  : _buildCart(context));
                },
                listener: (context, state) {
                  if (state is LoadingSearchState) {
                    setState(() {
                      _isLoading = true;
                    });
                  } else if (state is LoadingInitSearchState) {
                    setState(() {
                      _isLoadingFirst = true;
                    });
                  } else if (state is EmptySearchState) {
                    setState(() {
                      _isLoadingFirst = false;
                      _isEmptyProducts = true;
                    });
                  } else if (state is LoadedSearchState) {
                    setState(() {
                      _isLoading = false;
                      _isLoadingFirst = false;
                      _isEmptyProducts = false;
                      _products.addAll(state.products);
                      _noInternet = false;
                      _retry = false;
                      _hasMoreData = state.products.isNotEmpty;
                    });
                  } else if (state is ErrorSearchState) {
                    if (state.message == offlineFailureMessage) {
                      _noInternet = true;
                      _isLoadingFirst = false;
                    } else {
                      _retry = true;
                      _isLoadingFirst = false;
                      return SnackBarMessage().showErrorSnackBarMessage(
                          message:  state.message == serverFailureMessage
                              ? 'try_again'.tr(context)
                              : state.message, context: context);
                    }
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCart(BuildContext c) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: _products.length + (_isLoading ? 1 : 0),
      padding: TranslateX.isRtlLanguage(context)
          ? const EdgeInsets.only(
              top: 20.0,
              bottom: 80,
              right: 4,
              left: 15,
            )
          : const EdgeInsets.only(
              top: 20.0,
              bottom: 80,
              right: 15,
              left: 4,
            ),
      itemBuilder: (BuildContext context, int index) {
        if (index < _products.length) {
          return CustomCard(
              product: _products[index],
              pageType: PageType.product,
              runAddToCartAnimation: runAddToCartAnimation,
              runRemoveFromCartAnimation: runRemoveFromCartAnimation);
        } else {
          if (_isLoading && _hasMoreData && !_isLoadingFirst) {
            return const Center(
              child: CircularProgressIndicator(
                color: AppColors.appColor,
              ),
            );
          } else {
            return Container();
          }
        }
      },
    );
  }
}
