part of 'search_bloc.dart';

@immutable
abstract class SearchState extends Equatable {
  const SearchState();
  @override
  List<Object?> get props => [];
}

class SearchInitial extends SearchState {
  @override
  List<Object> get props => [];
}
class LoadingSearchState extends SearchInitial {
  @override
  List<Object> get props => [];
}

class LoadingInitSearchState extends SearchState {
  @override
  List<Object> get props => [];
}

class EmptySearchState extends SearchState {
  @override
  List<Object> get props => [];
}

class LoadedSearchState extends SearchState {
  final List<ProductElement> products;

  const LoadedSearchState({required this.products});
  @override
  List<Object?> get props => [products];
}

class ErrorSearchState extends SearchState {
  final String message;

  const ErrorSearchState({required this.message});
  @override
  List<Object?> get props => [message];
}
