import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/search/domain/usecases/search_usecase.dart';
import 'package:meta/meta.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchUseCase searchUseCase;
  SearchBloc({required this.searchUseCase}) : super(SearchInitial()) {
    on<SearchEvent>((event, emit) async {
      if (event is GetAllSearchEvent) {
        emit(LoadingInitSearchState());
        try{
          final failureOrDoneMessage =
          await searchUseCase(event.page, event.locale, event.searchWord);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorSearchState(message: serverFailureMessage));
        }
      } else if (event is LoadMoreSearchEvent) {
        emit(LoadingSearchState());
        try{
          final result =
          await searchUseCase(event.page, event.locale, event.searchWord);
          result.fold((failure) {
            emit(ErrorSearchState(message: _mapFailureToMessage(failure)));
          }, (moreProducts) {
            emit(LoadedSearchState(products: moreProducts));
          });
        } catch (e) {
          emit(const ErrorSearchState(message: serverFailureMessage));
        }
      }
    });
  }

  SearchState _mapFailureOrSuccessToState(
      Either<Failure, List<ProductElement>> either) {
    return either.fold(
        (failure) => ErrorSearchState(message: _mapFailureToMessage(failure)),
        (productInfo) {
      if (productInfo.isEmpty) {
        return EmptySearchState();
      } else {
        return LoadedSearchState(products: productInfo);
      }
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
