part of 'search_bloc.dart';

@immutable
abstract class SearchEvent extends Equatable{
  const SearchEvent();
  @override
  List<Object?> get props => [];
}

class GetAllSearchEvent extends SearchEvent {
  final String locale;
  final int page;
  final String searchWord;
  const GetAllSearchEvent({required this.locale,required this.page, required this.searchWord});
  @override
  List<Object?> get props => [locale, searchWord];
}

class LoadMoreSearchEvent extends SearchEvent {
  final String locale;
  final int page;
  final String searchWord;
  const LoadMoreSearchEvent(
      {required this.locale, required this.page, required this.searchWord});

  @override
  List<Object?> get props => [locale, page, searchWord];
}
