import 'package:jasmingmbh/features/offers/data/models/offer_products_model.dart';
import 'package:jasmingmbh/features/offers/domain/entities/offer_data.dart';

class OfferDataModel extends OfferData{
  const OfferDataModel(
      {required super.message, required super.data, required super.status});
  factory OfferDataModel.fromJson(Map<String, dynamic> json) {
    return OfferDataModel(
      data: json["data"] == null ? null : OfferProductsModel.fromJson(json["data"]),
      message: json["message"],
      status: json["status"],
    );
  }
}