import 'package:jasmingmbh/features/offers/domain/entities/offer_products.dart';
import 'package:jasmingmbh/features/products/data/models/product_element_model.dart';

class OfferProductsModel extends OfferProducts {
  const OfferProductsModel({required super.productElement});

  factory OfferProductsModel.fromJson(Map<String, dynamic> json) =>
      OfferProductsModel(
        productElement: json["products"] == null
            ? []
            : List<ProductElementModel>.from(
                json["products"]!.map((x) => ProductElementModel.fromJson(x))),
      );
}
