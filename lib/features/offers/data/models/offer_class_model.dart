import 'package:jasmingmbh/features/offers/domain/entities/offer_class.dart';

class OfferClassModel extends OfferClass {
  const OfferClassModel(
      {required super.offer,
      required super.offerType,
      required super.offerDesc,
      required super.offerValue,
      required super.startDate,
      required super.endDate});
  factory OfferClassModel.fromJson(Map<String, dynamic> json) {
    return OfferClassModel(
      offer: json["offer"],
      offerType: json["offer_type"],
      offerDesc: json["offer_desc"],
      offerValue: json["offer_value"],
      startDate: json["start_date"] == null
          ? null
          : DateTime.parse(json["start_date"]),
      endDate:
          json["end_date"] == null ? null : DateTime.parse(json["end_date"]),
    );
  }
}
