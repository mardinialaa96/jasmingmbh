import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/offers/data/models/offer_data_model.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/main.dart';

abstract class OffersRemoteDataSource {
  Future<List<ProductElement>?> getAllOffers(int pageNumber, String locale);
}

class OffersRemoteDataSourceImpl implements OffersRemoteDataSource {
  final http.Client client;

  OffersRemoteDataSourceImpl({required this.client});

  @override
  Future<List<ProductElement>?> getAllOffers(int page, String locale) async {
    print('page $page ........ locale $locale');
    final queryParameters = {
      'page': page.toString(),
      'locale': locale,
    };
    final url = Uri.parse('$baseUrl/offers');
    final response = await client
        .get(url.replace(queryParameters: queryParameters), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    print('token ${LocalStorageService().token}');
    print('response.statusCode ${response.statusCode}');
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      OfferDataModel offerDataModel = OfferDataModel.fromJson(decodeJson);
      print('response.body $offerDataModel');
      print('length ${offerDataModel.data?.productElement?.length}');
      if (offerDataModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
            (route) => false);
        return offerDataModel.data?.productElement;
      } else {
        return offerDataModel.data?.productElement;
      }
    } else {
      throw ServerException();
    }
  }
}
