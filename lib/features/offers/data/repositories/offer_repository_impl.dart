import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/offers/data/datasources/offers_remote_data_source.dart';
import 'package:jasmingmbh/features/offers/domain/repositories/offer_repositories.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class OfferRepositoryImpl implements OfferRepository {
  final OffersRemoteDataSource offersRemoteDataSource;
  final NetworkInfo networkInfo;

  OfferRepositoryImpl(
      {required this.offersRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, List<ProductElement>?>> getAllOffers(
      int page, String locale) async {
    if (await networkInfo.isConnected) {
      try {
        final offerInfo =
            await offersRemoteDataSource.getAllOffers(page, locale);
        return Right(offerInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
