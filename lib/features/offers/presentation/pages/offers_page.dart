import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/enum/page_type.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_card.dart';
import 'package:jasmingmbh/core/widgets/empty_widget.dart';
import 'package:jasmingmbh/core/widgets/no_internet_retry_widget.dart';
import 'package:jasmingmbh/core/widgets/search_section.dart';
import 'package:jasmingmbh/core/widgets/shimmer_widget.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';
import 'package:jasmingmbh/features/offers/presentation/bloc/offer_bloc.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class OffersPage extends StatefulWidget {
  const OffersPage({super.key});

  @override
  State<OffersPage> createState() => OffersPageState();
}

class OffersPageState extends State<OffersPage> {
  final ScrollController _scrollController = ScrollController();
  int _pageNumber = 1;
  late List<ProductElement> _products;
  bool _isLoading = false;
  bool _hasMoreData = true;
  bool _isLoadingFirst = false;
  bool _isEmptyProducts = false;
  bool _noInternet = false;
  bool _retry = false;
  int indexTab = 0;

  @override
  void initState() {
    super.initState();
    _products = [];
    _fetchProducts();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !_isLoading &&
        _hasMoreData) {
      _pageNumber++;
      BlocProvider.of<OfferBloc>(context).add(LoadMoreOffersEvent(
          locale: '${LocalStorageService().currentLang}', page: _pageNumber));
    }
  }

  void _fetchProducts() {
    _products = [];
    _pageNumber = 1;
    BlocProvider.of<OfferBloc>(context).add(GetAllOffersEvent(
        locale: '${LocalStorageService().currentLang}', page: _pageNumber));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SafeArea(
          child: Column(
        children: [
          _buildTopSearch(),
          _buildTabBarOffers(context),
        ],
      )),
    );
  }

  Widget _buildTopSearch() {
    return const Padding(
      padding: EdgeInsets.all(10.0),
      child: SearchSection(),
    );
  }

  Widget _buildTabBarOffers(BuildContext context) {
    return Expanded(
      child: DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 15.0),
              child: SizedBox(
                height: 35,
                child: TabBar(
                    tabAlignment: TabAlignment.center,
                    onTap: (value) {
                      _fetchProducts();
                      indexTab = value;
                    },
                    overlayColor: MaterialStateProperty.all(Colors.transparent),
                    dividerColor: Colors.transparent,
                    unselectedLabelStyle: AppTextStyle.tabBarTextStyle,
                    labelStyle: AppTextStyle.tabBarTextStyle
                        .copyWith(color: AppColors.whiteColor),
                    indicatorSize: TabBarIndicatorSize.label,
                    indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: AppColors.tabBarBackgroundColor),
                    labelPadding: const EdgeInsets.symmetric(horizontal: 10),
                    tabs: [
                      // before tax
                      Tab(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: AppColors.tabBarBackgroundColor,
                                  width: 1)),
                          child: Align(
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 15.0),
                              child: Text("before_tax".tr(context)),
                            ),
                          ),
                        ),
                      ),
                      // after tax
                      Tab(
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: AppColors.tabBarBackgroundColor,
                                  width: 1)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15.0),
                            child: Align(
                              alignment: Alignment.center,
                              child: Text("after_tax".tr(context)),
                            ),
                          ),
                        ),
                      ),
                    ]),
              ),
            ),
            Expanded(
              child: BlocListener<LocalCubit, LocalState>(
                listener: (context, state) {
                  _fetchProducts();
                },
                child: BlocConsumer<OfferBloc, OfferState>(
                  listener: (context, state) {
                    if (state is LoadingOfferState) {
                      setState(() {
                        _isLoading = true;
                      });
                    } else if (state is LoadingInitOfferState) {
                      setState(() {
                        _isLoadingFirst = true;
                      });
                    } else if (state is EmptyOfferState) {
                      setState(() {
                        _isLoadingFirst = false;
                        _isEmptyProducts = true;
                      });
                    } else if (state is LoadedOfferState) {
                      setState(() {
                        _isLoading = false;
                        _isLoadingFirst = false;
                        _isEmptyProducts = false;
                        if (state.products != null) {
                          _products.addAll(state.products!);
                        }
                        _noInternet = false;
                        _retry = false;
                        if (state.products != null) {
                          _hasMoreData = state.products!.isNotEmpty;
                        }
                      });
                    } else if (state is ErrorOfferState) {
                      if (state.message == offlineFailureMessage) {
                        _noInternet = true;
                        _isLoadingFirst = false;
                      } else {
                        _isLoadingFirst = false;
                        _retry = true;
                        return SnackBarMessage().showErrorSnackBarMessage(
                            message: state.message == serverFailureMessage
                                ? 'try_again'.tr(context)
                                : state.message, context: context);
                      }
                    }
                  },
                  builder: (context, state) {
                    return _isLoadingFirst
                        ? ShimmerWidget.shimmerCustomCart()
                        : _isEmptyProducts
                            ? EmptyWidget(text: 'offers_isEmpty'.tr(context))
                            : _noInternet || _retry
                                ? NoInternetRetryWidget(
                                    isNoInternet: _noInternet,
                                    onTap: () {
                                      _fetchProducts();
                                    })
                                : TabBarView(
                                    children: List.generate(
                                        2,
                                        (index) => ListView.builder(
                                              controller: _scrollController,
                                              itemCount: _products.length,
                                              padding: TranslateX.isRtlLanguage(
                                                      context)
                                                  ? const EdgeInsets.only(
                                                      bottom: 80,
                                                      right: 4,
                                                      left: 15,
                                                    )
                                                  : const EdgeInsets.only(
                                                      bottom: 80,
                                                      right: 15,
                                                      left: 4,
                                                    ),
                                              itemBuilder: (BuildContext
                                                          context,
                                                      int index) =>
                                                  CustomCard(
                                                      product: _products[index],
                                                      pageType: PageType.offer,
                                                      // indexTab 0 : before tax
                                                      // indexTab 1 : after tax
                                                      indexTab: indexTab),
                                            )),
                                  );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
