part of 'offer_bloc.dart';

@immutable
abstract class OfferEvent extends Equatable {
  const OfferEvent();
  @override
  List<Object?> get props => [];
}

class GetAllOffersEvent extends OfferEvent {
  final String locale;
  final int page;
  const GetAllOffersEvent(
      {required this.locale, required this.page});
  @override
  List<Object?> get props => [locale, page];
}

class LoadMoreOffersEvent extends OfferEvent {
  final String locale;
  final int page;
  const LoadMoreOffersEvent(
      {required this.locale, required this.page});

  @override
  List<Object?> get props => [locale, page];
}
