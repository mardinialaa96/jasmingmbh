

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/offers/domain/usecases/offer_usecases.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:meta/meta.dart';

part 'offer_event.dart';
part 'offer_state.dart';

class OfferBloc extends Bloc<OfferEvent, OfferState> {
  final OfferUseCase offerUseCase;
  OfferBloc({required this.offerUseCase}) : super(OfferInitial()) {
    on<OfferEvent>((event, emit) async {
      if (event is GetAllOffersEvent) {
        emit(LoadingInitOfferState());
        try{
          final failureOrDoneMessage =
          await offerUseCase(event.page, event.locale);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorOfferState(message: serverFailureMessage));
        }
      } else if (event is LoadMoreOffersEvent) {
        emit(LoadingOfferState());
        try{
          final result =
          await offerUseCase(event.page, event.locale);
          result.fold((failure) {
            emit(ErrorOfferState(message: _mapFailureToMessage(failure)));
          }, (moreProducts) {
            emit(LoadedOfferState(products: moreProducts));
          });
        } catch (e) {
          emit(const ErrorOfferState(message: serverFailureMessage));
        }
      }
    });
  }
  OfferState _mapFailureOrSuccessToState(
      Either<Failure, List<ProductElement>?> either) {
    return either.fold(
        (failure) => ErrorOfferState(message: _mapFailureToMessage(failure)),
        (productInfo) {
      if (productInfo == null || productInfo.isEmpty) {
        return EmptyOfferState();
      } else {
        return LoadedOfferState(products: productInfo);
      }
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
