part of 'offer_bloc.dart';

@immutable
abstract class OfferState extends Equatable {
  const OfferState();
  @override
  List<Object?> get props => [];
}

class OfferInitial extends OfferState {}

class LoadingOfferState extends OfferState {
  @override
  List<Object> get props => [];
}

class LoadingInitOfferState extends OfferState {
  @override
  List<Object> get props => [];
}

class EmptyOfferState extends OfferState {
  @override
  List<Object> get props => [];
}

class LoadedOfferState extends OfferState {
  final List<ProductElement>? products;

  const LoadedOfferState({required this.products});
  @override
  List<Object?> get props => [products];
}

class ErrorOfferState extends OfferState {
  final String message;

  const ErrorOfferState({required this.message});
  @override
  List<Object?> get props => [message];
}
