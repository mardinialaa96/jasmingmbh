import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

abstract class OfferRepository {
  Future<Either<Failure, List<ProductElement>?>> getAllOffers(
      int page, String locale);
}
