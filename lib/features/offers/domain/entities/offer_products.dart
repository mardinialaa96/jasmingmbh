import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class OfferProducts extends Equatable {
  final List<ProductElement>? productElement;
  const OfferProducts({
    this.productElement,
  });
  @override
  List<Object?> get props => [productElement];
}
