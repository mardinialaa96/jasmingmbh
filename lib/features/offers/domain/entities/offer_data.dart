import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/offers/domain/entities/offer_products.dart';

class OfferData extends Equatable {
  final int? status;
  final String? message;
  final OfferProducts? data;

  const OfferData({
    this.status,
    this.message,
    this.data,
  });
  @override
  List<Object?> get props => [status, message, data];
}
