import 'package:equatable/equatable.dart';

class OfferClass extends Equatable {
  final String? offer;
  final String? offerType;
  final String? offerDesc;
  final num? offerValue;
  final DateTime? startDate;
  final DateTime? endDate;

  const OfferClass({
    this.offer,
    this.offerType,
    this.offerDesc,
    this.offerValue,
    this.startDate,
    this.endDate,
  });
  @override
  List<Object?> get props => [offer, offerType, offerDesc,offerValue, startDate, endDate];

  Map<String, dynamic> toJson() {
    return {
      'offer': offer,
      'offer_type': offerType,
      'offer_desc': offerDesc,
      'offer_value': offerValue,
      'start_date': startDate?.toIso8601String(),
      'end_date': endDate?.toIso8601String(),
    };
  }
}
