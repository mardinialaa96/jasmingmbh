import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/offers/domain/repositories/offer_repositories.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class OfferUseCase {
  final OfferRepository repository;

  OfferUseCase(this.repository);

  Future<Either<Failure, List<ProductElement>?>> call(int page,String locale) async {
    return await repository.getAllOffers(page,locale);
  }
}
