import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications.dart';

abstract class NotificationRepository {
  Future<Either<Failure, Notifications>> getAllNotifications(String locale);
}
