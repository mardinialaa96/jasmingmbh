import 'package:equatable/equatable.dart';

class NotificationElement extends Equatable {
  final String? image;
  final String? type;
  final String? title;
  final String? text;
  final String? date;
  final String? time;

  const NotificationElement({
    this.image,
    this.type,
    this.title,
    this.text,
    this.date,
    this.time
  });

  @override
  List<Object?> get props => [image, type, title, text,date,time];
}
