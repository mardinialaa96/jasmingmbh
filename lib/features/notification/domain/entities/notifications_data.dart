import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notification_element.dart';

class NotificationsData extends Equatable {
  final List<NotificationElement>? notifications;

  const NotificationsData({
    this.notifications,
  });
  @override
  List<Object?> get props => [notifications];
}
