import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications_data.dart';

class Notifications extends Equatable {
  final NotificationsData? notificationsData;
  final String? msg;
  final int? status;

  const Notifications({
    this.notificationsData,
    this.msg,
    this.status,
  });
  @override
  List<Object?> get props => [notificationsData, msg, status];
}
