import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications.dart';
import 'package:jasmingmbh/features/notification/domain/repositories/notification_repository.dart';

class NotificationUseCase {
  final NotificationRepository repository;

  NotificationUseCase(this.repository);

  Future<Either<Failure, Notifications>> call(String locale) async {
    return await repository.getAllNotifications(locale);
  }
}
