part of 'notification_bloc.dart';

@immutable
abstract class NotificationState extends Equatable {
  const NotificationState();

  @override
  List<Object?> get props => [];
}

class NotificationInitial extends NotificationState {
  @override
  List<Object?> get props => [];
}

class LoadingNotificationState extends NotificationState {
  @override
  List<Object?> get props => [];
}

class LoadedNotificationState extends NotificationState {
  final Notifications notifications;

  const LoadedNotificationState({required this.notifications});
  @override
  List<Object?> get props => [notifications];
}

class ErrorNotificationState extends NotificationState {
  final String message;

  const ErrorNotificationState({required this.message});
  @override
  List<Object?> get props => [message];
}
