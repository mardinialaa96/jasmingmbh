import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications.dart';
import 'package:jasmingmbh/features/notification/domain/usecases/notification_usecase.dart';
import 'package:meta/meta.dart';

part 'notification_event.dart';
part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  NotificationUseCase notificationUseCase;
  NotificationBloc({required this.notificationUseCase})
      : super(NotificationInitial()) {
    on<NotificationEvent>((event, emit) async {
      if (event is NotificationUserEvent) {
        emit(LoadingNotificationState());
        try{
          final failureOrDoneMessage = await notificationUseCase(event.locale);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorNotificationState(message: serverFailureMessage));
        }
      }
    });
  }
  NotificationState _mapFailureOrSuccessToState(
      Either<Failure, Notifications> either) {
    return either.fold(
        (failure) =>
            ErrorNotificationState(message: _mapFailureToMessage(failure)),
        (notificationInfo) {
      return LoadedNotificationState(notifications: notificationInfo);
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
