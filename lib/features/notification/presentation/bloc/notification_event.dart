part of 'notification_bloc.dart';

@immutable
abstract class NotificationEvent extends Equatable {
  const NotificationEvent();

  @override
  List<Object?> get props => [];
}

class NotificationUserEvent extends NotificationEvent {
  final String locale;
  const NotificationUserEvent({required this.locale});
  @override
  List<Object?> get props => [locale];
}
