import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/widgets/empty_widget.dart';
import 'package:jasmingmbh/core/widgets/error_data_widget.dart';
import 'package:jasmingmbh/core/widgets/shimmer_widget.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notification_element.dart';
import 'package:jasmingmbh/features/notification/presentation/bloc/notification_bloc.dart';
import 'package:jasmingmbh/features/notification/presentation/widget/card_notification_widget.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({super.key});

  @override
  State<NotificationPage> createState() => NotificationPageSate();
}

class NotificationPageSate extends State<NotificationPage> {
  @override
  void initState() {
    BlocProvider.of<NotificationBloc>(context).add(
        NotificationUserEvent(locale: '${LocalStorageService().currentLang}'));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppColors.backgroundColor,
        title:
            Text('notification'.tr(context), style: AppTextStyle.titleBoldDark),
      ),
      body: BlocBuilder<NotificationBloc, NotificationState>(
        builder: (context, state) {
          if (state is LoadingNotificationState) {
            return ShimmerWidget.shimmerCard();
          } else if (state is LoadedNotificationState) {
            if (state.notifications.status != null &&
                state.notifications.status != 200) {
              return Center(
                child: Text(
                  state.notifications.msg ?? "",
                  style: AppTextStyle.subtitleBold,
                ),
              );
            } else {
              if (state.notifications.notificationsData != null &&
                  state.notifications.notificationsData!.notifications !=
                      null &&
                  state.notifications.notificationsData!.notifications!
                      .isEmpty) {
                return EmptyWidget(text: 'notification_isEmpty'.tr(context));
              } else {
                List<NotificationElement> notificationList =
                    state.notifications.notificationsData!.notifications!;
                return ListView.builder(
                  itemCount: notificationList.length,
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 10),
                    child: CardNotificationWidget(
                        notification: notificationList[index]),
                  ),
                );
              }
            }
          } else if (state is ErrorNotificationState) {
            return ErrorDataWidget(
                text: state.message == offlineFailureMessage
                    ? 'no_internet'.tr(context)
                    : state.message == serverFailureMessage
                        ? 'try_again'.tr(context)
                        : state.message,
                onTap: () {
                  BlocProvider.of<NotificationBloc>(context).add(
                      NotificationUserEvent(
                          locale: '${LocalStorageService().currentLang}'));
                });
          } else {
            return const SizedBox();
          }
        },
      ),
    );
  }
}
