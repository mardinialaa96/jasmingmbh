import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notification_element.dart';

class CardNotificationWidget extends StatelessWidget {
  final NotificationElement notification;
  const CardNotificationWidget({super.key, required this.notification});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        color: AppColors.whiteColor,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Container(
                width: 80.0,
                height: 80.0,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(50.0)),
                ),
                child: Image.asset(Assets.appIcon),
              ),
            ),
            Expanded(
                child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 5),
                  Text(notification.title ?? "",
                      style: AppTextStyle.notificationTitleText),
                  const SizedBox(height: 5),
                  Text(notification.text ?? "",
                      style: AppTextStyle.notificationSubText),
                  const SizedBox(height: 15),
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                      text: "${notification.date}",
                      style: AppTextStyle.notificationDateText,
                    ),
                    const WidgetSpan(child: SizedBox(width: 15.0)),
                    TextSpan(
                      text: "${notification.time}",
                      style: AppTextStyle.notificationDateText,
                    ),
                  ])),
                  const SizedBox(height: 5),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
