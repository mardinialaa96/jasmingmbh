import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/notification/data/models/notifications_model.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications.dart';
import 'package:jasmingmbh/main.dart';

abstract class NotificationRemoteDataSource {
  Future<Notifications> getAllNotifications(String locale);
}

class NotificationRemoteDataSourceImpl implements NotificationRemoteDataSource {
  final http.Client client;

  NotificationRemoteDataSourceImpl({required this.client});

  @override
  Future<Notifications> getAllNotifications(String locale) async {
    final queryParameters = {
      'locale': locale,
    };
    final token = LocalStorageService().token;
    if (token == null) {
      throw ServerException();
    }
    final url = Uri.parse('$baseUrl/notifications');
    final response = await client
        .get(url.replace(queryParameters: queryParameters), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    print('token ${LocalStorageService().token}');
    print('response.statusCode ${response.statusCode}');
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      NotificationsModel notificationsModel =
          NotificationsModel.fromJson(decodeJson);
      print('response.body $notificationsModel');
      print(
          'length ${notificationsModel.notificationsData?.notifications?.length}');
      if (notificationsModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
            (route) => false);
        return notificationsModel;
      } else {
        return notificationsModel;
      }
    } else {
      throw ServerException();
    }
  }
}
