import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/notification/data/datasources/notification_remote_data_source.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications.dart';
import 'package:jasmingmbh/features/notification/domain/repositories/notification_repository.dart';

class NotificationRepositoryImpl implements NotificationRepository {
  final NotificationRemoteDataSource notificationRemoteDataSource;
  final NetworkInfo networkInfo;

  NotificationRepositoryImpl(
      {required this.notificationRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, Notifications>> getAllNotifications(
      String locale) async {
    if (await networkInfo.isConnected) {
      try {
        final notificationInfo =
            await notificationRemoteDataSource.getAllNotifications(locale);
        return Right(notificationInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
