import 'package:jasmingmbh/features/notification/data/models/notification_element_model.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications_data.dart';

class NotificationsDataModel extends NotificationsData {
  const NotificationsDataModel({required super.notifications});

  factory NotificationsDataModel.fromJson(Map<String, dynamic> json) =>
      NotificationsDataModel(
        notifications: json["notifications"] == null
            ? []
            : List<NotificationElementModel>.from(json["notifications"]!
                .map((x) => NotificationElementModel.fromJson(x))),
      );
}
