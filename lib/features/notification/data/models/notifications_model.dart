import 'package:jasmingmbh/features/notification/data/models/notifications_data_model.dart';
import 'package:jasmingmbh/features/notification/domain/entities/notifications.dart';

class NotificationsModel extends Notifications {
  const NotificationsModel(
      {required super.notificationsData,
      required super.msg,
      required super.status});
  factory NotificationsModel.fromJson(Map<String, dynamic> json) =>
      NotificationsModel(
        notificationsData:
            json["data"] == null ? null : NotificationsDataModel.fromJson(json["data"]),
        msg: json["msg"],
        status: json["status"],
      );
}
