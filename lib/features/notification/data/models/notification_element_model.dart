import 'package:jasmingmbh/features/notification/domain/entities/notification_element.dart';

class NotificationElementModel extends NotificationElement {
  const NotificationElementModel(
      {required super.image,
      required super.title,
      required super.type,
      required super.text,
      required super.date,
      required super.time});

  factory NotificationElementModel.fromJson(Map<String, dynamic> json) =>
      NotificationElementModel(
        image: json["image"],
        type: json["type"],
        title: json["title"],
        text: json["text"],
        date: json["date"],
        time: json["time"],
      );
}
