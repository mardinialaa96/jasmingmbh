import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/data/models/success_or_failure_model.dart';
import 'package:jasmingmbh/features/notification/presentation/pages/notification_page.dart';
import 'package:jasmingmbh/main.dart';
import 'package:http/http.dart' as http;

class FirebaseApi {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final InternetConnectionChecker internetConnectionChecker = InternetConnectionChecker();
  final AndroidNotificationChannel _androidChannel =
  const AndroidNotificationChannel(
    "high_importance_channel",
    "High Importance Notifications",
    description: 'this channel is used for important notification',
    importance: Importance.defaultImportance,
  );
  final FlutterLocalNotificationsPlugin _localNotifications =
  FlutterLocalNotificationsPlugin();

  void handleMessage(RemoteMessage? message) {
    if (message == null) return;
    Navigator.of(navigatorKey.currentContext!).push(MaterialPageRoute(
      builder: (context) => const NotificationPage(),
    ));
  }

  Future<void> initPushNotifications() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    FirebaseMessaging.instance.getInitialMessage().then(handleMessage);
    FirebaseMessaging.onMessageOpenedApp.listen(handleMessage);
    FirebaseMessaging.onBackgroundMessage(handleBackgroundMessage);
    FirebaseMessaging.onMessage.listen((message) {
      final notification = message.notification;
      if (notification == null) return;
      _localNotifications.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            _androidChannel.id,
            _androidChannel.name,
            channelDescription: _androidChannel.description,
            icon: '@drawable/ic_launcher',
          ),
        ),
        payload: jsonEncode(message.data),
      );
    });
  }

  Future<void> initLocalNotifications() async {
    const ios = IOSInitializationSettings();
    const android = AndroidInitializationSettings('@drawable/ic_launcher');
    const setting = InitializationSettings(android: android, iOS: ios);
    await _localNotifications.initialize(setting,
        onSelectNotification: (payload) {
          final message = RemoteMessage.fromMap(jsonDecode(payload!));
          handleMessage(message);
        });
    final platform = _localNotifications.resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>();
    await platform?.createNotificationChannel(_androidChannel);
  }

  Future<void> initNotifications() async {
    NetworkInfo networkInfo = NetworkInfoImpl(internetConnectionChecker);
    await _firebaseMessaging.requestPermission();
    final fcmToken = await _firebaseMessaging.getToken();
    print("NOTIFICATION TOKEN: $fcmToken");
    print(LocalStorageService().isLoggedIn);
    if(LocalStorageService().isLoggedIn == true && await networkInfo.isConnected){
      await sendFCMAPI(fcmToken);
    }
    await initPushNotifications();
    await initLocalNotifications();
  }

  Future<void> handleBackgroundMessage(RemoteMessage message) async {
    if (message.notification != null) {
      print('title ${message.notification!.title}');
      print('body ${message.notification!.body}');
      print('payload ${message.data}');
    } else {
      print('Received FCM message without notification payload');
    }
  }

  Future<SuccessOrFailureModel> sendFCMAPI(String? fcmToken) async {
    final queryParameters = {
      'fcm_token': fcmToken,
    };
    final url = Uri.parse('$baseUrl/fcmToken');
    final response =
    await http.get(url.replace(queryParameters: queryParameters), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      SuccessOrFailureModel getFcmTokenModel =
      SuccessOrFailureModel.fromJson(decodeJson);
      print('FCM API Response: ${getFcmTokenModel.status}');
      return getFcmTokenModel;
    } else {
      throw ServerException();
    }
  }
}
