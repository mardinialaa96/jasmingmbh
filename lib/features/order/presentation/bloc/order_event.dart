part of 'order_bloc.dart';

@immutable
abstract class OrderEvent extends Equatable {
  const OrderEvent();

  @override
  List<Object?> get props => [];
}

class PostOrderEvent extends OrderEvent {
  final PostOrderItems postOrderItems;

  const PostOrderEvent({required this.postOrderItems});
  @override
  List<Object?> get props => [postOrderItems];
}
