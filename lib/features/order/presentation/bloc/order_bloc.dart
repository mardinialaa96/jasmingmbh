import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/order/domain/entities/order_return_data.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_order_items.dart';
import 'package:jasmingmbh/features/order/domain/usecases/order_usecases.dart';
import 'package:meta/meta.dart';

part 'order_event.dart';
part 'order_state.dart';

class OrderBloc extends Bloc<OrderEvent, OrderState> {
  final OrderUseCase orderUseCase;
  OrderBloc({required this.orderUseCase}) : super(OrderInitial()) {
    on<OrderEvent>((event, emit) async {
      if (event is PostOrderEvent) {
        emit(LoadingOrderState());
        try {
          final failureOrDoneMessage = await orderUseCase(event.postOrderItems);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorOrderState(message: serverFailureMessage));
        }
      }
    });
  }
  OrderState _mapFailureOrSuccessToState(
      Either<Failure, OrderReturnData> either) {
    return either.fold(
        (failure) => _mapFailureToState(failure),
        (orderInfo) {
      return LoadedOrderState(orderReturnData: orderInfo);
    });
  }
  OrderState _mapFailureToState(Failure failure) {
    if (failure is OfflineFailure) {
      return const ErrorOrderState(message: offlineFailureMessage);
    } else {
      return ErrorOrderState(message: _mapFailureToMessage(failure));
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
