part of 'cart_ids_bloc.dart';

@immutable
sealed class CartIdsEvent extends Equatable {
  const CartIdsEvent();

  @override
  List<Object?> get props => [];
}

class CartItemsIdsEvent extends CartIdsEvent {
  final PostCartIds postCartIds;
  final String locale;

  const CartItemsIdsEvent({required this.postCartIds, required this.locale});
  @override
  List<Object?> get props => [postCartIds, locale];
}
