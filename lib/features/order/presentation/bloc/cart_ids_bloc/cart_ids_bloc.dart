import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_cart_ids.dart';
import 'package:jasmingmbh/features/order/domain/usecases/cart_ids_usecases.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:meta/meta.dart';

part 'cart_ids_event.dart';
part 'cart_ids_state.dart';

class CartIdsBloc extends Bloc<CartIdsEvent, CartIdsState> {
  final CartIdsUseCase cartIdsUseCase;
  CartIdsBloc({required this.cartIdsUseCase}) : super(CartIdsInitial()) {
    on<CartIdsEvent>((event, emit) async {
      if (event is CartItemsIdsEvent) {
        emit(LoadingCartIdsState());
        try {
          final failureOrDoneMessage =
              await cartIdsUseCase(event.postCartIds, event.locale);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorCartIdsState(message: serverFailureMessage));
        }
      }
    });
  }
  CartIdsState _mapFailureOrSuccessToState(
      Either<Failure, List<ProductElement>> either) {
    return either.fold((failure) => _mapFailureToState(failure),
        (cartItemsInfo) {
      return LoadedCartIdsState(products: cartItemsInfo);
    });
  }

  CartIdsState _mapFailureToState(Failure failure) {
    if (failure is OfflineFailure) {
      return const ErrorCartIdsState(message: offlineFailureMessage);
    } else {
      return ErrorCartIdsState(message: _mapFailureToMessage(failure));
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
