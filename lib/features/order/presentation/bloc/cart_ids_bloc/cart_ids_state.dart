part of 'cart_ids_bloc.dart';

@immutable
sealed class CartIdsState extends Equatable {
  const CartIdsState();

  @override
  List<Object?> get props => [];
}

final class CartIdsInitial extends CartIdsState {}

class LoadingCartIdsState extends CartIdsState {
  @override
  List<Object?> get props => [];
}

class LoadedCartIdsState extends CartIdsState {
  final List<ProductElement> products;

  const LoadedCartIdsState({required this.products});
  @override
  List<Object?> get props => [products];
}

class ErrorCartIdsState extends CartIdsState {
  final String message;

  const ErrorCartIdsState({required this.message});
  @override
  List<Object?> get props => [message];
}
