part of 'order_bloc.dart';

@immutable
abstract class OrderState extends Equatable {
  const OrderState();

  @override
  List<Object?> get props => [];
}

class OrderInitial extends OrderState {}

class LoadingOrderState extends OrderState {
  @override
  List<Object?> get props => [];
}

class LoadedOrderState extends OrderState {
  final OrderReturnData orderReturnData;

  const LoadedOrderState({required this.orderReturnData});
  @override
  List<Object?> get props => [orderReturnData];
}

class ErrorOrderState extends OrderState {
  final String message;

  const ErrorOrderState({required this.message});
  @override
  List<Object?> get props => [message];
}
