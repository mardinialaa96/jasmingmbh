import 'package:jasmingmbh/features/order/domain/entities/order_return_data.dart';

class OrderReturnDataModel extends OrderReturnData {
  const OrderReturnDataModel(
      {required super.status,
      required super.data,
      required super.msg});

  factory OrderReturnDataModel.fromJson(Map<String, dynamic> json) =>
      OrderReturnDataModel(
        data: json["data"] == null
            ? []
            : List<dynamic>.from(json["data"]!.map((x) => x)),
        msg: json["msg"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null ? [] : List<dynamic>.from(data!.map((x) => x)),
        "msg": msg,
        "status": status,
      };
}
