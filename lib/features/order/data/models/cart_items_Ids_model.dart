import 'package:jasmingmbh/features/order/domain/entities/cart_items_ids.dart';

class CartItemsIdsModel extends CartItemsIds {
  const CartItemsIdsModel({required super.productId});

  factory CartItemsIdsModel.fromJson(Map<String, dynamic> json) {
    return CartItemsIdsModel(
      productId: json["product"],
    );
  }

  @override
  Map<String, dynamic> toJson() => {
        "product": productId,
      };
}
