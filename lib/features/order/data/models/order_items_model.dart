import 'package:jasmingmbh/features/order/domain/entities/order_items.dart';

class OrderItemsModel extends OrderItems {
  const OrderItemsModel({required super.productId, required super.amount});

  factory OrderItemsModel.fromJson(Map<String, dynamic> json) {
    return OrderItemsModel(
      productId: json["product"],
      amount: json["amount"],
    );
  }

  Map<String, dynamic> toJson() => {
        "product": productId,
        "amount": amount,
      };
}
