import 'package:jasmingmbh/features/order/data/models/order_items_model.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_order_items.dart';

class PostOrderItemsModel extends PostOrderItems{
  const PostOrderItemsModel({required super.orderItems});


  factory PostOrderItemsModel.fromMap(Map<String, dynamic> json) =>
      PostOrderItemsModel(
        orderItems: List<OrderItemsModel>.from(
            json['items'].map((x) => OrderItemsModel.fromJson(x))),
      );

  Map<String, Object?> toMap() {
    return {
      'items': orderItems.map((f) => f.toJson()).toList(),
    }..removeWhere((key, value) => value == null);
  }
}