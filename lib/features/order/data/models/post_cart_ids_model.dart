import 'package:jasmingmbh/features/order/data/models/cart_items_Ids_model.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_cart_ids.dart';

class PostCartIdsModel extends PostCartIds {
  const PostCartIdsModel({required super.cartItemsIds});

  factory PostCartIdsModel.fromMap(Map<String, dynamic> json) =>
      PostCartIdsModel(
        cartItemsIds: List<CartItemsIdsModel>.from(
            json['items'].map((x) => CartItemsIdsModel.fromJson(x))),
      );

  Map<String, Object?> toMap() {
    return {
      'items': cartItemsIds.map((f) => f.toJson()).toList(),
    }..removeWhere((key, value) => value == null);
  }
}
