import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/order/data/models/order_return_data_model.dart';
import 'package:jasmingmbh/features/order/data/models/post_order_items_model.dart';
import 'package:jasmingmbh/main.dart';

abstract class OrderRemoteDataSource {
  Future<OrderReturnDataModel> order(PostOrderItemsModel postOrderItemsModel);
}

class OrderRemoteDataSourceImpl implements OrderRemoteDataSource {
  final http.Client client;

  OrderRemoteDataSourceImpl({required this.client});

  @override
  Future<OrderReturnDataModel> order(
      PostOrderItemsModel postOrderItemsModel) async {
    var formData =
        http.MultipartRequest('POST', Uri.parse('$baseUrl/order/register'));
    formData.headers.addAll({
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });

    for (int i = 0; i < postOrderItemsModel.orderItems.length; i++) {
      formData.fields['items[$i][product]'] =
          postOrderItemsModel.orderItems[i].productId;
      formData.fields['items[$i][amount]'] =
          postOrderItemsModel.orderItems[i].amount;
    }

    try {
      final streamedResponse = await formData.send();
      final response = await http.Response.fromStream(streamedResponse);

      if (response.statusCode >= 200 && response.statusCode < 300) {
        var decodeJson = json.decode(response.body);
        OrderReturnDataModel returnDataModel =
            OrderReturnDataModel.fromJson(decodeJson);
        if (returnDataModel.status == 2000) {
          navigatorKey.currentState!.pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => const BlockPage()),
              (route) => false);
          return returnDataModel;
        } else {
          return returnDataModel;
        }
      } else {
        throw ServerException();
      }
    } catch (error) {
      throw OfflineFailure();
    }
  }
}
