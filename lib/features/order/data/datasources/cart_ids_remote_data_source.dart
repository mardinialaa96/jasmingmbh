import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/order/data/models/post_cart_ids_model.dart';
import 'package:jasmingmbh/features/products/data/models/product_by_category_model.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/main.dart';

abstract class CartIdsRemoteDataSource {
  Future<List<ProductElement>> getCartItems(
      PostCartIdsModel postCartIdsModel, String local);
}

class CartIdsRemoteDataSourceImpl implements CartIdsRemoteDataSource {
  final http.Client client;

  CartIdsRemoteDataSourceImpl({required this.client});

  @override
  Future<List<ProductElement>> getCartItems(
      PostCartIdsModel postCartIdsModel, String local) async {
    var formData = http.MultipartRequest(
        'POST', Uri.parse('$baseUrl/get-products?locale=$local'));
    formData.headers.addAll({
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });

    for (int i = 0; i < postCartIdsModel.cartItemsIds.length; i++) {
      formData.fields['items[$i]'] = postCartIdsModel.cartItemsIds[i].productId;
    }

    try {
      final streamedResponse = await formData.send();
      final response = await http.Response.fromStream(streamedResponse);

      if (response.statusCode >= 200 && response.statusCode < 300) {
        var decodeJson = json.decode(response.body);
        ProductByCategoryModel returnDataModel =
            ProductByCategoryModel.fromJson(decodeJson);
        if (returnDataModel.status == 2000) {
          navigatorKey.currentState!.pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => const BlockPage()),
              (route) => false);
          return returnDataModel.dataProductByCategoryInfo.products;
        } else {
          return returnDataModel.dataProductByCategoryInfo.products;
        }
      } else {
        throw ServerException();
      }
    } catch (error) {
      throw OfflineFailure();
    }
  }
}
