import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/order/data/datasources/cart_ids_remote_data_source.dart';
import 'package:jasmingmbh/features/order/data/models/post_cart_ids_model.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_cart_ids.dart';
import 'package:jasmingmbh/features/order/domain/repositories/cart_ids_repository.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class CartIdsRepositoryImpl implements CartIdsRepository {
  final CartIdsRemoteDataSource cartIdsRemoteDataSource;
  final NetworkInfo networkInfo;

  CartIdsRepositoryImpl(
      {required this.cartIdsRemoteDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, List<ProductElement>>> getCartItems(
      PostCartIds postCartIds,String local) async {
    final cartIdsModel =
        PostCartIdsModel(cartItemsIds: postCartIds.cartItemsIds);
    if (await networkInfo.isConnected) {
      try {
        final cartIdsInfo =
            await cartIdsRemoteDataSource.getCartItems(cartIdsModel,local);
        return Right(cartIdsInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
