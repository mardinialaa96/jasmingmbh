import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/order/data/datasources/order_remote_data_sources.dart';
import 'package:jasmingmbh/features/order/data/models/post_order_items_model.dart';
import 'package:jasmingmbh/features/order/domain/entities/order_return_data.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_order_items.dart';
import 'package:jasmingmbh/features/order/domain/repositories/order_repositories.dart';

class OrderRepositoryImpl implements OrderRepository {
  final OrderRemoteDataSource orderRemoteDataSource;
  final NetworkInfo networkInfo;

  OrderRepositoryImpl(
      {required this.orderRemoteDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, OrderReturnData>> order(
      PostOrderItems postOrderItems) async {
    final orderModel =
        PostOrderItemsModel(orderItems: postOrderItems.orderItems);
    if (await networkInfo.isConnected) {
      try {
        final orderInfo = await orderRemoteDataSource.order(orderModel);
        return Right(orderInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
