import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/order/domain/entities/order_return_data.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_order_items.dart';
import 'package:jasmingmbh/features/order/domain/repositories/order_repositories.dart';


class OrderUseCase {
  final OrderRepository repository;

  OrderUseCase(this.repository);

  Future<Either<Failure, OrderReturnData>> call(
      PostOrderItems postOrderItems) async {
    return await repository.order(postOrderItems);
  }
}
