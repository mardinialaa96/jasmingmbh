import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_cart_ids.dart';
import 'package:jasmingmbh/features/order/domain/repositories/cart_ids_repository.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class CartIdsUseCase {
  final CartIdsRepository repository;

  CartIdsUseCase(this.repository);

  Future<Either<Failure, List<ProductElement>>> call(
      PostCartIds postCartIds, String local) async {
    return await repository.getCartItems(postCartIds,local);
  }
}
