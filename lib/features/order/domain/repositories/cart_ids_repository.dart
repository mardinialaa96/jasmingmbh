import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_cart_ids.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

abstract class CartIdsRepository {
  Future<Either<Failure, List<ProductElement>>> getCartItems(
      PostCartIds postCartIds,String local);
}
