import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/order/domain/entities/order_return_data.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_order_items.dart';

abstract class OrderRepository {
  Future<Either<Failure, OrderReturnData>> order(PostOrderItems postOrderItems);
}
