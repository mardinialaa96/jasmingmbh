import 'package:equatable/equatable.dart';

class CartItemsIds extends Equatable {
  final String productId;

  const CartItemsIds({required this.productId});
  @override
  List<Object?> get props => [productId];

  Map<String, dynamic> toJson() => {
        "productId": productId,
      };
}
