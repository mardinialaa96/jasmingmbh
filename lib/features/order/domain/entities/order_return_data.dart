import 'package:equatable/equatable.dart';

class OrderReturnData extends Equatable {
  final List<dynamic>? data;
  final String? msg;
  final int? status;

  const OrderReturnData({
    this.data,
    this.msg,
    this.status,
  });
  @override
  List<Object?> get props => [data, msg, status];
}
