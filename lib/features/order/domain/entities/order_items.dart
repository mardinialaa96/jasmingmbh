import 'package:equatable/equatable.dart';

class OrderItems extends Equatable {
  final String productId;
  final String amount;

  const OrderItems({required this.productId, required this.amount});
  @override
  List<Object?> get props => [productId, amount];

  Map<String, dynamic> toJson() => {
    "productId": productId,
    "amount": amount,
  };
}
