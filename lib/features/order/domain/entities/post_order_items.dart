import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/order/domain/entities/order_items.dart';


class PostOrderItems extends Equatable {
  final List<OrderItems> orderItems;

  const PostOrderItems({required this.orderItems});
  @override
  List<Object?> get props => [orderItems];
}
