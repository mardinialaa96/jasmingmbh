import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/order/domain/entities/cart_items_ids.dart';

class PostCartIds extends Equatable {
  final List<CartItemsIds> cartItemsIds;

  const PostCartIds({required this.cartItemsIds});
  @override
  List<Object?> get props => [cartItemsIds];
}
