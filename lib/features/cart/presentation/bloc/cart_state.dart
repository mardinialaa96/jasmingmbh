part of 'cart_bloc.dart';

class CartState extends Equatable {
  final List<ProductElement> cartItems;
  final num totalPrice;

  const CartState({required this.cartItems, required this.totalPrice});

  factory CartState.initial() {
    return const CartState(cartItems: [], totalPrice: 0);
  }

  CartState clearCart() {
    return const CartState(cartItems: [], totalPrice: 0);
  }

  @override
  List<Object?> get props => [cartItems, totalPrice];
}
