import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(const CartState(cartItems: [], totalPrice: 0)) {
    _initPreferences();
    on<CartEvent>((event, emit) async {
      if (event is AddToCart) {
        _addToCart(event);
      } else if (event is RemoveFromCart) {
        _removeFromCart(event);
      } else if (event is ClearCart) {
        _clearCartItemsFromPrefs();
        emit(const CartState(cartItems: [], totalPrice: 0));
      } else if(event is UpdateCartItems){
        List<ProductElement> updatedProducts = [];
        final List<String>? cartItemsString = LocalStorageService().getCartItems;
        if (cartItemsString != null) {
          final List<ProductElement> cartItems = cartItemsString
              .map((jsonString) =>
              ProductElement.fromJsonCart(json.decode(jsonString)))
              .toList();
          cartItems.forEach((element) {
            event.updatedProducts.forEach((e) {
              if(element.product == e.product){
                updatedProducts.add(ProductElement(
                  product: e.product,
                  image: e.image,
                  name: e.name,
                  desc: e.desc,
                  unit: e.unit,
                  price: e.price,
                  packagePrice: e.packagePrice,
                  packageAmount: e.packageAmount,
                  taxIncluded: e.taxIncluded,
                  taxValue: e.taxValue,
                  offerCount: e.offerCount,
                  amount: e.amount,
                  offer: e.offer,
                  available: e.available,
                  quantity: element.quantity,
                  subtotal: _calculateSubTotal(e,element.quantity),
                ));
              }
            });
          });
          _updateTotalPrice(updatedProducts);
        }
      }
    });
  }

  Future<void> _initPreferences() async {
    final List<String>? cartItemsString = LocalStorageService().getCartItems;
    if (cartItemsString != null) {
      final List<ProductElement> cartItems = cartItemsString
          .map((jsonString) =>
              ProductElement.fromJsonCart(json.decode(jsonString)))
          .toList();
      num totalPrice = LocalStorageService().getTotalPrice ?? 0;
      emit(CartState(cartItems: cartItems, totalPrice: totalPrice));
    }
  }

  void _addToCart(AddToCart event) {
    List<ProductElement> updatedProducts = List.from(state.cartItems);
    int index = updatedProducts
        .indexWhere((element) => element.product == event.product.product);
    if (index != -1) {
      updatedProducts[index].quantity =
          updatedProducts[index].quantity + event.quantity;
      updatedProducts[index].subtotal = _calculateSubTotal(updatedProducts[index],updatedProducts[index].quantity);
    } else {
      updatedProducts.add(ProductElement(
        product: event.product.product,
        image: event.product.image,
        name: event.product.name,
        desc: event.product.desc,
        unit: event.product.unit,
        price: event.product.price,
        packagePrice: event.product.packagePrice,
        packageAmount: event.product.packageAmount,
        taxIncluded: event.product.taxIncluded,
        taxValue: event.product.taxValue,
        offerCount: event.product.offerCount,
        amount: event.product.amount,
        offer: event.product.offer,
        available: event.product.available,
        quantity: event.quantity,
        subtotal: _calculateSubTotal(event.product,event.quantity),
      ));
    }
    _updateTotalPrice(updatedProducts);
  }

  void _removeFromCart(RemoveFromCart event) {
    List<ProductElement> updatedProducts = List.from(state.cartItems);
    int index = updatedProducts
        .indexWhere((element) => element.product == event.product.product);
    if (index != -1) {
      if (updatedProducts[index].quantity > 1) {
        updatedProducts[index].quantity = updatedProducts[index].quantity - 1;
        updatedProducts[index].subtotal = _calculateSubTotal(updatedProducts[index],updatedProducts[index].quantity);
      } else {
        updatedProducts.removeAt(index);
      }
      _updateTotalPrice(updatedProducts);
    }
  }

  void _updateTotalPrice(List<ProductElement> updatedProducts) {
    num totalPrice = _calculateTotal(updatedProducts);
    _saveCartItemsToPrefs(updatedProducts, totalPrice);
    emit(CartState(cartItems: updatedProducts, totalPrice: totalPrice));
  }

  num _calculateTotal(List<ProductElement> products) {
    num totalPrice = 0.0;
    for (var product in products) {
      num newPrice = product.packagePrice ?? 0.0;
      if (product.offer != null &&
          product.offer!.offer == 'percent' &&
          product.offer!.offerValue != null) {
        newPrice = (product.packagePrice ?? 0.0 ) -
                ((product.packagePrice ??
                    0.0) * (product.offer!.offerValue! / 100));
      }
      totalPrice += ((product.quantity * newPrice) +
          (product.taxIncluded == 'Y'
              ? ((product.taxValue ?? 0.0) / 100) * (product.quantity * newPrice)
              : 0));
    }
    return totalPrice;
  }

  Future<void> _saveCartItemsToPrefs(
      List<ProductElement> updatedProducts, num totalPrice) async {
    final List<String> cartItemsString = updatedProducts
        .map((product) => json.encode(product.toJson()))
        .toList();
    LocalStorageService().isCartItems = cartItemsString;
    LocalStorageService().isTotalPrice = totalPrice.toDouble();
  }

  Future<void> _clearCartItemsFromPrefs() async {
    LocalStorageService().isCartItems = null;
    LocalStorageService().isTotalPrice = null;
  }

  num _calculateSubTotal(ProductElement product, int quantity){
    num subTotalPrice = 0.0;
    num newPrice = product.packagePrice ?? 0.0;
    if (product.offer != null &&
        product.offer!.offer == 'percent' &&
        product.offer!.offerValue != null) {
      newPrice = (product.packagePrice ??
          0.0) - ((product.packagePrice ??
                  0.0) * (product.offer!.offerValue! / 100));
    }
    subTotalPrice += ((quantity * newPrice) +
        (product.taxIncluded == 'Y'
            ? ((product.taxValue ?? 0.0) / 100) * (quantity * newPrice)
            : 0));
    return subTotalPrice;
  }

}
