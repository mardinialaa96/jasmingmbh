part of 'cart_bloc.dart';

@immutable
abstract class CartEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class AddToCart extends CartEvent {
  final ProductElement product;
  final int quantity;

  AddToCart({required this.product,required this.quantity});

  @override
  List<Object?> get props => [product, quantity];
}

class RemoveFromCart extends CartEvent {
  final ProductElement product;

  RemoveFromCart({required this.product});

  @override
  List<Object?> get props => [product];
}

class ClearCart extends CartEvent {}

class UpdateCartItems extends CartEvent {
  final List<ProductElement> updatedProducts;

   UpdateCartItems({required this.updatedProducts});

  @override
  List<Object> get props => [updatedProducts];
}