import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/add_to_cart_animation/add_to_cart_animation.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/enum/page_type.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/config.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/custom_card.dart';
import 'package:jasmingmbh/core/widgets/loading_widget.dart';
import 'package:jasmingmbh/core/widgets/lottie.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';
import 'package:jasmingmbh/features/order/domain/entities/cart_items_ids.dart';
import 'package:jasmingmbh/features/order/domain/entities/order_items.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_cart_ids.dart';
import 'package:jasmingmbh/features/order/domain/entities/post_order_items.dart';
import 'package:jasmingmbh/features/order/presentation/bloc/cart_ids_bloc/cart_ids_bloc.dart';
import 'package:jasmingmbh/features/order/presentation/bloc/order_bloc.dart';
import 'package:jasmingmbh/main.dart';

class CartPage extends StatefulWidget {
  const CartPage({super.key});

  @override
  State<StatefulWidget> createState() => CartPageState();
}

class CartPageState extends State<CartPage> {
  late final Function(GlobalKey) runAddToCartAnimation;
  late final Function(GlobalKey) runRemoveFromCartAnimation;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AddToCartAnimation(
      cartKey: cartKey,
      height: 30,
      width: 30,
      opacity: 0.85,
      jumpAnimation: const JumpAnimationOptions(
          active: false,
          curve: Curves.linear,
          duration: Duration(milliseconds: 0)),
      dragAnimation: const DragToCartAnimationOptions(
        curve: Curves.linearToEaseOut,
        duration: Duration(milliseconds: 400),
      ),
      createAddToCartAnimation: (unAddToCartAnimation) {
        runAddToCartAnimation = unAddToCartAnimation;
      },
      createRemoveFromCartAnimation: (unRemoveFromCartAnimation) {
        runRemoveFromCartAnimation = unRemoveFromCartAnimation;
      },
      child: Scaffold(
          body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  children: <Widget>[
                    Text('cart'.tr(context),
                        style: AppTextStyle.titleExtraBoldDark),
                  ],
                ),
              ),
              BlocConsumer<OrderBloc, OrderState>(listener: (context, state) {
                if (state is LoadedOrderState) {
                  if (state.orderReturnData.status != 200) {
                    SnackBarMessage().showErrorSnackBarMessage(
                        message: state.orderReturnData.msg ?? "",
                        context: context);
                  } else {
                    SnackBarMessage().showSuccessSnackBarMessage(
                        message: state.orderReturnData.msg ?? "",
                        context: context);
                    BlocProvider.of<CartBloc>(context).add(ClearCart());
                  }
                } else if (state is ErrorOrderState) {
                    SnackBarMessage().showErrorSnackBarMessage(
                        message: state.message == offlineFailureMessage
                            ? 'no_internet'.tr(context)
                            : state.message == serverFailureMessage
                            ? 'try_again'.tr(context)
                            : state.message, context: context);
                }
              }, builder: (context, state) {
                if (state is LoadingOrderState) {
                  return const Expanded(child: LoadingWidget());
                }
                return BlocListener<LocalCubit, LocalState>(
                  listener: (context, localS) {
                    final cartState = BlocProvider.of<CartBloc>(context).state;
                    List<CartItemsIds> orderItems = [];
                    for (var item in cartState.cartItems) {
                      CartItemsIds itemOrder =
                          CartItemsIds(productId: item.product.toString());
                      orderItems.add(itemOrder);
                    }
                    PostCartIds postOrderItems =
                        PostCartIds(cartItemsIds: orderItems);
                    if (cartState.cartItems.isNotEmpty) {
                      BlocProvider.of<CartIdsBloc>(context).add(
                          CartItemsIdsEvent(
                              postCartIds: postOrderItems,
                              locale: '${LocalStorageService().currentLang}'));
                    }
                  },
                  child: BlocConsumer<CartIdsBloc, CartIdsState>(
                    listener: (context, cartState) {
                      if (cartState is LoadedCartIdsState) {
                        BlocProvider.of<CartBloc>(context).add(UpdateCartItems(
                            updatedProducts: cartState.products));
                      }
                    },
                    builder: (context, state) {
                      return BlocBuilder<CartBloc, CartState>(
                        builder: (context, state) {
                          if (state.cartItems.isEmpty) {
                            return Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Center(
                                      child: LottieWidget(
                                    lottiePath: Assets.emptyCartLottie,
                                    size: 150,
                                  )),
                                  Center(
                                    child: Text(
                                      'empty_cart'.tr(context),
                                      style: AppTextStyle.subtitleBold,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          } else {
                            return Expanded(
                              child: Column(
                                children: [
                                  Expanded(
                                    flex: 100,
                                    child: ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: state.cartItems.length,
                                        padding:
                                            TranslateX.isRtlLanguage(context)
                                                ? const EdgeInsets.only(
                                                    top: 20.0,
                                                    bottom: 80,
                                                    right: 4,
                                                    left: 15,
                                                  )
                                                : const EdgeInsets.only(
                                                    top: 20.0,
                                                    bottom: 80,
                                                    right: 15,
                                                    left: 4,
                                                  ),
                                        itemBuilder: (BuildContext context,
                                                int index) =>
                                            CustomCard(
                                                pageType: PageType.cart,
                                                product: state.cartItems[index],
                                                runAddToCartAnimation:
                                                    runAddToCartAnimation,
                                                runRemoveFromCartAnimation:
                                                    runRemoveFromCartAnimation)),
                                  ),
                                  const Spacer(
                                    flex: 1,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8.0),
                                    child: Divider(
                                      color: AppColors.grayColor,
                                      height: 2,
                                      thickness: 1.5,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 30.0, vertical: 8),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('total'.tr(context),
                                            style: AppTextStyle.titleBoldDark),
                                        Text(
                                            '€ ${Config.formatPrice(state.totalPrice)}',
                                            style: AppTextStyle.titleBoldDark),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: SizedBox(
                                          width: double.infinity,
                                          child: CustomButton(
                                              text: "order".tr(context),
                                              onPressed: () {
                                                showDialog<void>(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      title: Text(
                                                        'orderConfirm_message'
                                                            .tr(context),
                                                        style: AppTextStyle
                                                            .titleBoldDark,
                                                      ),
                                                      actions: [
                                                        TextButton(
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                            List<OrderItems>
                                                                orderItems = [];
                                                            for (var item
                                                                in state
                                                                    .cartItems) {
                                                              OrderItems itemOrder = OrderItems(
                                                                  productId: item
                                                                      .product
                                                                      .toString(),
                                                                  amount: item
                                                                      .quantity
                                                                      .toString());
                                                              orderItems.add(
                                                                  itemOrder);
                                                            }
                                                            PostOrderItems
                                                                postOrderItems =
                                                                PostOrderItems(
                                                                    orderItems:
                                                                        orderItems);
                                                            BlocProvider.of<
                                                                        OrderBloc>(
                                                                    context)
                                                                .add(PostOrderEvent(
                                                                    postOrderItems:
                                                                        postOrderItems));
                                                          },
                                                          child: Text(
                                                            'confirm'
                                                                .tr(context),
                                                            style: AppTextStyle
                                                                .subtitleBold,
                                                          ),
                                                        ),
                                                        TextButton(
                                                          onPressed: () =>
                                                              Navigator.of(
                                                                      context)
                                                                  .pop(),
                                                          child: Text(
                                                              'cancel'
                                                                  .tr(context),
                                                              style: AppTextStyle
                                                                  .subtitleBold
                                                                  .copyWith(
                                                                      color: AppColors
                                                                          .redColor)),
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                              }))),
                                ],
                              ),
                            );
                          }
                        },
                      );
                    },
                  ),
                );
              })
            ],
          ),
        ),
      )),
    );
  }
}
