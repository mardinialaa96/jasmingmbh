import 'package:equatable/equatable.dart';


class Item extends Equatable {
  const Item(this.id,this.qr);

  final int id;
  final int qr;
  final String name = '';
  final int price = 42;

  @override
  List<Object> get props => [id, qr,name, price];
}