part of 'category_bloc.dart';

@immutable
abstract class CategoryEvent extends Equatable {
  const CategoryEvent();

  @override
  List<Object?> get props => [];
}

class GetAllCategoryEvent extends CategoryEvent {
  final String locale;
  final int page;
  const GetAllCategoryEvent({required this.locale, required this.page});
  @override
  List<Object?> get props => [locale, page];
}

class LoadMoreCategoryEvent extends CategoryEvent {
  final String locale;
  final int page;
  const LoadMoreCategoryEvent({required this.locale, required this.page});

  @override
  List<Object?> get props => [locale, page];
}
