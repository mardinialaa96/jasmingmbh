part of 'category_bloc.dart';

abstract class CategoryState extends Equatable {
  const CategoryState();
  @override
  List<Object?> get props => [];
}

class CategoryInitial extends CategoryState {
  @override
  List<Object> get props => [];
}

class LoadingCategoryState extends CategoryState {
  @override
  List<Object> get props => [];
}

class LoadingInitCategoryState extends CategoryState {
  @override
  List<Object> get props => [];
}

class EmptyCategoryState extends CategoryState {
  @override
  List<Object> get props => [];
}

class LoadedCategoryState extends CategoryState {
  final List<CategoryElement> category;

  const LoadedCategoryState({required this.category});
  @override
  List<Object?> get props => [category];
}

class ErrorCategoryState extends CategoryState {
  final String message;

  const ErrorCategoryState({required this.message});
  @override
  List<Object?> get props => [message];
}
