import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';
import 'package:jasmingmbh/features/home_category/domain/usecases/category_usecases.dart';
import 'package:meta/meta.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  final CategoryUseCase categoryUseCase;

  CategoryBloc({required this.categoryUseCase}) : super(CategoryInitial()) {
    on<CategoryEvent>((event, emit) async {
      if (event is GetAllCategoryEvent) {
        emit(LoadingInitCategoryState());
        try{
          final failureOrDoneMessage =
          await categoryUseCase(event.page, event.locale);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorCategoryState(message: serverFailureMessage));
        }
      } else if (event is LoadMoreCategoryEvent) {
        emit(LoadingCategoryState());
        try{
          final result = await categoryUseCase(event.page, event.locale);
          result.fold((failure) {
            emit(ErrorCategoryState(message: _mapFailureToMessage(failure)));
          }, (moreCategory) {
            emit(LoadedCategoryState(category: moreCategory));
          });
        } catch (e) {
          emit(const ErrorCategoryState(message: serverFailureMessage));
        }
      }
    });
  }

  CategoryState _mapFailureOrSuccessToState(
      Either<Failure, List<CategoryElement>> either) {
    return either.fold(
        (failure) => ErrorCategoryState(message: _mapFailureToMessage(failure)),
        (productInfo) {
      if (productInfo.isEmpty) {
        return EmptyCategoryState();
      } else {
        return LoadedCategoryState(category: productInfo);
      }
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
