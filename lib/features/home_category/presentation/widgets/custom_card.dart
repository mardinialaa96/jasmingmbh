import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/widgets/global_function.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';
import 'package:marquee/marquee.dart';

class CustomCard extends StatelessWidget {
  final CategoryElement blocCategory;
  const CustomCard({super.key, required this.blocCategory});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 250,
        child: Card(
          elevation: 5,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          margin: const EdgeInsets.only(top: 80),
          child: Stack(
            children: [
              Positioned.fill(
                child: Container(
                  color: AppColors.whiteColor,
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(height: 60),
                      Expanded(
                        child:
                            GlobalFunction().isTextLong(blocCategory.name, 14)
                                ? Marquee(
                                    text: blocCategory.name,
                                    style: AppTextStyle.titleDark,
                                    pauseAfterRound: const Duration(seconds: 1),
                                    blankSpace: 5.0)
                                : Text(
                                    blocCategory.name,
                                    style: AppTextStyle.titleDark,
                                  ),
                      ),
                      const SizedBox(height: 3),
                      Expanded(
                        child: Text(
                          blocCategory.productCount == 0
                              ? "${blocCategory.productCount} ${'item'.tr(context)}"
                              : "${blocCategory.productCount} ${'items'.tr(context)}",
                          style: AppTextStyle.subtitleBold,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
