import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/widgets/global_function.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/change_password_page.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/login_page.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/profile_page.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';
import 'package:jasmingmbh/features/notification/presentation/pages/notification_page.dart';
import 'package:jasmingmbh/features/terms_of_service/presentation/pages/terms_of_service.dart';

class MyDrawer{
  Widget buildDrawer(BuildContext context) {
    return Drawer(
      backgroundColor: AppColors.drawerBackgroundColor,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  DrawerHeader(
                      child: Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColors.drawerBackgroundColor,
                                border: Border.all(color: AppColors.appColor)),
                            child: const Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Icon(
                                Icons.person,
                                size: 60,
                                color: AppColors.appColor,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(
                            '${LocalStorageService().firstName}  ${LocalStorageService().lastName}',
                            style: AppTextStyle.titleBoldDark,
                          )
                        ],
                      )),
                  ListTile(
                    leading: const Icon(Icons.notifications,
                        color: AppColors.drawerIconColor),
                    title: Text('notification'.tr(context),
                        style: AppTextStyle.drawerText),
                    onTap: () => Navigator.of(context,rootNavigator: true).push(
                        CupertinoPageRoute(
                            builder: (BuildContext context) =>
                            const NotificationPage())),
                  ),
                  const Divider(color: AppColors.grayColor),
                  ListTile(
                    leading: const Icon(Icons.person_pin_outlined,
                        color: AppColors.drawerIconColor),
                    title: Text('my_profile'.tr(context),
                        style: AppTextStyle.drawerText),
                    onTap: () => Navigator.of(context,rootNavigator: true).push(
                        CupertinoPageRoute(
                            builder: (BuildContext context) =>
                            const ProfilePage())),
                  ),
                  ListTile(
                    leading:
                    const Icon(Icons.lock, color: AppColors.drawerIconColor),
                    title: Text('password'.tr(context),
                        style: AppTextStyle.drawerText),
                    onTap: () => Navigator.of(context,rootNavigator: true).push(
                        CupertinoPageRoute(
                            builder: (BuildContext context) =>
                            const ChangePasswordPage())),
                  ),
                  const Divider(color: AppColors.grayColor),
                  ListTile(
                    leading: const Icon(Icons.language,
                        color: AppColors.drawerIconColor),
                    title: Text('change_language'.tr(context),
                        style: AppTextStyle.drawerText),
                    onTap: () => _showAlertDialog(context),
                  ),
                  ListTile(
                    leading: const Icon(Icons.file_copy,
                        color: AppColors.drawerIconColor),
                    title: Text('privacy_policy'.tr(context), style: AppTextStyle.drawerText),
                    onTap: () => Navigator.of(context,rootNavigator: true).push(
                        CupertinoPageRoute(
                            builder: (BuildContext context) =>
                            const TermsOfService())),
                  ),
                  ListTile(
                    onTap: () => _showAlertDialogSignOut(context),
                    leading: const Icon(Icons.exit_to_app_outlined,
                        color: AppColors.drawerIconColor),
                    title: Text('sign_out'.tr(context),
                        style: AppTextStyle.drawerText),
                  ),
                  const Divider(color: AppColors.grayColor),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: 'Powered by ',
                        style: AppTextStyle.titleDark,
                      ),
                      TextSpan(
                        text: 'Vision',
                        style: AppTextStyle.drawerText
                            .copyWith(color: AppColors.appColor),
                        recognizer: TapGestureRecognizer()
                          ..onTap = GlobalFunction.launchURL,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _showAlertDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return BlocBuilder<LocalCubit, LocalState>(
          builder: (context, state) {
            if (state is ChangeLocaleState) {
              return AlertDialog(
                title: Text(
                  'choose_language'.tr(context),
                  style: AppTextStyle.titleBoldDark,
                ),
                content: SingleChildScrollView(
                  child: Column(
                    children: ['de', 'ar', 'en']
                        .map(
                          (e) => GestureDetector(
                        onTap: () {
                          context.read<LocalCubit>().changeLanguage(e);
                          Navigator.of(context).pop();
                        },
                        child: ListTile(
                          title: Text(
                              e == 'de'
                                  ? 'Deutsch'
                                  : e == 'ar'
                                  ? 'العربية'
                                  : 'English',
                              style: AppTextStyle.drawerText),
                          leading: Radio(
                            fillColor:
                            MaterialStateProperty.all(AppColors.appColor),
                            value: e,
                            groupValue: state.locale.languageCode,
                            onChanged: (_) {
                              context.read<LocalCubit>().changeLanguage(e);
                              Navigator.of(context).pop();
                            },
                          ),
                        ),
                      ),
                    )
                        .toList(),
                  ),
                ),
              );
            }
            return const SizedBox();
          },
        );
      },
    );
  }

  _showAlertDialogSignOut(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'signOut_message'.tr(context),
            style: AppTextStyle.titleBoldDark,
          ),
          actions: [
            TextButton(
              onPressed: () {
                LocalStorageService().isLoggedIn = null;
                LocalStorageService().isFirstName = null;
                LocalStorageService().isLastName = null;
                BlocProvider.of<CartBloc>(context).add(ClearCart());
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => const LogInPage(),
                    ),
                        (Route<dynamic> route) => false);
              },
              child: Text(
                'confirm'.tr(context),
                style: AppTextStyle.subtitleBold,
              ),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('cancel'.tr(context),
                  style: AppTextStyle.subtitleBold
                      .copyWith(color: AppColors.redColor)),
            ),
          ],
        );
      },
    );
  }
}