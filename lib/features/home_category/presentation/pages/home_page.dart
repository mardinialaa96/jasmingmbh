import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/no_internet_retry_widget.dart';
import 'package:jasmingmbh/core/widgets/search_section.dart';
import 'package:jasmingmbh/core/widgets/shimmer_widget.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';
import 'package:jasmingmbh/features/home_category/presentation/bloc/category_bloc/category_bloc.dart';
import 'package:jasmingmbh/features/home_category/presentation/widgets/custom_card.dart';
import 'package:jasmingmbh/features/home_category/presentation/widgets/my_drawer.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';
import 'package:jasmingmbh/features/products/presentation/pages/products_page.dart';
import 'package:jumping_dot/jumping_dot.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  final ScrollController _scrollController = ScrollController();
  int _pageNumber = 1;
  late List<CategoryElement> _category;
  bool _isLoading = false;
  bool _hasMoreData = true;
  bool _isLoadingFirst = false;
  bool _isEmptyProducts = false;
  bool _noInternet = false;
  bool _retry = false;
  @override
  void initState() {
    super.initState();
    _category = [];
    _fetchProducts();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !_isLoading &&
        _hasMoreData) {
      _pageNumber++;
      BlocProvider.of<CategoryBloc>(context).add(LoadMoreCategoryEvent(
        locale: '${LocalStorageService().currentLang}',
        page: _pageNumber,
      ));
    }
  }

  void _fetchProducts() {
    BlocProvider.of<CategoryBloc>(context).add(GetAllCategoryEvent(
        locale: '${LocalStorageService().currentLang}', page: _pageNumber));
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final crossAxisCount =  screenWidth < 600 ? 2 : 4;
    const cardHeight = 250;
    final childAspectRatio = screenWidth / (cardHeight * crossAxisCount);
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          controller: _scrollController,
          shrinkWrap: true,
          slivers: <Widget>[
            SliverAppBar(
              leading: Container(),
              expandedHeight: 100,
              flexibleSpace: const Padding(
                padding: EdgeInsets.all(10.0),
                child: SearchSection(isHomePage: true),
              ),
            ),
            SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                sliver: BlocListener<LocalCubit, LocalState>(
                  listener: (context, state) {
                    if (state is LanguageChanged) {
                      _category = [];
                      _pageNumber = 1;
                      _fetchProducts();
                    }
                  },
                  child: BlocConsumer<CategoryBloc, CategoryState>(
                    builder: (context, state) {
                      return _isLoadingFirst
                          ? SliverFillRemaining(
                              child: ShimmerWidget.shimmerCategory(screenWidth))
                          : _isEmptyProducts
                              ? SliverToBoxAdapter(
                                  child: Center(
                                    child: Text('category_isEmpty'.tr(context),
                                        style: AppTextStyle.titleBoldDark),
                                  ),
                                )
                              : _noInternet || _retry
                                  ? SliverFillRemaining(
                                      child: NoInternetRetryWidget(
                                          isNoInternet: _noInternet,
                                          onTap: () {
                                            _category = [];
                                            _pageNumber = 1;
                                            _fetchProducts();
                                          }),
                                    )
                                  : SliverGrid(
                                      delegate: SliverChildBuilderDelegate(
                                        (context, index) {
                                          if (index < _category.length) {
                                            return GestureDetector(
                                              onTap: () => Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (BuildContext
                                                              context) =>
                                                          ProductsPage(
                                                              categoryId:
                                                                  _category[
                                                                          index]
                                                                      .category))),
                                              child: Stack(
                                                alignment: Alignment.topCenter,
                                                children: [
                                                  CustomCard(
                                                      key: UniqueKey(),
                                                      blocCategory:
                                                          _category[index]),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            10),
                                                    height: 130,
                                                    child: CachedNetworkImage(
                                                      imageUrl: _category[index]
                                                          .image,
                                                      placeholder:
                                                          (context, url) =>
                                                              Center(
                                                        child: JumpingDots(
                                                          color: AppColors
                                                              .appColor,
                                                          radius: 8,
                                                          numberOfDots: 3,
                                                        ),
                                                      ),
                                                      errorWidget: (context,
                                                              url, error) =>
                                                          const Icon(
                                                              Icons.error),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          } else {
                                            if (_isLoading &&
                                                _hasMoreData &&
                                                !_isLoadingFirst) {
                                              // Display loading indicator at the bottom
                                              return const SliverToBoxAdapter(
                                                child: Center(
                                                  child:
                                                      CircularProgressIndicator(
                                                    color: AppColors.appColor,
                                                  ),
                                                ),
                                              );
                                            } else {
                                              return const SliverToBoxAdapter(
                                                child: SizedBox(),
                                              );
                                            }
                                          }
                                        },
                                        childCount: _category.length,
                                      ),
                                      gridDelegate:
                                           SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: crossAxisCount,
                                              crossAxisSpacing: 10,
                                              mainAxisSpacing: 10,
                                              childAspectRatio: childAspectRatio),
                                    );
                    },
                    listener: (context, state) {
                      if (state is LoadingCategoryState) {
                        setState(() {
                          _isLoading = true;
                        });
                      } else if (state is LoadingInitCategoryState) {
                        setState(() {
                          _isLoadingFirst = true;
                        });
                      } else if (state is EmptyCategoryState) {
                        setState(() {
                          _isLoadingFirst = false;
                          _isEmptyProducts = true;
                        });
                      } else if (state is LoadedCategoryState) {
                        setState(() {
                          _isLoading = false;
                          _isLoadingFirst = false;
                          _isEmptyProducts = false;
                          _category.addAll(state.category);
                          _noInternet = false;
                          _retry = false;
                          _hasMoreData = state.category.isNotEmpty;
                        });
                      } else if (state is ErrorCategoryState) {
                        if (state.message == offlineFailureMessage) {
                          _noInternet = true;
                          _isLoadingFirst = false;
                        } else {
                          _retry = true;
                          _isLoadingFirst = false;
                          return SnackBarMessage().showErrorSnackBarMessage(
                              message: state.message == serverFailureMessage
                                  ? 'try_again'.tr(context)
                                  : state.message, context: context);
                        }
                      }
                    },
                  ),
                )),
            const SliverPadding(
              padding: EdgeInsets.only(bottom: 100),
            ),
          ],
        ),
      ),
      drawer: MyDrawer().buildDrawer(context),
    );
  }
}
