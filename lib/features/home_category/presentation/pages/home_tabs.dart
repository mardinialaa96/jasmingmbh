
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/add_to_cart_animation/add_to_cart_animation.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/profile_page.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:jasmingmbh/features/cart/presentation/pages/cart_page.dart';
import 'package:jasmingmbh/features/home_category/presentation/pages/home_page.dart';
import 'package:jasmingmbh/features/offers/presentation/pages/offers_page.dart';
import 'package:jasmingmbh/features/our_team/presentation/pages/our_team_page.dart';
import 'package:jasmingmbh/main.dart';
import 'package:badges/badges.dart' as badges;

class HomeTabs extends StatefulWidget {
  const HomeTabs({
    super.key,
  });

  @override
  HomeUserTabsState createState() => HomeUserTabsState();
}

class HomeUserTabsState extends State<HomeTabs>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      vsync: this,
      length: 5,
      initialIndex: currentIndex,
    );

    _tabController.addListener(() {
      setState(() {
        currentIndex = _tabController.index;
      });
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        backgroundColor: AppColors.backgroundColor,
        height: kBottomNavigationBarHeight + 8,
        items: <BottomNavigationBarItem>[
          _buildBottomNavigationBarItem(Assets.home, currentIndex == 0),
          _buildBottomNavigationBarItem(Assets.offers, currentIndex == 1),
          _buildBottomNavigationBarItem(Assets.cart, currentIndex == 2,
              isCart: true),
          _buildBottomNavigationBarItem(Assets.chat, currentIndex == 3),
          _buildBottomNavigationBarItem(Assets.user, currentIndex == 4),
        ],
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
          _tabController.animateTo(index);
        },
      ),
      tabBuilder: (context, index) {
        return CupertinoTabView(
          builder: (context) {
            switch (index) {
              case 0:
                return const CupertinoPageScaffold(
                  child: HomePage(),
                );
              case 1:
                return const CupertinoPageScaffold(
                  child: OffersPage(),
                );
              case 2:
                return const CupertinoPageScaffold(
                  child: CartPage(),
                );
              case 3:
                return const CupertinoPageScaffold(
                  child: OurTeamPage(),
                );
              case 4:
                return const CupertinoPageScaffold(
                  child: ProfilePage(isTabs: true),
                );
              default:
                return const CupertinoPageScaffold(
                  child: HomePage(),
                );
            }
          },
        );
      },
    );
  }

  BottomNavigationBarItem _buildBottomNavigationBarItem(
      String imagePath, bool isActive,
      {bool isCart = false}) {
    return BottomNavigationBarItem(
      backgroundColor: AppColors.whiteColor,
      icon: isCart
          ? Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: isActive ? AppColors.appColor : Colors.transparent,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: BlocBuilder<CartBloc, CartState>(
                    builder: (context, state) {
                      int cartItemCount = state.cartItems.length;
                      return AddToCartIcon(
                        key: cartKey,
                        icon: badges.Badge(
                          position: badges.BadgePosition.topStart(
                              top: -15, start: -12),
                          showBadge: cartItemCount > 0,
                          badgeContent: Text(
                            '$cartItemCount',
                            style: AppTextStyle.miniText,
                          ),
                          badgeStyle: const badges.BadgeStyle(
                            shape: badges.BadgeShape.circle,
                            badgeColor: AppColors.appColor,
                            padding: EdgeInsets.all(5),
                          ),
                          child: Image.asset(
                            imagePath,
                            width: 25, // Adjust size of the image
                            height: 25, // Adjust size of the image
                            color: isActive
                                ? AppColors.selectedNavBarIcon
                                : AppColors.unSelectedNavBarIcon,
                          ),
                        ),
                        badgeOptions: const BadgeOptions(
                          active: false,
                        ),
                      );
                    },
                  ),
                ),
              ],
            )
          : Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width:
                      40, // Adjust size as needed to fit the circular background
                  height:
                      40, // Adjust size as needed to fit the circular background
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: isActive ? AppColors.appColor : Colors.transparent,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Image.asset(
                    imagePath,
                    width: 25, // Adjust size of the image
                    height: 25, // Adjust size of the image
                    color: isActive
                        ? AppColors.selectedNavBarIcon
                        : AppColors.unSelectedNavBarIcon,
                  ),
                ),
              ],
            ),
      label: '',
    );
  }
}
