import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/data_category_info.dart';

class Category extends Equatable {
  final DataCategoryInfo? dataCategoryInfo;
  final String? msg;
  final int status;
  const Category(
      {required this.dataCategoryInfo, required this.msg, required this.status});

  @override
  List<Object?> get props => [dataCategoryInfo, msg, status];
}
