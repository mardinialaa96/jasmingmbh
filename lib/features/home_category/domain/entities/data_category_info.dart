import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';

class DataCategoryInfo extends Equatable {
  final List<CategoryElement>? categories;
  const DataCategoryInfo({required this.categories});

  @override
  List<Object?> get props => [categories];
}
