import 'package:equatable/equatable.dart';

class CategoryElement extends Equatable {
  final int category;
  final String image;
  final String name;
  final int productCount;
  const CategoryElement(
      {required this.category,
      required this.image,
      required this.name,
      required this.productCount});

  @override
  List<Object?> get props => [category, image, name, productCount];
}
