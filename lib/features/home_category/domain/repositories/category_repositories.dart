import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';

abstract class CategoryRepository {
  Future<Either<Failure, List<CategoryElement>>> getAllCategory(int page,String locale);
}
