import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';
import 'package:jasmingmbh/features/home_category/domain/repositories/category_repositories.dart';

class CategoryUseCase {
  final CategoryRepository repository;

  CategoryUseCase(this.repository);

  Future<Either<Failure, List<CategoryElement>>> call(int page,String locale) async {
    return await repository.getAllCategory(page,locale);
  }
}
