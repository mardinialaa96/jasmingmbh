import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/home_category/data/models/category_model.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';
import 'package:jasmingmbh/main.dart';

abstract class CategoryRemoteDataSource {
  Future<List<CategoryElement>> getAllCategory(int pageNumber, String locale);
}

class CategoryRemoteDataSourceImpl implements CategoryRemoteDataSource {
  final http.Client client;

  CategoryRemoteDataSourceImpl({required this.client});

  @override
  Future<List<CategoryElement>> getAllCategory(int page, String locale) async {
    print('page $page ........ locale $locale');
    final queryParameters = {
      'page': page.toString(),
      'locale': locale,
    };
    final token = LocalStorageService().token;
    if (token == null) {
      throw ServerException();
    }
    final url = Uri.parse('$baseUrl/categories');
    final response = await client
        .get(url.replace(queryParameters: queryParameters), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    print('token ${LocalStorageService().token}');
    print('response.statusCode ${response.statusCode}');
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      CategoryModel categoryModel = CategoryModel.fromJson(decodeJson);
      print('response.body $categoryModel');
      print('length ${categoryModel.dataCategoryInfo?.categories?.length}');
      if (categoryModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
            (route) => false);
        return categoryModel.dataCategoryInfo?.categories ?? [];
      } else {
        return categoryModel.dataCategoryInfo?.categories ?? [];
      }
    } else {
      throw ServerException();
    }
  }
}
