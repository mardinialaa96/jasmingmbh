import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/home_category/data/datasources/category_remote_data_source.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';
import 'package:jasmingmbh/features/home_category/domain/repositories/category_repositories.dart';

class CategoryRepositoryImpl implements CategoryRepository {
  final CategoryRemoteDataSource categoryRemoteDataSource;
  final NetworkInfo networkInfo;

  CategoryRepositoryImpl(
      {required this.categoryRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, List<CategoryElement>>> getAllCategory(
      int page, String locale) async {
    if (await networkInfo.isConnected) {
      try {
        final categoryInfo =
            await categoryRemoteDataSource.getAllCategory(page, locale);
        return Right(categoryInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
