import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';

class CategoryElementModel extends CategoryElement {
  const CategoryElementModel(
      {required super.category,
      required super.image,
      required super.name,
      required super.productCount});

  factory CategoryElementModel.fromJson(Map<String, dynamic> json) =>
      CategoryElementModel(
        category: json["category"],
        image: json["image"],
        name: json["name"],
        productCount: json["productCount"],
      );
}
