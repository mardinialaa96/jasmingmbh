import 'package:jasmingmbh/features/home_category/data/models/category_element_model.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/data_category_info.dart';

class DataCategoryInfoModel extends DataCategoryInfo {
  const DataCategoryInfoModel({required super.categories});

  factory DataCategoryInfoModel.fromJson(Map<String, dynamic> json) =>
      DataCategoryInfoModel(
        categories: json["categories"] == null
            ? []
            : List<CategoryElementModel>.from(json["categories"]!
                .map((x) => CategoryElementModel.fromJson(x))),
      );
}
