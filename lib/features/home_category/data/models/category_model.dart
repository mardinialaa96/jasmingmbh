import 'package:jasmingmbh/features/home_category/data/models/data_category_info_model.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category.dart';


class CategoryModel extends Category {
  const CategoryModel(
      {required super.dataCategoryInfo, required super.msg, required super.status});
  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
      dataCategoryInfo: DataCategoryInfoModel.fromJson(json["data"]),
      msg: json["msg"],
      status: json["status"],
    );
  }
}
