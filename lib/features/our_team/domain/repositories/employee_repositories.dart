import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';

abstract class EmployeesRepository {
  Future<Either<Failure, List<EmployeesElement>>> getAllEmployees(
      String locale);
}
