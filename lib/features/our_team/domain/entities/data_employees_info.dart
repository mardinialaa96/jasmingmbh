import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/home_category/domain/entities/category_element.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';

class DataEmployeesInfo extends Equatable {
  final List<EmployeesElement> employees;
  const DataEmployeesInfo({required this.employees});

  @override
  List<Object?> get props => [employees];
}
