import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/data_employees_info.dart';

class Employees extends Equatable {
  final DataEmployeesInfo dataEmployeesInfo;
  final String msg;
  final int status;
  const Employees(
      {required this.dataEmployeesInfo,
      required this.msg,
      required this.status});

  @override
  List<Object?> get props => [dataEmployeesInfo, msg, status];
}
