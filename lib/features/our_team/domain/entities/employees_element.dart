import 'package:equatable/equatable.dart';

class EmployeesElement extends Equatable {
  final String image;
  final String mobile;
  final String job;
  const EmployeesElement(
      {required this.image, required this.mobile, required this.job});

  @override
  List<Object?> get props => [image, mobile, job];
}
