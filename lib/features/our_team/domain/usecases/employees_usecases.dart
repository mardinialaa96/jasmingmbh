import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';
import 'package:jasmingmbh/features/our_team/domain/repositories/employee_repositories.dart';

class EmployeesUseCase {
  final EmployeesRepository repository;

  EmployeesUseCase(this.repository);

  Future<Either<Failure, List<EmployeesElement>>> call(String locale) async {
    return await repository.getAllEmployees(locale);
  }
}
