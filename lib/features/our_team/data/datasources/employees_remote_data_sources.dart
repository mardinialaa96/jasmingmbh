import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/our_team/data/models/employees_model.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';
import 'package:jasmingmbh/main.dart';

abstract class EmployeesRemoteDataSource {
  Future<List<EmployeesElement>> getAllEmployees(String locale);
}

class EmployeesRemoteDataSourceImpl implements EmployeesRemoteDataSource {
  final http.Client client;

  EmployeesRemoteDataSourceImpl({required this.client});

  @override
  Future<List<EmployeesElement>> getAllEmployees(String locale) async {
    final queryParameters = {
      'locale': locale,
    };
    final token = LocalStorageService().token;
    if (token == null) {
      throw ServerException();
    }
    final url = Uri.parse('$baseUrl/team');
    final response = await client
        .get(url.replace(queryParameters: queryParameters), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    print('token ${LocalStorageService().token}');
    print('response.statusCode ${response.statusCode}');
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      EmployeesModel employeesModel = EmployeesModel.fromJson(decodeJson);
      print('response.body $employeesModel');
      print('length ${employeesModel.dataEmployeesInfo.employees.length}');
      if (employeesModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
                (route) => false);
        return employeesModel.dataEmployeesInfo.employees;
      } else {
        return employeesModel.dataEmployeesInfo.employees;
      }
    } else {
      throw ServerException();
    }
  }
}
/*import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/our_team/data/models/employees_model.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';
import 'package:jasmingmbh/main.dart';

abstract class EmployeesRemoteDataSource {
  Future<List<EmployeesElement>> getAllEmployees(String locale);
}

class EmployeesRemoteDataSourceImpl implements EmployeesRemoteDataSource {
  final Dio _dio;

  EmployeesRemoteDataSourceImpl({required Dio dio}) : _dio = dio;

  @override
  Future<List<EmployeesElement>> getAllEmployees(String locale) async {
    final queryParameters = {'locale': locale};
    final token = LocalStorageService().token;
    if (token == null) {
      throw ServerException();
    }

    try {
      Response response = await _dio.get(
        '$baseUrl/team',
        queryParameters: queryParameters,
        options: Options(
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          },
        ),
      );

      print('response.statusCode ${response.statusCode}');

      if (response.statusCode == 200) {
        var decodeJson = response.data;
        EmployeesModel employeesModel = EmployeesModel.fromJson(decodeJson);

        print('response.body $employeesModel');
        print('length ${employeesModel.dataEmployeesInfo.employees.length}');

        if (employeesModel.status == 2000) {
          navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
                (route) => false,
          );
        }

        return employeesModel.dataEmployeesInfo.employees;
      } else {
        throw ServerException(); // Throw exception for non-200 status codes
      }
    } catch (e) {

      const maxRetryCount = 3;
      const retryInterval = Duration(seconds: 2);
      int retryCount = 0;

      while (retryCount < maxRetryCount) {
        retryCount++;
        try {
          Response response = await _dio.get(
            '$baseUrl/team',
            queryParameters: queryParameters,
            options: Options(
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer $token',
              },
            ),
          );

          print('Retry $retryCount - response.statusCode ${response.statusCode}');

          if (response.statusCode == 200) {
            var decodeJson = response.data;
            EmployeesModel employeesModel = EmployeesModel.fromJson(decodeJson);

            print('Retry $retryCount - response.body $employeesModel');
            print('Retry $retryCount - length ${employeesModel.dataEmployeesInfo.employees.length}');

            if (employeesModel.status == 2000) {
              navigatorKey.currentState!.pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => const BlockPage()),
                    (route) => false,
              );
            }

            return employeesModel.dataEmployeesInfo.employees;
          } else {
            throw ServerException(); // Throw exception for non-200 status codes
          }
        } catch (e) {
          // Handle errors or retry based on specific conditions
          print('Retry $retryCount - Error: $e');
          await Future.delayed(retryInterval);
        }
      }

      throw ServerException(); // If retries fail, throw ServerException
    }
  }
}*/


