import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/our_team/data/datasources/employees_remote_data_sources.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';
import 'package:jasmingmbh/features/our_team/domain/repositories/employee_repositories.dart';

class EmployeesRepositoryImpl implements EmployeesRepository {
  final EmployeesRemoteDataSource employeesRemoteDataSource;
  final NetworkInfo networkInfo;

  EmployeesRepositoryImpl(
      {required this.employeesRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, List<EmployeesElement>>> getAllEmployees(
      String locale) async {
    if (await networkInfo.isConnected) {
      try {
        final employeesInfo =
            await employeesRemoteDataSource.getAllEmployees(locale);
        return Right(employeesInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
