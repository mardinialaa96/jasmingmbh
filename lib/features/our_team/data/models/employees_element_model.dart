
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';

class EmployeesElementModel extends EmployeesElement {
  const EmployeesElementModel(
      {
        required super.image,
        required super.mobile,
        required super.job});

  factory EmployeesElementModel.fromJson(Map<String, dynamic> json) =>
      EmployeesElementModel(
        image: json["image"],
        mobile: json["mobile"],
        job: json["job"],
      );
}
