import 'package:jasmingmbh/features/our_team/data/models/data_employees_info_model.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees.dart';

class EmployeesModel extends Employees {
  const EmployeesModel(
      {required super.dataEmployeesInfo,
      required super.msg,
      required super.status});
  factory EmployeesModel.fromJson(Map<String, dynamic> json) {
    return EmployeesModel(
      dataEmployeesInfo: DataEmployeesInfoModel.fromJson(json["data"]),
      msg: json["msg"],
      status: json["status"],
    );
  }
}
