
import 'package:jasmingmbh/features/our_team/data/models/employees_element_model.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/data_employees_info.dart';

class DataEmployeesInfoModel extends DataEmployeesInfo {
  const DataEmployeesInfoModel({required super.employees});

  factory DataEmployeesInfoModel.fromJson(Map<String, dynamic> json) =>
      DataEmployeesInfoModel(
        employees: json["employees"] == null
            ? []
            : List<EmployeesElementModel>.from(json["employees"]!
                .map((x) => EmployeesElementModel.fromJson(x))),
      );
}
