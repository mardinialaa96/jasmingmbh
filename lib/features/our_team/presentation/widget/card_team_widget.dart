import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';
import 'package:jumping_dot/jumping_dot.dart';

class CardTeamWidget extends StatelessWidget {
  final EmployeesElement employee;
  const CardTeamWidget({super.key, required this.employee});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        color: AppColors.whiteColor,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
              child: Container(
                width: 100.0,
                height: 100.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: AppColors.appColor,
                  ),
                  shape: BoxShape.circle,
                ),
                child: ClipOval(
                  child: CachedNetworkImage(
                    imageUrl: employee.image,
                    placeholder: (context, url) => JumpingDots(
                      color: AppColors.appColor,
                      radius: 8,
                      numberOfDots: 3,
                    ),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Expanded(
                child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(employee.job, style: AppTextStyle.titleBoldDark),
                  const SizedBox(height: 5),
                  Text(employee.mobile, style: AppTextStyle.subtitleBold),
                  const SizedBox(height: 5),
                  Text('chat_whatsapp'.tr(context),
                      style: AppTextStyle.subtitleBold),
                ],
              ),
            )),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Icon(
                Icons.arrow_forward_outlined,
                color: AppColors.grayColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
