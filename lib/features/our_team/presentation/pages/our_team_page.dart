import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/widgets/empty_widget.dart';
import 'package:jasmingmbh/core/widgets/error_data_widget.dart';
import 'package:jasmingmbh/core/widgets/global_function.dart';
import 'package:jasmingmbh/core/widgets/shimmer_widget.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';
import 'package:jasmingmbh/features/our_team/presentation/bloc/employees_bloc/employees_bloc.dart';
import 'package:jasmingmbh/features/our_team/presentation/widget/card_team_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class OurTeamPage extends StatefulWidget {
  const OurTeamPage({super.key});

  @override
  State<OurTeamPage> createState() => OurTeamPageState();
}

class OurTeamPageState extends State<OurTeamPage> {
  @override
  void initState() {
    BlocProvider.of<EmployeesBloc>(context).add(GetAllEmployeesEvent(
      locale: '${LocalStorageService().currentLang}',
    ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                children: <Widget>[
                  Text('our_team'.tr(context),
                      style: AppTextStyle.titleExtraBoldDark),
                ],
              ),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: BlocListener<LocalCubit, LocalState>(
                listener: (context, state) {
                  BlocProvider.of<EmployeesBloc>(context)
                      .add(GetAllEmployeesEvent(
                    locale: '${LocalStorageService().currentLang}',
                  ));
                },
                child: BlocBuilder<EmployeesBloc, EmployeesState>(
                  builder: (context, state) {
                    if (state is LoadingEmployeesState) {
                      return ShimmerWidget.shimmerCard();
                    } else if (state is LoadedEmployeesState) {
                      if (state.employees.isEmpty) {
                        return EmptyWidget(
                            text: 'employees_isEmpty'.tr(context));
                      } else {
                        return ListView.builder(
                          padding: const EdgeInsets.only(bottom: 80),
                          itemCount: state.employees.length,
                          itemBuilder: (context, index) => Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: GestureDetector(
                              onTap: () async {
                                String phoneNumber = GlobalFunction()
                                    .removeDoubleZero(
                                        state.employees[index].mobile);
                                String webUrl = 'https://wa.me/$phoneNumber';
                                try {
                                  print(Uri.parse(webUrl));
                                  await launchUrl(Uri.parse(webUrl));
                                } catch (e) {
                                  print(
                                      'Could not open WhatsApp in the browser: $e');
                                }
                              },
                              child: CardTeamWidget(
                                  employee: state.employees[index]),
                            ),
                          ),
                        );
                      }
                    } else if (state is ErrorEmployeesState) {
                        return ErrorDataWidget(
                            text: state.message == offlineFailureMessage
                                ? 'no_internet'.tr(context)
                                : state.message == serverFailureMessage
                                ? 'try_again'.tr(context)
                                : state.message,
                            onTap: () {
                              BlocProvider.of<EmployeesBloc>(context)
                                  .add(GetAllEmployeesEvent(
                                locale: '${LocalStorageService().currentLang}',
                              ));
                            });

                    } else {
                      return const SizedBox();
                    }
                  },
                ),
              ),
            )),
          ],
        ),
      ),
    ));
  }
}
