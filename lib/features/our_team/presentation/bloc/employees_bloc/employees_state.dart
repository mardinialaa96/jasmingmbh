part of 'employees_bloc.dart';

@immutable
abstract class EmployeesState extends Equatable {}

class EmployeesInitial extends EmployeesState {
  @override
  List<Object> get props => [];
}

class LoadingEmployeesState extends EmployeesState {
  @override
  List<Object> get props => [];
}

class LoadedEmployeesState extends EmployeesState {
  final List<EmployeesElement> employees;

  LoadedEmployeesState({required this.employees});
  @override
  List<Object?> get props => [employees];
}

class ErrorEmployeesState extends EmployeesState {
  final String message;

  ErrorEmployeesState({required this.message});
  @override
  List<Object?> get props => [message];
}
