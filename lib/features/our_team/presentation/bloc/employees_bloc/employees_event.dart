part of 'employees_bloc.dart';

@immutable
abstract class EmployeesEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class GetAllEmployeesEvent extends EmployeesEvent {
  final String locale;
  GetAllEmployeesEvent({required this.locale});
  @override
  List<Object?> get props => [locale];
}
