

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/our_team/domain/entities/employees_element.dart';
import 'package:jasmingmbh/features/our_team/domain/usecases/employees_usecases.dart';
import 'package:meta/meta.dart';

part 'employees_event.dart';
part 'employees_state.dart';

class EmployeesBloc extends Bloc<EmployeesEvent, EmployeesState> {
  final EmployeesUseCase employeesUseCase;
  EmployeesBloc({required this.employeesUseCase}) : super(EmployeesInitial()) {
    on<EmployeesEvent>((event, emit) async {
      if (event is GetAllEmployeesEvent) {
        emit(LoadingEmployeesState());
        try{
          final failureOrDoneMessage = await employeesUseCase(event.locale);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(ErrorEmployeesState(message: serverFailureMessage));
        }
      }
    });
  }
}

EmployeesState _mapFailureOrSuccessToState(
    Either<Failure, List<EmployeesElement>> either) {
  return either.fold(
      (failure) => ErrorEmployeesState(message: _mapFailureToMessage(failure)),
      (employeesInfo) {
    return LoadedEmployeesState(employees: employeesInfo);
  });
}

String _mapFailureToMessage(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return serverFailureMessage;
    case EmptyCacheFailure:
      return emptyCacheFailureMessage;
    case OfflineFailure:
      return offlineFailureMessage;
    default:
      return 'Unexpected Error, Please try again later';
  }
}
