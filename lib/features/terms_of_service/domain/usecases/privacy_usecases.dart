import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/items_of_services_return_data.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/repositories/privacy_repositories.dart';


class PrivacyUseCase {
  final PrivacyRepository repository;

  PrivacyUseCase(this.repository);

  Future<Either<Failure, ItemsOfServicesReturnData>> call() async {
    return await repository.getPrivacy();
  }
}
