import 'package:equatable/equatable.dart';

class Privacy extends Equatable{
  final String? ar;
  final String? de;
  final String? en;

  const Privacy({
    this.ar,
    this.de,
    this.en,
  });
  @override
  List<Object?> get props => [ar, de, en];
}