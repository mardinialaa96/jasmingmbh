import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/data_privacy.dart';

class ItemsOfServicesReturnData extends Equatable{
  final DataPrivacy? data;
  final String? msg;
  final int? status;

  const ItemsOfServicesReturnData({
    this.data,
    this.msg,
    this.status,
  });
  @override
  List<Object?> get props => [data, msg, status];
}