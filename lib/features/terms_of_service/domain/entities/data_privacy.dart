import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/privacy.dart';

class DataPrivacy extends Equatable {
  final Privacy? privacy;
  const DataPrivacy({this.privacy});

  @override
  List<Object?> get props => [privacy];
}
