import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/items_of_services_return_data.dart';

abstract class PrivacyRepository {
  Future<Either<Failure, ItemsOfServicesReturnData>> getPrivacy();
}
