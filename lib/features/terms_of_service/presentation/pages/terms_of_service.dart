import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/widgets/error_data_widget.dart';
import 'package:jasmingmbh/features/terms_of_service/presentation/bloc/privacy_bloc.dart';

class TermsOfService extends StatefulWidget {
  const TermsOfService({super.key});

  @override
  State<TermsOfService> createState() => TermsOfServiceState();
}

class TermsOfServiceState extends State<TermsOfService> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<PrivacyBloc>(context).add(const GetPrivacyEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            backgroundColor: AppColors.appColor,
            expandedHeight: 100.0,
            flexibleSpace: FlexibleSpaceBar(
              background: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        const Icon(
                          Icons.file_copy,
                          color: AppColors.whiteColor,
                          size: 50,
                        ),
                        Expanded(
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0),
                            child: Text(
                              'privacy_policy'.tr(context),
                              style: const TextStyle(
                                  color: AppColors.whiteColor,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            leading: Container(),
            actions: [
              IconButton(
                icon: const Icon(
                  Icons.arrow_forward_ios,
                  color: AppColors.whiteColor,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(0),
              child: Container(
                  width: double.maxFinite,
                  decoration: const BoxDecoration(
                      color: AppColors.whiteColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: const Text("")),
            ),
          ),
          BlocBuilder<PrivacyBloc, PrivacyState>(
            builder: (context, state) {
              if (state is LoadingPrivacyState) {
                return const SliverFillRemaining(
                    child: Center(
                  child: CircularProgressIndicator(color: AppColors.appColor),
                ));
              } else if (state is ErrorPrivacyState) {
                  return SliverFillRemaining(
                      child: ErrorDataWidget(
                          text: state.message == offlineFailureMessage
                              ? 'no_internet'.tr(context)
                              : state.message == serverFailureMessage
                              ? 'try_again'.tr(context)
                              : state.message,
                          onTap: () {
                            BlocProvider.of<PrivacyBloc>(context)
                                .add(const GetPrivacyEvent());
                          }));

              } else if (state is LoadedPrivacyState) {
                return SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Center(
                      child: LocalStorageService().currentLang == 'ar'
                          ? Text(
                              "${state.itemsOfServicesReturnData.data?.privacy?.ar}",
                              style: AppTextStyle.titleDark)
                          : LocalStorageService().currentLang == 'de'
                              ? Text(
                                  "${state.itemsOfServicesReturnData.data?.privacy?.de}",
                                  style: AppTextStyle.titleDark)
                              : Text(
                                  "${state.itemsOfServicesReturnData.data?.privacy?.en}",
                                  style: AppTextStyle.titleDark),
                    ),
                  ),
                );
              } else {
                return const SliverToBoxAdapter();
              }
            },
          )
        ],
      ),
    );
  }
}
