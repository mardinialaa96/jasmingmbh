part of 'privacy_bloc.dart';

@immutable
sealed class PrivacyState extends Equatable {
  const PrivacyState();
  @override
  List<Object?> get props => [];
}

final class PrivacyInitial extends PrivacyState {}

class LoadingPrivacyState extends PrivacyState {}

class ErrorPrivacyState extends PrivacyState {
  final String message;

  const ErrorPrivacyState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedPrivacyState extends PrivacyState {
  final ItemsOfServicesReturnData itemsOfServicesReturnData;

  const LoadedPrivacyState({required this.itemsOfServicesReturnData});
  @override
  List<Object?> get props => [itemsOfServicesReturnData];
}
