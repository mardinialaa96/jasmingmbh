part of 'privacy_bloc.dart';

@immutable
sealed class PrivacyEvent extends Equatable {
  const PrivacyEvent();
  @override
  List<Object?> get props => [];
}

class GetPrivacyEvent extends PrivacyEvent {
  const GetPrivacyEvent();
  @override
  List<Object?> get props => [];
}
