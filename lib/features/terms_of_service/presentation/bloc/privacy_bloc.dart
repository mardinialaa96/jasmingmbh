import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/items_of_services_return_data.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/usecases/privacy_usecases.dart';
import 'package:meta/meta.dart';

part 'privacy_event.dart';
part 'privacy_state.dart';

class PrivacyBloc extends Bloc<PrivacyEvent, PrivacyState> {
  final PrivacyUseCase privacyUseCase;
  PrivacyBloc({required this.privacyUseCase}) : super(PrivacyInitial()) {
    on<PrivacyEvent>((event, emit) async {
      if (event is GetPrivacyEvent) {
        emit(LoadingPrivacyState());
        try {
          final failureOrDoneMessage = await privacyUseCase();
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorPrivacyState(message: serverFailureMessage));
        }
      }
    });
  }

  PrivacyState _mapFailureOrSuccessToState(
      Either<Failure, ItemsOfServicesReturnData> either) {
    return either.fold(
            (failure) =>
              ErrorPrivacyState(message: _mapFailureToMessage(failure)),
            (updateAppInfo) =>
            LoadedPrivacyState(itemsOfServicesReturnData: updateAppInfo));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
