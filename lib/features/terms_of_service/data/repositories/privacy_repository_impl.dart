import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/terms_of_service/data/datasources/privacy_remote_data_sources.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/items_of_services_return_data.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/repositories/privacy_repositories.dart';

class PrivacyRepositoryImpl implements PrivacyRepository {
  final PrivacyRemoteDataSource privacyRemoteDataSource;
  final NetworkInfo networkInfo;

  PrivacyRepositoryImpl(
      {required this.privacyRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, ItemsOfServicesReturnData>> getPrivacy() async {
    if (await networkInfo.isConnected) {
      try {
        final privacyInfo = await privacyRemoteDataSource.getPrivacy();
        return Right(privacyInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
