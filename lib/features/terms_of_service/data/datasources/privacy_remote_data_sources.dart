import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/terms_of_service/data/models/items_of_services_return_data_model.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/items_of_services_return_data.dart';
import 'package:jasmingmbh/main.dart';

abstract class PrivacyRemoteDataSource {
  Future<ItemsOfServicesReturnData> getPrivacy();
}

class PrivacyRemoteDataSourceImpl implements PrivacyRemoteDataSource {
  final http.Client client;

  PrivacyRemoteDataSourceImpl({required this.client});

  @override
  Future<ItemsOfServicesReturnData> getPrivacy() async {
    final url = Uri.parse('$baseUrl/privcy-policy');
    final response = await client.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      ItemsOfServicesReturnDataModel itemsOfServicesReturnDataModel =
          ItemsOfServicesReturnDataModel.fromJson(decodeJson);
      if (itemsOfServicesReturnDataModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
            (route) => false);
        return itemsOfServicesReturnDataModel;
      } else {
        return itemsOfServicesReturnDataModel;
      }
    } else {
      throw ServerException();
    }
  }
}
