import 'package:jasmingmbh/features/terms_of_service/data/models/privacy_model.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/data_privacy.dart';

class DataPrivacyModel extends DataPrivacy {
  const DataPrivacyModel({super.privacy});
  factory DataPrivacyModel.fromJson(Map<String, dynamic> json) =>
      DataPrivacyModel(
        privacy: PrivacyModel.fromJson(json["privcy"]),
      );
}
