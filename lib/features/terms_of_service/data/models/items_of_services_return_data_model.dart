import 'package:jasmingmbh/features/terms_of_service/data/models/data_privacy_model.dart';
import 'package:jasmingmbh/features/terms_of_service/domain/entities/items_of_services_return_data.dart';

class ItemsOfServicesReturnDataModel extends ItemsOfServicesReturnData {
  const ItemsOfServicesReturnDataModel({super.status, super.data, super.msg});

  factory ItemsOfServicesReturnDataModel.fromJson(Map<String, dynamic> json) =>
      ItemsOfServicesReturnDataModel(
        data: DataPrivacyModel.fromJson(json["data"]),
        msg: json["msg"],
        status: json["status"],
      );
}
