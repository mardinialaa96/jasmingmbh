import 'package:jasmingmbh/features/terms_of_service/domain/entities/privacy.dart';

class PrivacyModel extends Privacy {
  const PrivacyModel({super.ar, super.de, super.en});

  factory PrivacyModel.fromJson(Map<String, dynamic> json) => PrivacyModel(
        ar: json["ar"],
        en: json["en"],
        de: json["de"],
      );
}
