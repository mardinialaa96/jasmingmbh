import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/products/data/datasources/products_remore_data_sources.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/products/domain/repositories/products_repositories.dart';

class ProductsRepositoryImpl implements ProductsRepository {
  final ProductsRemoteDataSource productsRemoteDataSource;
  final NetworkInfo networkInfo;

  ProductsRepositoryImpl(
      {required this.productsRemoteDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, List<ProductElement>>> getAllProducts(
      int page, String locale, int categoryId) async {
    if (await networkInfo.isConnected) {
      try {
        final productInfo = await productsRemoteDataSource.getAllProducts(
            page, locale, categoryId);
        return Right(productInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
