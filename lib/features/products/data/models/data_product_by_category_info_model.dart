import 'package:jasmingmbh/features/products/data/models/product_element_model.dart';
import 'package:jasmingmbh/features/products/domain/entities/data_product_by_category_info.dart';

class DataProductByCategoryInfoModel extends DataProductByCategoryInfo {
  const DataProductByCategoryInfoModel({required super.products});

  factory DataProductByCategoryInfoModel.fromJson(Map<String, dynamic> json) =>
      DataProductByCategoryInfoModel(
        products: json["products"] == null
            ? []
            : List<ProductElementModel>.from(
                json["products"]!.map((x) => ProductElementModel.fromJson(x))),
      );
}
