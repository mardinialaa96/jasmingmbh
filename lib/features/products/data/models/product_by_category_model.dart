import 'package:jasmingmbh/features/products/data/models/data_product_by_category_info_model.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_by_category.dart';

class ProductByCategoryModel extends ProductByCategory {
  const ProductByCategoryModel(
      {required super.dataProductByCategoryInfo,
      required super.msg,
      required super.status});
  factory ProductByCategoryModel.fromJson(Map<String, dynamic> json) {
    return ProductByCategoryModel(
      dataProductByCategoryInfo:
          DataProductByCategoryInfoModel.fromJson(json["data"]),
      msg: json["msg"],
      status: json["status"],
    );
  }
}
