import 'package:jasmingmbh/features/offers/data/models/offer_class_model.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class ProductElementModel extends ProductElement {
   ProductElementModel(
      {required super.product,
      required super.image,
      required super.name,
      required super.desc,
      required super.amount,
      required super.unit,
      required super.price,
      required super.packagePrice,
      required super.packageAmount,
      required super.taxIncluded,
      required super.taxValue,
      required super.offerCount,
      super.offer,
      required super.available});

  factory ProductElementModel.fromJson(Map<String, dynamic> json) =>
      ProductElementModel(
        product: json["product"],
        image: json["image"],
        name: json["name"],
        desc: json["desc"],
        amount: json["amount"],
        unit: json["unit"],
        price: json["price"],
        packagePrice: json["package_price"],
        packageAmount:  json["package_amount"],
        taxIncluded: json["tax_included"],
        taxValue: json["tax_value"],
        offerCount: json["offer_count"],
        offer: json["offer"] == null ? null : OfferClassModel.fromJson(json["offer"]),
        available: json["available"],
      );
}
