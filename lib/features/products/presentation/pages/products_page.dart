import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/add_to_cart_animation/add_to_cart_animation.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/enum/page_type.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_card.dart';
import 'package:jasmingmbh/core/widgets/empty_widget.dart';
import 'package:jasmingmbh/core/widgets/no_internet_retry_widget.dart';
import 'package:jasmingmbh/core/widgets/search_section.dart';
import 'package:jasmingmbh/core/widgets/shimmer_widget.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/products/presentation/bloc/products_bloc/products_bloc.dart';
import 'package:jasmingmbh/main.dart';

class ProductsPage extends StatefulWidget {
  final int categoryId;
  const ProductsPage({super.key, required this.categoryId});

  @override
  State<ProductsPage> createState() => ProductsPageState();
}

class ProductsPageState extends State<ProductsPage> {
  final ScrollController _scrollController = ScrollController();
  int _pageNumber = 1;
  late List<ProductElement> _products;
  bool _isLoading = false;
  bool _hasMoreData = true;
  bool _isLoadingFirst = false;
  bool _isEmptyProducts = false;
  bool _noInternet = false;
  bool _retry = false;
  late Function(GlobalKey) runAddToCartAnimation;
  late final Function(GlobalKey) runRemoveFromCartAnimation;

  @override
  void initState() {
    super.initState();
    _products = [];
    _fetchProducts();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !_isLoading &&
        _hasMoreData) {
      _pageNumber++;
      BlocProvider.of<ProductsBloc>(context).add(LoadMoreProductsEvent(
        locale: '${LocalStorageService().currentLang}',
        page: _pageNumber,
        categoryId: widget.categoryId,
      ));
    }
  }

  void _fetchProducts() {
    BlocProvider.of<ProductsBloc>(context).add(GetAllProductsEvent(
      locale: '${LocalStorageService().currentLang}',
      page: _pageNumber,
      categoryId: widget.categoryId,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return AddToCartAnimation(
      cartKey: cartKey,
      height: 30,
      width: 30,
      opacity: 0.85,
      jumpAnimation: const JumpAnimationOptions(
          active: false,
          curve: Curves.linear,
          duration: Duration(milliseconds: 0)),
      dragAnimation: const DragToCartAnimationOptions(
        curve: Curves.linearToEaseOut,
        duration: Duration(milliseconds: 400),
      ),
      createAddToCartAnimation: (unAddToCartAnimation) {
        runAddToCartAnimation = unAddToCartAnimation;
      },
      createRemoveFromCartAnimation: (unRemoveFromCartAnimation) {
        runRemoveFromCartAnimation = unRemoveFromCartAnimation;
      },
      child: Scaffold(
        backgroundColor: AppColors.backgroundColor,
        body: SafeArea(
            child: BlocConsumer<ProductsBloc, ProductsState>(
          builder: (context, state) {
            return Stack(
              children: [
                Column(
                  children: [
                    _buildTopSearch(context),
                    Expanded(
                      child: _isLoadingFirst
                          ? ShimmerWidget.shimmerCustomCart()
                          : _isEmptyProducts
                              ? EmptyWidget(
                                  text: 'products_isEmpty'.tr(context))
                              : _noInternet || _retry
                                  ? NoInternetRetryWidget(
                                      isNoInternet: _noInternet,
                                      onTap: () {
                                        _products = [];
                                        _pageNumber = 1;
                                        _fetchProducts();
                                      })
                                  : _buildCart(context),
                    ),
                  ],
                ),
              ],
            );
          },
          listener: (context, state) {
            if (state is LoadingProductsState) {
              setState(() {
                _isLoading = true;
              });
            } else if (state is LoadingInitProductsState) {
              setState(() {
                _isLoadingFirst = true;
              });
            } else if (state is EmptyProductsState) {
              setState(() {
                _isLoadingFirst = false;
                _isEmptyProducts = true;
              });
            } else if (state is LoadedProductsState) {
              setState(() {
                _isLoading = false;
                _isLoadingFirst = false;
                _isEmptyProducts = false;
                _products.addAll(state.products);
                _noInternet = false;
                _retry = false;
                _hasMoreData = state.products.isNotEmpty;
              });
            } else if (state is ErrorProductsState) {
              if (state.message == offlineFailureMessage) {
                _noInternet = true;
                _isLoadingFirst = false;
              } else {
                _retry = true;
                _isLoadingFirst = false;
                return SnackBarMessage().showErrorSnackBarMessage(
                    message:  state.message == serverFailureMessage
                        ? 'try_again'.tr(context)
                        : state.message, context: context);
              }
            }
          },
        )),
      ),
    );
  }

  Widget _buildTopSearch(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          const Expanded(child: SearchSection()),
          IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: const Icon(Icons.arrow_forward_ios)),
        ],
      ),
    );
  }

  Widget _buildCart(BuildContext c) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: _products.length + (_isLoading ? 1 : 0),
      padding: TranslateX.isRtlLanguage(context)
          ? const EdgeInsets.only(
              top: 20.0,
              bottom: 80,
              right: 4,
              left: 15,
            )
          : const EdgeInsets.only(
              top: 20.0,
              bottom: 80,
              right: 15,
              left: 4,
            ),
      itemBuilder: (BuildContext context, int index) {
        if (index < _products.length) {
          return CustomCard(
              product: _products[index],
              pageType: PageType.product,
              runAddToCartAnimation: runAddToCartAnimation,
              runRemoveFromCartAnimation: runRemoveFromCartAnimation);
        } else {
          if (_isLoading && _hasMoreData && !_isLoadingFirst) {
            return const Center(
              child: CircularProgressIndicator(
                color: AppColors.appColor,
              ),
            );
          } else {
            return Container();
          }
        }
      },
    );
  }
}
