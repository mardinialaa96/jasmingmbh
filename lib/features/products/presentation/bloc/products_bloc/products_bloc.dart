import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/products/domain/usecases/products_usecases.dart';

part 'products_event.dart';
part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final ProductsUseCase productsUseCase;
  ProductsBloc({required this.productsUseCase}) : super(ProductsInitial()) {
    on<ProductsEvent>((event, emit) async {
      if (event is GetAllProductsEvent) {
        emit(LoadingInitProductsState());
        try{
          final failureOrDoneMessage =
          await productsUseCase(event.page, event.locale, event.categoryId);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorProductsState(message: serverFailureMessage));
        }

      } else if (event is LoadMoreProductsEvent) {
        emit(LoadingProductsState());
        try{
          final result =
          await productsUseCase(event.page, event.locale, event.categoryId);
          result.fold((failure) {
            emit(ErrorProductsState(message: _mapFailureToMessage(failure)));
          }, (moreProducts) {
            emit(LoadedProductsState(
                products: moreProducts));
          });
        } catch (error) {
          emit(const ErrorProductsState(message: serverFailureMessage));
        }
      }
    });
  }

  ProductsState _mapFailureOrSuccessToState(
      Either<Failure, List<ProductElement>> either) {
    return either.fold(
        (failure) => ErrorProductsState(message: _mapFailureToMessage(failure)),
        (productInfo) {
          if(productInfo.isEmpty) {
            return EmptyProductsState();
          } else {
            return LoadedProductsState(products: productInfo);
          }
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
