part of 'products_bloc.dart';

abstract class ProductsState extends Equatable {
  const ProductsState();
  @override
  List<Object?> get props => [];
}

class ProductsInitial extends ProductsState {
  @override
  List<Object> get props => [];
}

class LoadingProductsState extends ProductsState {
  @override
  List<Object> get props => [];
}

class LoadingInitProductsState extends ProductsState {
  @override
  List<Object> get props => [];
}

class EmptyProductsState extends ProductsState {
  @override
  List<Object> get props => [];
}


class LoadedProductsState extends ProductsState {
  final List<ProductElement> products;

  const LoadedProductsState({required this.products});
  @override
  List<Object?> get props => [products];
}


class ErrorProductsState extends ProductsState {
  final String message;

  const ErrorProductsState({required this.message});
  @override
  List<Object?> get props => [message];
}

