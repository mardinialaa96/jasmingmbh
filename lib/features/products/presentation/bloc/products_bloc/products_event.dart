part of 'products_bloc.dart';

abstract class ProductsEvent extends Equatable {
  const ProductsEvent();
  @override
  List<Object?> get props => [];
}

class GetAllProductsEvent extends ProductsEvent {
  final String locale;
  final int page;
  final int categoryId;
  const GetAllProductsEvent({required this.locale,required this.page, required this.categoryId});
  @override
  List<Object?> get props => [locale, categoryId];
}

class LoadMoreProductsEvent extends ProductsEvent {
  final String locale;
  final int page;
  final int categoryId;
  const LoadMoreProductsEvent(
      {required this.locale, required this.page, required this.categoryId});

  @override
  List<Object?> get props => [locale, page, categoryId];
}
