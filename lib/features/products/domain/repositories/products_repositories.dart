import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

abstract class ProductsRepository {
  Future<Either<Failure, List<ProductElement>>> getAllProducts(
      int page, String locale, int categoryId);
}
