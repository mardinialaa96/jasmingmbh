import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';
import 'package:jasmingmbh/features/products/domain/repositories/products_repositories.dart';

class ProductsUseCase {
  final ProductsRepository repository;

  ProductsUseCase(this.repository);

  Future<Either<Failure, List<ProductElement>>> call(int page,String locale,int categoryId) async {
    return await repository.getAllProducts(page,locale,categoryId);
  }
}
