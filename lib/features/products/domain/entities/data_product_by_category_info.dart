import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/products/domain/entities/product_element.dart';

class DataProductByCategoryInfo extends Equatable {
  final List<ProductElement> products;
  const DataProductByCategoryInfo({required this.products});

  @override
  List<Object?> get props => [products];
}
