import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/offers/data/models/offer_class_model.dart';
import 'package:jasmingmbh/features/offers/domain/entities/offer_class.dart';

class ProductElement extends Equatable {
  final int product;
  final String? image;
  final String? name;
  final String? desc;
  final num? amount;
  final String? unit;
  final num? price;
  final num? packagePrice;
  final num? packageAmount;
  final String? taxIncluded;
  final num? taxValue;
  final int? offerCount;
  final OfferClass? offer;
  final String? available;
  int quantity;
  num subtotal;

  ProductElement({
    required this.product,
    required this.image,
    required this.name,
    required this.desc,
    required this.amount,
    required this.unit,
    required this.price,
    required this.packagePrice,
    required this.packageAmount,
    required this.taxIncluded,
    required this.taxValue,
    required this.offerCount,
    this.offer,
    required this.available,
    this.quantity = 0,
    this.subtotal = 0.0
  });

  @override
  List<Object?> get props => [
    product,
    image,
    name,
    desc,
    amount,
    unit,
    price,
    packagePrice,
    packageAmount,
    taxValue,
    taxIncluded,
    offerCount,
    offer,
    available,
    quantity,
    subtotal,
  ];

  factory ProductElement.fromJson(Map<String, dynamic> json) => ProductElement(
    product: json["product"],
    image: json["image"],
    name: json["name"],
    desc: json["desc"],
    amount: json["amount"],
    unit: json["unit"],
    price: json["price"],
    packagePrice: json["package_price"],
    packageAmount: json["package_amount"],
    taxIncluded: json["tax_included"],
    taxValue: json["tax_value"],
    offerCount: json["offer_count"],
    offer: json["offer"] == null ? null : OfferClassModel.fromJson(json["offer"]),
    available: json["available"],
  );

  Map<String, dynamic> toJson() => {
    "product": product,
    "image": image,
    "name": name,
    "desc": desc,
    "amount": amount,
    "unit": unit,
    "price": price,
    "package_price": packagePrice,
    "package_amount": packageAmount,
    "tax_included": taxIncluded,
    "tax_value": taxValue,
    "offer_count": offerCount,
    "offer": offer?.toJson(),
    "available": available,
    "quantity": quantity,
    "subtotal": subtotal
  };

  factory ProductElement.fromJsonCart(Map<String, dynamic> json) => ProductElement(
      product: json["product"],
      image: json["image"],
      name: json["name"],
      desc: json["desc"],
      amount: json["amount"],
      unit: json["unit"],
      price: json["price"],
      packagePrice: json["package_price"],
      packageAmount: json["package_amount"],
      taxIncluded: json["tax_included"],
      taxValue: json["tax_value"],
      offerCount: json["offer_count"],
      offer: json["offer"] == null ? null : OfferClassModel.fromJson(json["offer"]),
      available: json["available"],
      quantity: json["quantity"],
      subtotal: json["subtotal"]
  );

  static List<ProductElement> fromJsonList(List<dynamic> jsonList) {
    return jsonList.map((json) => ProductElement.fromJson(json)).toList();
  }
}
