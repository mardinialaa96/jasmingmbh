import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/products/domain/entities/data_product_by_category_info.dart';

class ProductByCategory extends Equatable {
  final DataProductByCategoryInfo dataProductByCategoryInfo;
  final String msg;
  final int status;
  const ProductByCategory(
      {required this.dataProductByCategoryInfo,
      required this.msg,
      required this.status});

  @override
  List<Object?> get props => [dataProductByCategoryInfo, msg,status];
}
