import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/error_data_widget.dart';
import 'package:jasmingmbh/features/home_category/presentation/pages/home_tabs.dart';
import 'package:jasmingmbh/features/update_app/presentation/bloc/update_app_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:pub_semver/pub_semver.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdateAppPage extends StatefulWidget {
  final Widget child;
  const UpdateAppPage({
    super.key,
    required this.child,
  });

  @override
  State<UpdateAppPage> createState() => UpdateAppPageState();
}

class UpdateAppPageState extends State<UpdateAppPage> {
  String? appVersion;

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    BlocProvider.of<UpdateAppBloc>(context).add(
        UpdateAppVersionEvent(locale: '${LocalStorageService().currentLang}'));
  }

  Future<void> _initPackageInfo() async {
    final packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      appVersion = packageInfo.version;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: BlocBuilder<UpdateAppBloc, UpdateAppState>(
        builder: (context, state) {
          if (state is LoadingUpdateAppState) {
            return const Center(
              child: CircularProgressIndicator(
                color: AppColors.appColor,
              ),
            );
          } else if (state is ErrorUpdateAppState) {
              return ErrorDataWidget(
                  text: state.message == offlineFailureMessage
                      ? 'no_internet'.tr(context)
                      : state.message == serverFailureMessage
                      ? 'try_again'.tr(context)
                      : state.message,
                  onTap: () {
                    BlocProvider.of<UpdateAppBloc>(context).add(
                        UpdateAppVersionEvent(
                            locale: '${LocalStorageService().currentLang}'));
                  });

          } else if (state is LoadedUpdateAppState) {
            if (appVersion != null) {
              return updateWidget(state, appVersion!);
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: AppColors.appColor,
                ),
              );
            }
          }
          return const SizedBox();
        },
      ),
    );
  }

  Widget updateWidget(LoadedUpdateAppState state, String appVersion) {
    bool showUpdateUi = false;
    bool forceUpdate = false;
    Version applicationVersion = Version.parse(appVersion);
    if (state.updateApp.status == 200 &&
        state.updateApp.data != null &&
        state.updateApp.data?.app != null) {
      Version minimumApp =
          Version.parse(state.updateApp.data?.app?.minVersion ?? "");
      Version latestApp =
          Version.parse(state.updateApp.data?.app?.lastVersion ?? "");
      if (applicationVersion < minimumApp) {
        showUpdateUi = true;
        forceUpdate = true;
        print("force user to update app");
      }
      if (applicationVersion < latestApp) {
        if (state.updateApp.data?.app?.mandatory == true) {
          showUpdateUi = true;
          forceUpdate = true;
          print("force user to update app");
        } else {
          showUpdateUi = true;
          print("No force user to update app");
        }
      }
      print(showUpdateUi);
      if (showUpdateUi == true) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                    child: Lottie.asset(Assets.downloadUpdate,
                        width: 300, height: 300, fit: BoxFit.fill)),
                Text('updateAppMessage'.tr(context),
                    style: AppTextStyle.subtitleBold,
                    textAlign: TextAlign.center),
              ],
            )),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: SizedBox(
                      width: 200,
                      child: CustomButton(
                          text: "update".tr(context),
                          onPressed: () async {
                            if (Platform.isAndroid) {
                              String? androidUrl =
                                  state.updateApp.data?.app?.androidUpdateUrl;
                              if (androidUrl != null) {
                                try {
                                  print(Uri.parse(androidUrl));
                                  await launchUrl(Uri.parse(androidUrl));
                                } catch (e) {
                                  print(
                                      'Could not open WhatsApp in the browser: $e');
                                }
                              }
                            } else if (Platform.isIOS) {
                              String? iosUrl =
                                  state.updateApp.data?.app?.iosUpdateUrl;
                              if (iosUrl != null) {
                                try {
                                  print(Uri.parse(iosUrl));
                                  await launchUrl(Uri.parse(iosUrl));
                                } catch (e) {
                                  print(
                                      'Could not open WhatsApp in the browser: $e');
                                }
                              }
                            }
                          }),
                    ),
                  ),
                  const SizedBox(width: 10),
                  if (forceUpdate == false)
                    Expanded(
                      child: SizedBox(
                        width: 200,
                        child: CustomButton(
                            text: "skip".tr(context),
                            onPressed: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) => const HomeTabs()));
                            }),
                      ),
                    ),
                ],
              ),
            )
          ],
        );
      } else {
        return widget.child;
      }
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(state.updateApp.msg ?? "", style: AppTextStyle.subtitleBold),
            const SizedBox(height: 10),
            InkWell(
              onTap: () {
                BlocProvider.of<UpdateAppBloc>(context).add(
                    UpdateAppVersionEvent(
                        locale: '${LocalStorageService().currentLang}'));
              },
              child: const Icon(
                Icons.refresh,
                color: AppColors.appColor,
                size: 40,
              ),
            )
          ],
        ),
      );
    }
  }
}
