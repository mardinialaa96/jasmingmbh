import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app.dart';
import 'package:jasmingmbh/features/update_app/domain/usecases/update_app_usecases.dart';
import 'package:meta/meta.dart';

part 'update_app_event.dart';
part 'update_app_state.dart';

class UpdateAppBloc extends Bloc<UpdateAppEvent, UpdateAppState> {
  final UpdateAppUseCase updateAppUseCase;
  UpdateAppBloc({required this.updateAppUseCase}) : super(UpdateAppInitial()) {
    on<UpdateAppEvent>((event, emit) async {
      if (event is UpdateAppVersionEvent) {
        emit(LoadingUpdateAppState());
        try {
          final failureOrDoneMessage =
              await updateAppUseCase(event.locale);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorUpdateAppState(message: serverFailureMessage));
        }
      }
    });
  }
  UpdateAppState _mapFailureOrSuccessToState(
      Either<Failure, UpdateApp> either) {
    return either.fold(
            (failure) =>
            ErrorUpdateAppState(message: _mapFailureToMessage(failure)),
            (updateAppInfo) =>
            LoadedUpdateAppState(updateApp: updateAppInfo));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
