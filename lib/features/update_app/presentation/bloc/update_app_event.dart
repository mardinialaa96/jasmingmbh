part of 'update_app_bloc.dart';

@immutable
sealed class UpdateAppEvent extends Equatable {
  const UpdateAppEvent();
  @override
  List<Object?> get props => [];
}

class UpdateAppVersionEvent extends UpdateAppEvent {
  final String locale;

  const UpdateAppVersionEvent({required this.locale});
  @override
  List<Object?> get props => [locale];
}
