part of 'update_app_bloc.dart';

@immutable
sealed class UpdateAppState extends Equatable{
  const UpdateAppState();
  @override
  List<Object?> get props => [];
}

final class UpdateAppInitial extends UpdateAppState {}

class LoadingUpdateAppState extends UpdateAppState {}

class ErrorUpdateAppState extends UpdateAppState {
final String message;

const ErrorUpdateAppState({required this.message});
@override
List<Object?> get props => [message];
}

class LoadedUpdateAppState extends UpdateAppState {
final UpdateApp updateApp;

const LoadedUpdateAppState({required this.updateApp});
@override
List<Object?> get props => [updateApp];
}
