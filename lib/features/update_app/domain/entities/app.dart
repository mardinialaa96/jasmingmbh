import 'package:equatable/equatable.dart';

class App extends Equatable {
  final String minVersion;
  final String lastVersion;
  final bool mandatory;
  final String androidUpdateUrl;
  final String iosUpdateUrl;

  const App({
    required this.minVersion,
    required this.lastVersion,
    required this.mandatory,
    required this.androidUpdateUrl,
    required this.iosUpdateUrl,
  });

  @override
  List<Object?> get props =>
      [minVersion, lastVersion, mandatory, androidUpdateUrl, iosUpdateUrl];
}
