import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/app.dart';

class UpdateAppInfo extends Equatable {
  final App? app;

  const UpdateAppInfo({required this.app});

  @override
  List<Object?> get props => [app];

}
