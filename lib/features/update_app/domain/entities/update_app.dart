import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app_info.dart';

class UpdateApp extends Equatable {
  final int? status;
  final String? msg;
  final UpdateAppInfo? data;

  const UpdateApp({this.status, this.msg, this.data});
  @override
  List<Object?> get props => [];
}
