import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app.dart';
import 'package:jasmingmbh/features/update_app/domain/repository/update_app_repositories.dart';



class UpdateAppUseCase {
  final UpdateAppRepository repository;

  UpdateAppUseCase(this.repository);

  Future<Either<Failure, UpdateApp>> call(
      String locale) async {
    return await repository.updateApp(locale);
  }
}
