import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app.dart';


abstract class UpdateAppRepository {
  Future<Either<Failure, UpdateApp>> updateApp(String locale);
}
