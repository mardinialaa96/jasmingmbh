import 'package:jasmingmbh/features/update_app/data/models/update_app_info_model.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app.dart';

class UpdateAppModel extends UpdateApp {
  const UpdateAppModel(
      {required super.msg,
        required super.data,
        required super.status});
  factory UpdateAppModel.fromJson(Map<String, dynamic> json) {
    return UpdateAppModel(
        msg: json["msg"],
        status: json["status"],
        data: json["data"] == null
            ? null : UpdateAppInfoModel.fromJson(json["data"]));
  }
}
