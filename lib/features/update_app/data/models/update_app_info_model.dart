import 'package:jasmingmbh/features/update_app/data/models/app_model.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app_info.dart';

class UpdateAppInfoModel extends UpdateAppInfo {
  const UpdateAppInfoModel(
      {super.app});

  factory UpdateAppInfoModel.fromJson(Map<String, dynamic> json) {
    return UpdateAppInfoModel(
      app: json["app"] == null
          ?  null : AppModel.fromJson(json["app"]),
    );
  }

}
