import 'package:jasmingmbh/features/update_app/domain/entities/app.dart';

class AppModel extends App {
  const AppModel(
      {required super.minVersion,
      required super.lastVersion,
      required super.mandatory,
      required super.androidUpdateUrl,
      required super.iosUpdateUrl});

  factory AppModel.fromJson(Map<String, dynamic> json) {
    return AppModel(
        minVersion: json["minVersion"],
        lastVersion: json["lastVersion"],
        mandatory: json["mandatory"],
        androidUpdateUrl: json["androidUpdateUrl"],
        iosUpdateUrl: json["iosUpdateUrl"]);
  }

  Map<String, dynamic> toJson() => {
        "minVersion": minVersion,
        "lastVersion": lastVersion,
        "mandatory": mandatory,
        "androidUpdateUrl": androidUpdateUrl,
        "iosUpdateUrl": iosUpdateUrl,
      };
}
