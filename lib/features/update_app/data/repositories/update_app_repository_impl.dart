import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/update_app/data/datasources/update_app_data_sources.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app.dart';
import 'package:jasmingmbh/features/update_app/domain/repository/update_app_repositories.dart';

class UpdateAppRepositoryImpl implements UpdateAppRepository {
  final UpdateAppRemoteDataSource updateAppRemoteDataSource;
  final NetworkInfo networkInfo;

  UpdateAppRepositoryImpl(
      {required this.updateAppRemoteDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, UpdateApp>> updateApp(String locale) async {
    if (await networkInfo.isConnected) {
      try {
        final updateAppInfo = await updateAppRemoteDataSource.updateApp(locale);
        return Right(updateAppInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
