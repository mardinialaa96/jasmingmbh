import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/features/update_app/data/models/update_app_model.dart';
import 'package:jasmingmbh/features/update_app/domain/entities/update_app.dart';
import 'package:jasmingmbh/main.dart';

abstract class UpdateAppRemoteDataSource {
  Future<UpdateApp> updateApp(String locale);
}

class UpdateAppRemoteDataSourceImpl implements UpdateAppRemoteDataSource {
  final http.Client client;

  UpdateAppRemoteDataSourceImpl({required this.client});

  @override
  Future<UpdateApp> updateApp(String locale) async {
        final queryParameters = {
        'locale': locale,
        };
        final token = LocalStorageService().token;
    if (token == null) {
      throw ServerException();
    }
    final url = Uri.parse('$baseUrl/check-app-version');
    final response = await client
        .get(url.replace(queryParameters: queryParameters), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      UpdateAppModel updateAppModel = UpdateAppModel.fromJson(decodeJson);
      print(updateAppModel.toString());
      if (updateAppModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
            (route) => false);
        return updateAppModel;
      } else {
        return updateAppModel;
      }
    } else {
      throw ServerException();
    }
  }
}
