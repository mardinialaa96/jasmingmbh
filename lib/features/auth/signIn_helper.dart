import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/features/auth/domain/entities/city.dart';
import 'package:jasmingmbh/features/auth/domain/entities/gender.dart';

class SignInHelper {

  // TextEditingController
  static final firstNameController = TextEditingController();
  static final lastNameController = TextEditingController();
  static final phoneController = TextEditingController();
  static final emailController = TextEditingController();
  static final shopNameController = TextEditingController();
  static final districtController = TextEditingController();
  static final passwordController = TextEditingController();
  static final confirmPasswordController = TextEditingController();
  static final addressController = TextEditingController();

  // FocusNode
  static final shopNameFocusNode = FocusNode();
  static final districtFocusNode = FocusNode();
  static final passwordFocusNode = FocusNode();
  static final confirmPasswordFocusNode = FocusNode();
  static final addressFocusNode = FocusNode();

  static final firstNameFocusNode = FocusNode();
  static final lastNameFocusNode = FocusNode();
  static final phoneFocusNode = FocusNode();
  static final emailFocusNode = FocusNode();

  static List<Gender> genders = [
    Gender(0),
    Gender(1),
  ];

  static List<City> city = [
    City('BER', 'برلين', 'Berlin','Berlin'),
    City('MUC', 'ميونيخ', 'München','Munich'),
    City('HAM', 'هامبورج', 'Hamburg','Hamburg'),
    City('CGN', 'كولونيا', 'Köln','Cologne'),
    City('FRA', 'فرانكفورت', 'Frankfurt','Frankfurt'),
    City('STR', 'شتوتغارت', 'Stuttgart','Stuttgart'),
    City('DTM', 'دورتموند', 'Dortmund','Dortmund'),
    City('DUS', 'دوسلدورف', 'Düsseldorf','Dusseldorf'),
    City('ESS', 'إسن', 'Essen','Essen'),
    City('BON', 'بون', 'Bonn','Essen'),
  ];

  static int? selectedItem;
  static  City? selectedCityItem;

  static bool isObscure = true;
  static bool isObscure2 = true;

  String? validateEmail(String value, BuildContext context) {
    String emailPattern = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$';
    RegExp regExp = RegExp(emailPattern);

    if (value.isEmpty) {
      return 'empty_email_message'.tr(context);
    } else if (!regExp.hasMatch(value)) {
      return 'valid_email_message'.tr(context);
    }

    return null;
  }

  void clearSignInHelper() {
    firstNameController.clear();
    lastNameController.clear();
    phoneController.clear();
    emailController.clear();
    shopNameController.clear();
    districtController.clear();
    passwordController.clear();
    confirmPasswordController.clear();
    addressController.clear();

    selectedItem = null;
    selectedCityItem = null;

    isObscure = false;
    isObscure2 = false;
  }
}
