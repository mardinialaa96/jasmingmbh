import 'package:jasmingmbh/features/auth/domain/entities/post_login_parametesd.dart';

class PostLoginParametersModel extends PostLoginParameters {
  const PostLoginParametersModel(
      {required String email,
      required String password,
      required String locale,
      required String device})
      : super(email: email, password: password, locale: locale, device: device);

  factory PostLoginParametersModel.fromJson(Map<String, dynamic> json) {
    return PostLoginParametersModel(
        email: json['email'],
        password: json['password'],
        locale: json['locale'],
        device: json['device']);
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'password': password,
      'locale': locale,
      'device': device
    };
  }
}
