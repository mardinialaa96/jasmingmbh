import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';

class SuccessOrFailureModel extends SuccessOrFailure {
  const SuccessOrFailureModel(
      {required super.data, required super.msg, required super.status});
  factory SuccessOrFailureModel.fromJson(Map<String, dynamic> json) =>
      SuccessOrFailureModel(
        data: json["data"],
        msg: json["msg"],
        status: json["status"],
      );
}
