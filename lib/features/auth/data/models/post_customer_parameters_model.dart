import 'package:jasmingmbh/features/auth/domain/entities/post_customer_parameters.dart';

class PostCustomerParametersModel extends PostCustomerParameters {
  const PostCustomerParametersModel(
      {required super.firstname,
      required super.lastname,
      required super.gender,
      required super.mobile,
      required super.email,
      required super.city,
      required super.area,
      required super.address,
      required super.store,
      required super.password,
      required super.locale});

  factory PostCustomerParametersModel.fromJson(Map<String, dynamic> json) {
    return PostCustomerParametersModel(
      firstname: json["firstname"],
      lastname: json["lastname"],
      gender: json["gender"],
      mobile: json["mobile"],
      email: json["email"],
      city: json["city"],
      area: json["area"],
      address: json["address"],
      store: json["store"],
      password: json["password"],
      locale: json['locale']
    );
  }

  Map<String, dynamic> toJson() => {
    "firstname": firstname,
    "lastname": lastname,
    "gender": gender,
    "mobile": mobile,
    "email": email,
    "city": city,
    "area": area,
    "address": address,
    "store": store,
    "password": password,
    "locale": locale
  };
}
