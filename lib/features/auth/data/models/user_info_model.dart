import 'package:jasmingmbh/features/auth/data/models/data_user_Info_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';

class UserInfoModel extends UserInfo {
  const UserInfoModel(
      {required super.dataUserInfo,
        required super.msg,
        required super.status});

  factory UserInfoModel.fromJson(Map<String, dynamic> json) {
    return UserInfoModel(
      dataUserInfo: json["data"] == null
          ? null : DataUserInfoModel.fromJson(json["data"]),
      msg: json["msg"],
      status: json["status"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "data": dataUserInfo?.toJson(),
      "msg": msg,
      "status": status,
    };
  }

}
