import 'package:jasmingmbh/features/auth/domain/entities/post_customer_profile.dart';

class PostCustomerProfileModel extends PostCustomerProfile {
  const PostCustomerProfileModel(
      {required super.firstname,
      required super.lastname,
      required super.gender,
      required super.mobile,
      required super.city,
      required super.area,
      required super.address,
      required super.store,
      required super.locale});

  factory PostCustomerProfileModel.fromJson(Map<String, dynamic> json) {
    return PostCustomerProfileModel(
        firstname: json["firstname"],
        lastname: json["lastname"],
        gender: json["gender"],
        mobile: json["mobile"],
        city: json["city"],
        area: json["area"],
        address: json["address"],
        store: json["store"],
        locale: json['locale']);
  }

  Map<String, dynamic> toJson() => {
        "firstname": firstname,
        "lastname": lastname,
        "gender": gender,
        "mobile": mobile,
        "city": city,
        "area": area,
        "address": address,
        "store": store,
        "locale": locale
      };
}
