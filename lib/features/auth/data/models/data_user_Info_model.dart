import 'package:jasmingmbh/features/auth/data/models/customer_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/data_user_info.dart';

class DataUserInfoModel extends DataUserInfo {
  const DataUserInfoModel(
      { super.token,
        super.customer});

  factory DataUserInfoModel.fromJson(Map<String, dynamic> json) {
    return DataUserInfoModel(
      token: json["token"],
      customer: json["customer"] == null
          ?  null : CustomerModel.fromJson(json["customer"]),
    );
  }

}
