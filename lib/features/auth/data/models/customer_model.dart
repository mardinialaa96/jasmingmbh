import 'package:jasmingmbh/features/auth/domain/entities/customer.dart';

class CustomerModel extends Customer {
  const CustomerModel(
      {required super.firstname,
      required super.lastname,
      required super.gender,
      required super.mobile,
      required super.email,
      required super.city,
      required super.area,
      required super.address,
      required super.store});

  factory CustomerModel.fromJson(Map<String, dynamic> json) {
    return CustomerModel(
      firstname: json["firstname"],
      lastname: json["lastname"],
      gender: json["gender"],
      mobile: json["mobile"],
      email: json["email"],
      city: json["city"],
      area: json["area"],
      address: json["address"],
      store: json["store"],
    );
  }

  Map<String, dynamic> toJson() => {
        "firstname": firstname,
        "lastname": lastname,
        "gender": gender,
        "mobile": mobile,
        "email": email,
        "city": city,
        "area": area,
        "address": address,
        "store": store,
      };
}
