import 'package:jasmingmbh/features/auth/data/models/data_user_Info_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/register_return_data.dart';

class RegisterReturnDataModel extends RegisterReturnData {
  const RegisterReturnDataModel(
      {required super.message,
        required super.data,
        required super.status,
        required super.error});
  factory RegisterReturnDataModel.fromJson(Map<String, dynamic> json) {
    return RegisterReturnDataModel(
        message: json["msg"],
        status: json["status"],
        error: json["error"] == null ? [] : List<List<String>>.from(json["error"]!.map((x) => List<String>.from(x.map((x) => x)))),
        data: json["data"] == null
            ? null : DataUserInfoModel.fromJson(json["data"]));
  }
}
