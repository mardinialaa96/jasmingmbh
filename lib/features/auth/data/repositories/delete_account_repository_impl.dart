import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/auth/data/datasources/delete_account_data_sources.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/delete_account_repositories.dart';

class DeleteAccountRepositoryImpl implements DeleteAccountRepository {
  final DeleteAccountRemoteDataSource deleteAccountRemoteDataSource;
  final NetworkInfo networkInfo;

  DeleteAccountRepositoryImpl(
      {required this.deleteAccountRemoteDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, SuccessOrFailure>> deleteAccount() async {
    if (await networkInfo.isConnected) {
      try {
        final deleteAccountInfo =
            await deleteAccountRemoteDataSource.deleteAccount();
        return Right(deleteAccountInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
