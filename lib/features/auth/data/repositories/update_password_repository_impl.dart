import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/auth/data/datasources/update_password_data_source.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/update_password_repositories.dart';

class UpdatePasswordRepositoryImpl implements UpdatePasswordRepository {
  final UpdatePasswordRemoteDataSource updatePasswordRemoteDataSource;
  final NetworkInfo networkInfo;

  UpdatePasswordRepositoryImpl(
      {required this.updatePasswordRemoteDataSource,
      required this.networkInfo});
  @override
  Future<Either<Failure, SuccessOrFailure>> updatePassword(
      String password) async {
    if (await networkInfo.isConnected) {
      try {
        final updatePasswordInfo =
            await updatePasswordRemoteDataSource.updatePassword(password);
        return Right(updatePasswordInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
