import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/auth/data/datasources/register_remote_data_sources.dart';
import 'package:jasmingmbh/features/auth/data/models/post_customer_parameters_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_parameters.dart';
import 'package:jasmingmbh/features/auth/domain/entities/register_return_data.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/register_repositories.dart';

class RegisterRepositoryImpl implements RegisterRepository {
  final RegisterRemoteDataSource registerRemoteDataSource;
  final NetworkInfo networkInfo;

  RegisterRepositoryImpl(
      {required this.registerRemoteDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, RegisterReturnData>> register(
      PostCustomerParameters postCustomerParameters) async {
    final registerModel = PostCustomerParametersModel(
      email: postCustomerParameters.email,
      address: postCustomerParameters.address,
      area: postCustomerParameters.area,
      city: postCustomerParameters.city,
      firstname: postCustomerParameters.firstname,
      lastname: postCustomerParameters.lastname,
      gender: postCustomerParameters.gender,
      mobile: postCustomerParameters.mobile,
      store: postCustomerParameters.store,
      password: postCustomerParameters.password,
      locale: postCustomerParameters.locale
    );
    if (await networkInfo.isConnected) {
      try {
        final userInfo = await registerRemoteDataSource.register(registerModel);
        return Right(userInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
