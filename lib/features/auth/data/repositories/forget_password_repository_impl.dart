import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/auth/data/datasources/forget_password_data_sources.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/forget_password_repositories.dart';

class ForgetPasswordRepositoryImpl implements ForgetPasswordRepository {
  final ForgetPasswordRemoteDataSource forgetPasswordRemoteDataSource;
  final NetworkInfo networkInfo;

  ForgetPasswordRepositoryImpl(
      {required this.forgetPasswordRemoteDataSource,
      required this.networkInfo});
  @override
  Future<Either<Failure, SuccessOrFailure>> forgetPassword(
      String email, String locale) async {
    if (await networkInfo.isConnected) {
      try {
        final forgetPasswordInfo =
            await forgetPasswordRemoteDataSource.forgetPassword(email, locale);
        return Right(forgetPasswordInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
