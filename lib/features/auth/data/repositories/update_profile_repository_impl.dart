import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/auth/data/datasources/update_profile_data_sources.dart';
import 'package:jasmingmbh/features/auth/data/models/post_customer_profile_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_profile.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/update_profile_repositories.dart';

class UpdateProfileRepositoryImpl implements UpdateProfileRepository {
  final UpdateProfileDataSource updateProfileDataSource;
  final NetworkInfo networkInfo;

  UpdateProfileRepositoryImpl(
      {required this.updateProfileDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, SuccessOrFailure>> updateProfile(
      PostCustomerProfile customer) async {
    final updateProfileModel = PostCustomerProfileModel(
        address: customer.address,
        area: customer.area,
        city: customer.city,
        firstname: customer.firstname,
        lastname: customer.lastname,
        gender: customer.gender,
        mobile: customer.mobile,
        store: customer.store,
        locale: customer.locale);
    if (await networkInfo.isConnected) {
      try {
        final updateProfileInfo =
            await updateProfileDataSource.updateProfile(updateProfileModel);
        return Right(updateProfileInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
