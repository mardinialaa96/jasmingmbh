import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/auth/data/datasources/login_datasources/user_local_data_source.dart';
import 'package:jasmingmbh/features/auth/data/datasources/profile_remote_data_sources.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/profile_repositories.dart';

class ProfileRepositoryImpl implements ProfileRepository {
  final ProfileRemoteDataSource profileRemoteDataSource;
  final UserLocalDataSource userLocalDataSource;
  final NetworkInfo networkInfo;

  ProfileRepositoryImpl(
      {required this.profileRemoteDataSource, required this.userLocalDataSource,required this.networkInfo});
  @override
  Future<Either<Failure, UserInfo>> profile() async {
    if (await networkInfo.isConnected) {
      try {
        final userInfo = await profileRemoteDataSource.profile();
        if(userInfo.dataUserInfo != null) {
          userLocalDataSource.cacheUser(userInfo);
        }
        return Right(userInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localUser = await userLocalDataSource.getCacheUser();
        return Right(localUser);
      } on EmptyCacheException {
        return Left(EmptyCacheFailure());
      }
    }
  }
}
