import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/network/network_info.dart';
import 'package:jasmingmbh/features/auth/data/datasources/login_datasources/login_remote_data_source.dart';
import 'package:jasmingmbh/features/auth/data/datasources/login_datasources/user_local_data_source.dart';
import 'package:jasmingmbh/features/auth/data/models/post_login_parameters_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_login_parametesd.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/login_repositories.dart';

class LoginRepositoryImpl implements LoginRepository {
  final LoginRemoteDataSource loginRemoteDataSource;
  final UserLocalDataSource userLocalDataSource;
  final NetworkInfo networkInfo;

  LoginRepositoryImpl(
      {required this.loginRemoteDataSource,required this.userLocalDataSource ,required this.networkInfo});
  @override
  Future<Either<Failure, UserInfo>> login(
      PostLoginParameters postLoginParameters) async {
    final loginModel = PostLoginParametersModel(
        email: postLoginParameters.email,
        password: postLoginParameters.password,
        locale: postLoginParameters.locale,
        device: postLoginParameters.device);

    if (await networkInfo.isConnected) {
      try {
        final userInfo = await loginRemoteDataSource.login(loginModel);
        if(userInfo.dataUserInfo != null) {
          userLocalDataSource.cacheUser(userInfo);
        }
        return Right(userInfo);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
