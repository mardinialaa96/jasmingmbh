import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/data/models/post_customer_parameters_model.dart';
import 'package:jasmingmbh/features/auth/data/models/register_return_data_model.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/main.dart';

abstract class RegisterRemoteDataSource {
  Future<RegisterReturnDataModel> register(PostCustomerParametersModel postCustomerModel);
}

class RegisterRemoteDataSourceImpl implements RegisterRemoteDataSource {
  final http.Client client;

  RegisterRemoteDataSourceImpl({required this.client});

  @override
  Future<RegisterReturnDataModel> register(PostCustomerParametersModel postCustomerModel) async {
    final response = await client.post(Uri.parse('$baseUrl/register'),
        body: jsonEncode(postCustomerModel.toJson()),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      RegisterReturnDataModel registerReturnDataModel =
          RegisterReturnDataModel.fromJson(decodeJson);
      if (registerReturnDataModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
                (route) => false);
        return registerReturnDataModel;
      } else {
        return registerReturnDataModel;
      }
    } else {
      throw ServerException();
    }
  }
}
