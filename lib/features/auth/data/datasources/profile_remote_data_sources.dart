import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/data/models/user_info_model.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/main.dart';

abstract class ProfileRemoteDataSource {
  Future<UserInfoModel> profile();
}

class ProfileRemoteDataSourceImpl implements ProfileRemoteDataSource {
  final http.Client client;

  ProfileRemoteDataSourceImpl({required this.client});

  @override
  Future<UserInfoModel> profile() async {
    final response = await client.get(Uri.parse('$baseUrl/profile'), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${LocalStorageService().token}',
    });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      UserInfoModel userInfoModel = UserInfoModel.fromJson(decodeJson);
      if (userInfoModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
                (route) => false);
        return userInfoModel;
      } else {
        return userInfoModel;
      }
    } else {
      throw ServerException();
    }
  }
}
