import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/data/models/post_login_parameters_model.dart';
import 'package:jasmingmbh/features/auth/data/models/user_info_model.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/main.dart';


abstract class LoginRemoteDataSource {
  Future<UserInfoModel> login(
      PostLoginParametersModel postLoginParametersModel);
}

class LoginRemoteDataSourceImpl implements LoginRemoteDataSource {
  final http.Client client;

  LoginRemoteDataSourceImpl({required this.client});

  @override
  Future<UserInfoModel> login(
      PostLoginParametersModel postLoginParametersModel) async {
    final response = await client.post(Uri.parse('$baseUrl/login'),
        body: jsonEncode(postLoginParametersModel.toJson()),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      UserInfoModel userInfoModel = UserInfoModel.fromJson(decodeJson);
      if (userInfoModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
                (route) => false);
        return userInfoModel;
      } else {
        return userInfoModel;
      }
    } else {
      throw ServerException();
    }
  }
}
