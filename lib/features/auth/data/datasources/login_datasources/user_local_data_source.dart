import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/features/auth/data/models/user_info_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserLocalDataSource {
  Future<UserInfoModel> getCacheUser();
  Future<Unit> cacheUser(UserInfo userInfo);
}

const cachedCustomer = 'CACHED_CUSTOMER';

class UserLocalDataSourceImpl implements UserLocalDataSource {
  final SharedPreferences sharedPreferences;

  UserLocalDataSourceImpl({required this.sharedPreferences});
  @override
  Future<Unit> cacheUser(UserInfo userInfo) {
    print('cacheUser');
    UserInfoModel userInfoModel = UserInfoModel(
      dataUserInfo: userInfo.dataUserInfo,
      msg: userInfo.msg,
      status: userInfo.status
    );
    String encodedEmployee = jsonEncode(userInfoModel.toJson());
    sharedPreferences.setString(cachedCustomer, encodedEmployee);
    LocalStorageService().isFirstName = userInfo.dataUserInfo?.customer?.firstname;
    LocalStorageService().isLastName = userInfo.dataUserInfo?.customer?.lastname;
    print(LocalStorageService().firstName);
    print(LocalStorageService().lastName);
    return Future.value(unit);
  }

  @override
  Future<UserInfoModel> getCacheUser() {
    print('getCacheUser');
    final jsonString = sharedPreferences.getString(cachedCustomer);
    if (jsonString != null) {
      UserInfoModel jsonToUserModel =
      UserInfoModel.fromJson(jsonDecode(jsonString));
      return Future.value(jsonToUserModel);
    } else {
      throw EmptyCacheException();
    }
  }
}
