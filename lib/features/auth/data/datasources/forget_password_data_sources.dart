import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/data/models/success_or_failure_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/main.dart';

abstract class ForgetPasswordRemoteDataSource {
  Future<SuccessOrFailure> forgetPassword(String email, String locale);
}

class ForgetPasswordRemoteDataSourceImpl
    implements ForgetPasswordRemoteDataSource {
  final http.Client client;

  ForgetPasswordRemoteDataSourceImpl({required this.client});

  @override
  Future<SuccessOrFailure> forgetPassword(String email, String locale) async {
    final response = await client.post(Uri.parse('$baseUrl/forget-password'),
        body: jsonEncode({'email': email, 'locale': locale}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      SuccessOrFailureModel forgetPasswordModel =
          SuccessOrFailureModel.fromJson(decodeJson);
      if (forgetPasswordModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
            (route) => false);
        return forgetPasswordModel;
      } else {
        return forgetPasswordModel;
      }
    } else {
      throw ServerException();
    }
  }
}
