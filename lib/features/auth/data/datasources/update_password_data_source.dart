import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/data/models/success_or_failure_model.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/main.dart';

abstract class UpdatePasswordRemoteDataSource {
  Future<SuccessOrFailure> updatePassword(String password);
}

class UpdatePasswordRemoteDataSourceImpl
    implements UpdatePasswordRemoteDataSource {
  final http.Client client;

  UpdatePasswordRemoteDataSourceImpl({required this.client});

  @override
  Future<SuccessOrFailure> updatePassword(String password) async {
    final response = await client.patch(Uri.parse('$baseUrl/update-password'),
        body: jsonEncode({'password': password}),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ${LocalStorageService().token}',
        });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      SuccessOrFailureModel updatePasswordModel =
      SuccessOrFailureModel.fromJson(decodeJson);
      if (updatePasswordModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
                (route) => false);
        return updatePasswordModel;
      } else {
        return updatePasswordModel;
      }
    } else {
      throw ServerException();
    }
  }
}
