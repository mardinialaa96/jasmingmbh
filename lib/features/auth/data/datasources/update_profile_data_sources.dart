import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:jasmingmbh/core/error/exception.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/base_url.dart';
import 'package:jasmingmbh/features/auth/data/models/post_customer_profile_model.dart';
import 'package:jasmingmbh/features/auth/data/models/success_or_failure_model.dart';
import 'package:http/http.dart' as http;
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/block_page.dart';
import 'package:jasmingmbh/main.dart';

abstract class UpdateProfileDataSource {
  Future<SuccessOrFailure> updateProfile(PostCustomerProfileModel customerModel);
}

class UpdateProfileDataSourceImpl implements UpdateProfileDataSource {
  final http.Client client;

  UpdateProfileDataSourceImpl({required this.client});

  @override
  Future<SuccessOrFailure> updateProfile(PostCustomerProfileModel customerModel) async {
    final response = await client.put(Uri.parse('$baseUrl/update-profile'),
        body: jsonEncode(customerModel.toJson()),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ${LocalStorageService().token}',
        });
    if (response.statusCode >= 200 && response.statusCode < 300) {
      var decodeJson = json.decode(response.body);
      SuccessOrFailureModel successOrFailureModel =
          SuccessOrFailureModel.fromJson(decodeJson);
      if (successOrFailureModel.status == 2000) {
        navigatorKey.currentState!.pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => const BlockPage()),
                (route) => false);
        return successOrFailureModel;
      } else {
        return successOrFailureModel;
      }
    } else {
      throw ServerException();
    }
  }
}


