import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_profile.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/update_profile_repositories.dart';

class UpdateProfileUseCases {
  final UpdateProfileRepository repository;

  UpdateProfileUseCases(this.repository);

  Future<Either<Failure, SuccessOrFailure>> call(PostCustomerProfile customer) async {
    return await repository.updateProfile(customer);
  }
}
