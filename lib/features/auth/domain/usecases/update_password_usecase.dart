import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/update_password_repositories.dart';

class UpdatePasswordUseCase {
  final UpdatePasswordRepository repository;

  UpdatePasswordUseCase(this.repository);

  Future<Either<Failure, SuccessOrFailure>> call(
      String password) async {
    return await repository.updatePassword(password);
  }
}
