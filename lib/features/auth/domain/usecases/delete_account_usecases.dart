import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/delete_account_repositories.dart';

class DeleteAccountUseCase {
  final DeleteAccountRepository repository;

  DeleteAccountUseCase(this.repository);

  Future<Either<Failure, SuccessOrFailure>> call() async {
    return await repository.deleteAccount();
  }
}
