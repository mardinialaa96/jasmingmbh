import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_parameters.dart';
import 'package:jasmingmbh/features/auth/domain/entities/register_return_data.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/register_repositories.dart';

class RegisterUseCase {
  final RegisterRepository repository;

  RegisterUseCase(this.repository);

  Future<Either<Failure, RegisterReturnData>> call(PostCustomerParameters postCustomerParameters) async {
    return await repository.register(postCustomerParameters);
  }
}
