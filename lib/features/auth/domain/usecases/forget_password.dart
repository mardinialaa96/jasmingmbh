import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/forget_password_repositories.dart';


class ForgetPasswordUseCase {
  final ForgetPasswordRepository repository;

  ForgetPasswordUseCase(this.repository);

  Future<Either<Failure, SuccessOrFailure>> call(
      String email, String local) async {
    return await repository.forgetPassword(email, local);
  }
}
