import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';
import 'package:jasmingmbh/features/auth/domain/repositories/profile_repositories.dart';

class ProfileUseCase {
  final ProfileRepository repository;

  ProfileUseCase(this.repository);

  Future<Either<Failure, UserInfo>> call() async {
    return await repository.profile();
  }
}
