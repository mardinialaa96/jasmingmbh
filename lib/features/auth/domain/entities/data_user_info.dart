import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/auth/domain/entities/customer.dart';

class DataUserInfo extends Equatable {
  final String? token;
  final Customer? customer;

  const DataUserInfo({this.token, required this.customer});

  @override
  List<Object?> get props => [token, customer];

  Map<String, dynamic> toJson() {
    return {
      'token': token,
      'customer': customer?.toJson(),
    };
  }
}
