import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/auth/domain/entities/data_user_info.dart';

class UserInfo extends Equatable{
  final DataUserInfo? dataUserInfo;
  final String? msg;
  final int? status;
  const UserInfo(
      {required this.dataUserInfo,
        required this.msg,
        required this.status});

  @override
  List<Object?> get props => [dataUserInfo, msg, status];
}