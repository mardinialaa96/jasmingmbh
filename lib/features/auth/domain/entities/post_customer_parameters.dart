import 'package:equatable/equatable.dart';

class PostCustomerParameters extends Equatable {
  final String firstname;
  final String lastname;
  final String gender;
  final String mobile;
  final String email;
  final String city;
  final String area;
  final String address;
  final String store;
  final String password;
  final String locale;

  const PostCustomerParameters(
      {required this.firstname,
        required this.lastname,
        required this.gender,
        required this.mobile,
        required this.email,
        required this.city,
        required this.area,
        required this.address,
        required this.store,
        required this.password,
        required this.locale});

  @override
  List<Object?> get props =>
      [firstname, lastname, gender, mobile, email, city, area, address, store, password,locale];
}
