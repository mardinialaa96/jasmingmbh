import 'package:equatable/equatable.dart';

class PostCustomerProfile extends Equatable {
  final String firstname;
  final String lastname;
  final String gender;
  final String mobile;
  final String city;
  final String area;
  final String address;
  final String store;
  final String locale;

  const PostCustomerProfile(
      {required this.firstname,
        required this.lastname,
        required this.gender,
        required this.mobile,
        required this.city,
        required this.area,
        required this.address,
        required this.store,
        required this.locale});

  @override
  List<Object?> get props =>
      [firstname, lastname, gender, mobile, city, area, address, store,locale];
}
