import 'package:equatable/equatable.dart';

class PostLoginParameters extends Equatable {
  final String email;
  final String password;
  final String locale;
  final String device;

  const PostLoginParameters(
      {required this.email,
      required this.password,
      required this.locale,
      required this.device});

  @override
  List<Object?> get props => [email, password, locale, device];
}
