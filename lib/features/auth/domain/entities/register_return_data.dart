import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/features/auth/domain/entities/data_user_info.dart';

class RegisterReturnData extends Equatable {
  final int? status;
  final String? message;
  final DataUserInfo? data;
  final List<List<String>>? error;

  const RegisterReturnData({this.status, this.message, this.data,this.error});
  @override
  List<Object?> get props => [status, message, data,error];
}
