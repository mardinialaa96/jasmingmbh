import 'package:equatable/equatable.dart';

class Customer extends Equatable {
  final String firstname;
  final String lastname;
  final int gender;
  final String mobile;
  final String email;
  final String city;
  final String area;
  final String address;
  final String store;

  const Customer(
      {required this.firstname,
      required this.lastname,
      required this.gender,
      required this.mobile,
      required this.email,
      required this.city,
      required this.area,
      required this.address,
      required this.store});

  Map<String, dynamic> toJson() {
    return {
      'firstname': firstname,
      'lastname': lastname,
      'gender': gender,
      'mobile': mobile,
      'email': email,
      'city': city,
      'area': area,
      'address': address,
      'store': store,
    };
  }

  @override
  List<Object?> get props =>
      [firstname, lastname, gender, mobile, email, city, area, address, store];
}
