class City {
  final String code;
  final String nameDe;
  final String nameAr;
  final String nameEn;

  City(this.code, this.nameAr, this.nameDe,this.nameEn);
}
