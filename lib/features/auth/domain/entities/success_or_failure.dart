import 'package:equatable/equatable.dart';

class SuccessOrFailure extends Equatable {
  final int? status;
  final String? msg;
  final dynamic data;

  const SuccessOrFailure({this.status, this.msg, this.data});
  @override
  List<Object?> get props => [];
}
