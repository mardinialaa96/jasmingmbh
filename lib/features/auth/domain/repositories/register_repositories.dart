import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_parameters.dart';
import 'package:jasmingmbh/features/auth/domain/entities/register_return_data.dart';

abstract class RegisterRepository {
  Future<Either<Failure, RegisterReturnData>> register(PostCustomerParameters postCustomerParameters);
}
