import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';

abstract class DeleteAccountRepository {
  Future<Either<Failure, SuccessOrFailure>> deleteAccount();
}
