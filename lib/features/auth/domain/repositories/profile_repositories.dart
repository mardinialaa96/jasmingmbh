import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';

abstract class ProfileRepository {
  Future<Either<Failure, UserInfo>> profile();
}
