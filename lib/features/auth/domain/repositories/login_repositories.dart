import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_login_parametesd.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';

abstract class LoginRepository {
  Future<Either<Failure, UserInfo>> login(
      PostLoginParameters postLoginParameters);
}
