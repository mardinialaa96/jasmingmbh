import 'package:dartz/dartz.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_profile.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';

abstract class UpdateProfileRepository {
  Future<Either<Failure, SuccessOrFailure>> updateProfile(PostCustomerProfile customer);
}
