part of 'delete_account_bloc.dart';

@immutable
abstract class DeleteAccountEvent extends Equatable {
  const DeleteAccountEvent();

  @override
  List<Object?> get props => [];
}

class DeleteAccountUserEvent extends DeleteAccountEvent {
  const DeleteAccountUserEvent();
  @override
  List<Object?> get props => [];
}
