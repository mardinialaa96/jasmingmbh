import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/delete_account_usecases.dart';
import 'package:meta/meta.dart';

part 'delete_account_event.dart';
part 'delete_account_state.dart';

class DeleteAccountBloc extends Bloc<DeleteAccountEvent, DeleteAccountState> {
  final DeleteAccountUseCase deleteAccountUseCase;
  DeleteAccountBloc({required this.deleteAccountUseCase})
      : super(DeleteAccountInitial()) {
    on<DeleteAccountEvent>((event, emit) async {
      if (event is DeleteAccountUserEvent) {
        emit(LoadingDeleteAccountState());
        try{
          final failureOrDoneMessage = await deleteAccountUseCase();
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorDeleteAccountState(message: serverFailureMessage));
        }
      }
    });
  }
  DeleteAccountState _mapFailureOrSuccessToState(
      Either<Failure, SuccessOrFailure> either) {
    return either.fold(
        (failure) =>
            ErrorDeleteAccountState(message: _mapFailureToMessage(failure)),
        (deleteAccountInfo) =>
            LoadedDeleteAccountState(deleteAccountInfo: deleteAccountInfo));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
