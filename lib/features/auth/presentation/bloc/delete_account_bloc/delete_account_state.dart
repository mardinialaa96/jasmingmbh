part of 'delete_account_bloc.dart';

@immutable
abstract class DeleteAccountState extends Equatable {
  const DeleteAccountState();

  @override
  List<Object?> get props => [];
}

class DeleteAccountInitial extends DeleteAccountState {}

class LoadingDeleteAccountState extends DeleteAccountState {}

class ErrorDeleteAccountState extends DeleteAccountState {
  final String message;

  const ErrorDeleteAccountState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedDeleteAccountState extends DeleteAccountState {
  final SuccessOrFailure deleteAccountInfo;

  const LoadedDeleteAccountState({required this.deleteAccountInfo});
  @override
  List<Object?> get props => [deleteAccountInfo];
}
