part of 'update_password_bloc.dart';

@immutable
abstract class UpdatePasswordEvent extends Equatable {
  const UpdatePasswordEvent();

  @override
  List<Object?> get props => [];
}

class UpdatePasswordUserEvent extends UpdatePasswordEvent {
  final String password;

  const UpdatePasswordUserEvent({required this.password});
  @override
  List<Object?> get props => [password];
}
