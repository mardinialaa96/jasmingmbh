part of 'update_password_bloc.dart';

@immutable
abstract class UpdatePasswordState extends Equatable {
  const UpdatePasswordState();

  @override
  List<Object?> get props => [];
}

class UpdatePasswordInitial extends UpdatePasswordState {}

class LoadingUpdatePasswordState extends UpdatePasswordState {}

class ErrorUpdatePasswordState extends UpdatePasswordState {
  final String message;

  const ErrorUpdatePasswordState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedUpdatePasswordState extends UpdatePasswordState {
  final SuccessOrFailure updatePassword;

  const LoadedUpdatePasswordState({required this.updatePassword});
  @override
  List<Object?> get props => [updatePassword];
}
