import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/update_password_usecase.dart';
import 'package:meta/meta.dart';

part 'update_password_event.dart';
part 'update_password_state.dart';

class UpdatePasswordBloc
    extends Bloc<UpdatePasswordEvent, UpdatePasswordState> {
  final UpdatePasswordUseCase updatePasswordUseCase;
  UpdatePasswordBloc({required this.updatePasswordUseCase})
      : super(UpdatePasswordInitial()) {
    on<UpdatePasswordEvent>((event, emit) async {
      if (event is UpdatePasswordUserEvent) {
        emit(LoadingUpdatePasswordState());
        try {
          final failureOrDoneMessage =
          await updatePasswordUseCase(event.password);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorUpdatePasswordState(message: serverFailureMessage));
        }
      }
    });
  }

  UpdatePasswordState _mapFailureOrSuccessToState(
      Either<Failure, SuccessOrFailure> either) {
    return either.fold(
        (failure) =>
            ErrorUpdatePasswordState(message: _mapFailureToMessage(failure)),
        (updatePasswordInfo) =>
            LoadedUpdatePasswordState(updatePassword: updatePasswordInfo));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
