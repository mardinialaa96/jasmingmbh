part of 'manage_account_bloc.dart';

@immutable
sealed class ManageAccountState extends Equatable {
  const ManageAccountState();

  @override
  List<Object?> get props => [];
}

final class ManageAccountInitial extends ManageAccountState {}

class LoadingUpdateProfileManageAccountState extends ManageAccountState {}

class ErrorUpdateProfileManageAccountState extends ManageAccountState {
  final String message;

  const ErrorUpdateProfileManageAccountState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedUpdateProfileManageAccountState extends ManageAccountState {
  final SuccessOrFailure updateProfile;

  const LoadedUpdateProfileManageAccountState({required this.updateProfile});
  @override
  List<Object?> get props => [updateProfile];
}

class LoadingDeleteAccountManageAccountState extends ManageAccountState {}

class ErrorDeleteAccountManageAccountState extends ManageAccountState {
  final String message;

  const ErrorDeleteAccountManageAccountState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedDeleteAccountManageAccountState extends ManageAccountState {
  final SuccessOrFailure deleteAccountInfo;

  const LoadedDeleteAccountManageAccountState(
      {required this.deleteAccountInfo});
  @override
  List<Object?> get props => [deleteAccountInfo];
}
