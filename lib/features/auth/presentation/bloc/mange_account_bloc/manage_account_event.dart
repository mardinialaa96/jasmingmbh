part of 'manage_account_bloc.dart';

@immutable
sealed class ManageAccountEvent extends Equatable {
  const ManageAccountEvent();

  @override
  List<Object?> get props => [];
}

class UpdateProfileUserManageAccountEvent extends ManageAccountEvent {
  final PostCustomerProfile customer;

  const UpdateProfileUserManageAccountEvent({required this.customer});
  @override
  List<Object?> get props => [customer];
}

class DeleteAccountUserManageAccountEvent extends ManageAccountEvent {
  const DeleteAccountUserManageAccountEvent();
  @override
  List<Object?> get props => [];
}
