import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_profile.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/delete_account_usecases.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/update_profile_usecases.dart';
import 'package:meta/meta.dart';

part 'manage_account_event.dart';
part 'manage_account_state.dart';

class ManageAccountBloc extends Bloc<ManageAccountEvent, ManageAccountState> {
  final UpdateProfileUseCases updateProfileUseCases;
  final DeleteAccountUseCase deleteAccountUseCase;
  ManageAccountBloc(
      {required this.updateProfileUseCases, required this.deleteAccountUseCase})
      : super(ManageAccountInitial()) {
    on<ManageAccountEvent>((event, emit) async {
      if (event is UpdateProfileUserManageAccountEvent) {
        emit(LoadingUpdateProfileManageAccountState());
        try{
          final failureOrDoneMessage =
          await updateProfileUseCases(event.customer);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage, 'update'));
        } catch (e) {
          emit(const ErrorUpdateProfileManageAccountState(message: serverFailureMessage));
        }

      } else if (event is DeleteAccountUserManageAccountEvent) {
        emit(LoadingDeleteAccountManageAccountState());
        try {
          final failureOrDoneMessage = await deleteAccountUseCase();
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage, 'delete'));
        } catch (e) {
          emit(const ErrorDeleteAccountManageAccountState(message: serverFailureMessage));
        }
      }
    });
  }
  ManageAccountState _mapFailureOrSuccessToState(
      Either<Failure, SuccessOrFailure> either, String s) {
    return either.fold((failure) {
      if (s == 'update') {
        return ErrorUpdateProfileManageAccountState(
            message: _mapFailureToMessage(failure));
      } else {
        return ErrorDeleteAccountManageAccountState(
            message: _mapFailureToMessage(failure));
      }
    }, (invoiceFileInfo) {
      if (s == 'update') {
        return LoadedUpdateProfileManageAccountState(
            updateProfile: invoiceFileInfo);
      } else if (s == 'delete') {
        return LoadedDeleteAccountManageAccountState(
            deleteAccountInfo: invoiceFileInfo);
      } else {
        return LoadedUpdateProfileManageAccountState(
            updateProfile: invoiceFileInfo);
      }
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
