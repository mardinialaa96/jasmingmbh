part of 'login_bloc.dart';

@immutable
abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object?> get props => [];
}

class LoginUserEvent extends LoginEvent {
  final PostLoginParameters postLoginParameters;

  const LoginUserEvent({required this.postLoginParameters});
  @override
  List<Object?> get props => [postLoginParameters];
}
