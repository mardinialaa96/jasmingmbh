import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_login_parametesd.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/login_usecases.dart';
import 'package:jasmingmbh/features/notification/firebase_api.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginUseCase loginUseCase;
  LoginBloc({required this.loginUseCase}) : super(LoginInitial()) {
    on<LoginEvent>((event, emit) async {
      if (event is LoginUserEvent) {
        emit(LoadingLoginState());
        try{
          final failureOrDoneMessage =
          await loginUseCase(event.postLoginParameters);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorLoginState(message: serverFailureMessage));
        }
      }
    });
  }
  LoginState _mapFailureOrSuccessToState(Either<Failure, UserInfo> either) {
    return either.fold(
        (failure) => ErrorLoginState(message: _mapFailureToMessage(failure)),
        (userInfo) {
          if(userInfo.status == 200) {
            LocalStorageService().isToken = userInfo.dataUserInfo?.token;
            LocalStorageService().isLoggedIn = true;
            FirebaseApi().initNotifications();
          }
         return LoadedUserInfoState(userInfo: userInfo);
        });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
