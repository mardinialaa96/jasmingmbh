part of 'register_bloc.dart';

@immutable
abstract class RegisterState extends Equatable {
  const RegisterState();

  @override
  List<Object?> get props => [];
}

class RegisterInitial extends RegisterState {}

class LoadingRegisterState extends RegisterState {}

class ErrorRegisterState extends RegisterState {
  final String message;

  const ErrorRegisterState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedRegisterState extends RegisterState {
  final RegisterReturnData registerReturnData;

  const LoadedRegisterState({required this.registerReturnData});
  @override
  List<Object?> get props => [registerReturnData];
}
