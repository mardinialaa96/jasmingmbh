part of 'register_bloc.dart';

@immutable
abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object?> get props => [];
}

class RegisterUserEvent extends RegisterEvent {
  final PostCustomerParameters postCustomerParameters;

  const RegisterUserEvent({required this.postCustomerParameters});
  @override
  List<Object?> get props => [postCustomerParameters];
}
