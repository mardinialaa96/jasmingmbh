import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_parameters.dart';
import 'package:jasmingmbh/features/auth/domain/entities/register_return_data.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/register_usecases.dart';
import 'package:meta/meta.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final RegisterUseCase registerUseCase;
  RegisterBloc({required this.registerUseCase}) : super(RegisterInitial()) {
    on<RegisterEvent>((event, emit) async {
      if (event is RegisterUserEvent) {
        emit(LoadingRegisterState());
        try{
          final failureOrDoneMessage = await registerUseCase(event.postCustomerParameters);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorRegisterState(message: serverFailureMessage));
        }

      }
    });
  }
  RegisterState _mapFailureOrSuccessToState(
      Either<Failure, RegisterReturnData> either) {
    return either.fold(
        (failure) => ErrorRegisterState(message: _mapFailureToMessage(failure)),
        (registerReturnData) {
      return LoadedRegisterState(registerReturnData: registerReturnData);
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
