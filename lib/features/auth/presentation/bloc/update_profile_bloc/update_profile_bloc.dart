import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_profile.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/update_profile_usecases.dart';
import 'package:meta/meta.dart';

part 'update_profile_event.dart';
part 'update_profile_state.dart';

class UpdateProfileBloc extends Bloc<UpdateProfileEvent, UpdateProfileState> {
  final UpdateProfileUseCases updateProfileUseCases;
  UpdateProfileBloc({required this.updateProfileUseCases})
      : super(UpdateProfileInitial()) {
    on<UpdateProfileEvent>((event, emit) async {
      if (event is UpdateProfileUserEvent) {
        emit(LoadingUpdateProfileState());
        try {
          final failureOrDoneMessage =
          await updateProfileUseCases(event.customer);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorUpdateProfileState(message: serverFailureMessage));
        }
        /*final failureOrDoneMessage =
            await updateProfileUseCases(event.customer);
        emit(_mapFailureOrSuccessToState(failureOrDoneMessage));*/
      }
    });
  }

  UpdateProfileState _mapFailureOrSuccessToState(
      Either<Failure, SuccessOrFailure> either) {
    return either.fold(
        (failure) =>
            ErrorUpdateProfileState(message: _mapFailureToMessage(failure)),
        (updatePasswordInfo) =>
            LoadedUpdateProfileState(updateProfile: updatePasswordInfo));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
