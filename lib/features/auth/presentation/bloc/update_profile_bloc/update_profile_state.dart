part of 'update_profile_bloc.dart';

@immutable
abstract class UpdateProfileState extends Equatable {
  const UpdateProfileState();

  @override
  List<Object?> get props => [];
}

class UpdateProfileInitial extends UpdateProfileState {}

class LoadingUpdateProfileState extends UpdateProfileState {}

class ErrorUpdateProfileState extends UpdateProfileState {
  final String message;

  const ErrorUpdateProfileState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedUpdateProfileState extends UpdateProfileState {
  final SuccessOrFailure updateProfile;

  const LoadedUpdateProfileState({required this.updateProfile});
  @override
  List<Object?> get props => [updateProfile];
}
