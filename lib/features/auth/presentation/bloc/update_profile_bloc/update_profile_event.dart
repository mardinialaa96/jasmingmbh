part of 'update_profile_bloc.dart';

@immutable
abstract class UpdateProfileEvent extends Equatable {
  const UpdateProfileEvent();

  @override
  List<Object?> get props => [];
}

class UpdateProfileUserEvent extends UpdateProfileEvent {
  final PostCustomerProfile customer;

  const UpdateProfileUserEvent({required this.customer});
  @override
  List<Object?> get props => [customer];
}
