import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/success_or_failure.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/forget_password.dart';
import 'package:meta/meta.dart';

part 'forget_password_event.dart';
part 'forget_password_state.dart';

class ForgetPasswordBloc
    extends Bloc<ForgetPasswordEvent, ForgetPasswordState> {
  final ForgetPasswordUseCase forgetPasswordUseCase;
  ForgetPasswordBloc({required this.forgetPasswordUseCase})
      : super(ForgetPasswordInitial()) {
    on<ForgetPasswordEvent>((event, emit) async {
      if (event is ForgetPasswordUserEvent) {
        emit(LoadingForgetPasswordState());
        try {
          final failureOrDoneMessage =
              await forgetPasswordUseCase(event.email, event.locale);
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorForgetPasswordState(message: serverFailureMessage));
        }
      }
    });
  }
  ForgetPasswordState _mapFailureOrSuccessToState(
      Either<Failure, SuccessOrFailure> either) {
    return either.fold(
        (failure) =>
            ErrorForgetPasswordState(message: _mapFailureToMessage(failure)),
        (forgetPasswordInfo) =>
            LoadedForgetPasswordState(forgetPassword: forgetPasswordInfo));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
