part of 'forget_password_bloc.dart';

@immutable
abstract class ForgetPasswordState extends Equatable {
  const ForgetPasswordState();

  @override
  List<Object?> get props => [];
}

class ForgetPasswordInitial extends ForgetPasswordState {}

class LoadingForgetPasswordState extends ForgetPasswordState {}

class ErrorForgetPasswordState extends ForgetPasswordState {
  final String message;

  const ErrorForgetPasswordState({required this.message});
  @override
  List<Object?> get props => [message];
}

class LoadedForgetPasswordState extends ForgetPasswordState {
  final SuccessOrFailure forgetPassword;

  const LoadedForgetPasswordState({required this.forgetPassword});
  @override
  List<Object?> get props => [forgetPassword];
}
