part of 'forget_password_bloc.dart';

@immutable
sealed class ForgetPasswordEvent extends Equatable {
  const ForgetPasswordEvent();

  @override
  List<Object?> get props => [];
}

class ForgetPasswordUserEvent extends ForgetPasswordEvent {
  final String email;
  final String locale;

  const ForgetPasswordUserEvent({required this.email, required this.locale});
  @override
  List<Object?> get props => [email, locale];
}
