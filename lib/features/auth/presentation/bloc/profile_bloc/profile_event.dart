part of 'profile_bloc.dart';

@immutable
abstract class ProfileEvent extends Equatable {
  const ProfileEvent();

  @override
  List<Object?> get props => [];
}

class ProfileUserEvent extends ProfileEvent {
  const ProfileUserEvent();
  @override
  List<Object?> get props => [];
}

