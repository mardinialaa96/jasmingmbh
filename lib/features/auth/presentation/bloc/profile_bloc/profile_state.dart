part of 'profile_bloc.dart';

@immutable
abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object?> get props => [];
}

class ProfileInitial extends ProfileState {
  @override
  List<Object?> get props => [];
}

class LoadingProfileState extends ProfileState {
  @override
  List<Object?> get props => [];
}

class LoadedProfileState extends ProfileState {
  final UserInfo userInfo;

  const LoadedProfileState({required this.userInfo});
  @override
  List<Object?> get props => [userInfo];
}

class ErrorProfileState extends ProfileState {
  final String message;

  const ErrorProfileState({required this.message});
  @override
  List<Object?> get props => [message];
}

