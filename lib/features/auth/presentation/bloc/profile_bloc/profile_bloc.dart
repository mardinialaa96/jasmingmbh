import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:jasmingmbh/core/error/failures.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/features/auth/domain/entities/user_info.dart';
import 'package:jasmingmbh/features/auth/domain/usecases/profile_usecases.dart';
import 'package:meta/meta.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileUseCase profileUseCase;
  ProfileBloc({required this.profileUseCase}) : super(ProfileInitial()) {
    on<ProfileEvent>((event, emit) async {
      if (event is ProfileUserEvent) {
        emit(LoadingProfileState());
        try {
          final failureOrDoneMessage = await profileUseCase();
          emit(_mapFailureOrSuccessToState(failureOrDoneMessage));
        } catch (e) {
          emit(const ErrorProfileState(message: serverFailureMessage));
        }
      }
    });
  }

  ProfileState _mapFailureOrSuccessToState(Either<Failure, UserInfo> either) {
    return either.fold(
        (failure) => ErrorProfileState(message: _mapFailureToMessage(failure)),
        (userInfo) {
      return LoadedProfileState(userInfo: userInfo);
    });
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return serverFailureMessage;
      case EmptyCacheFailure:
        return emptyCacheFailureMessage;
      case OfflineFailure:
        return offlineFailureMessage;
      default:
        return 'Unexpected Error, Please try again later';
    }
  }
}
