import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/custom_textfield.dart';
import 'package:jasmingmbh/core/widgets/loading_widget.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_parameters.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/register_bloc/register_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/login_page.dart';
import 'package:jasmingmbh/features/auth/signIn_helper.dart';
import 'package:jasmingmbh/features/terms_of_service/presentation/pages/terms_of_service.dart';

class SignUpPageInfoTwo extends StatefulWidget {
  final String firstName;
  final String lastName;
  final String phone;
  final String email;
  final int gender;
  final String countryCode;
  const SignUpPageInfoTwo(
      {super.key,
      required this.firstName,
      required this.lastName,
      required this.phone,
      required this.email,
      required this.gender,
      required this.countryCode});

  @override
  State<SignUpPageInfoTwo> createState() => SignUpPageInfoTwoState();
}

class SignUpPageInfoTwoState extends State<SignUpPageInfoTwo> {
  final formKeyTwoPage = GlobalKey<FormState>();
  bool isChecked = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: _buildBody()),
    );
  }

  Widget _buildBody() => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: BlocConsumer<RegisterBloc, RegisterState>(
          listener: (context, state) {
            if (state is LoadedRegisterState) {
              if (state.registerReturnData.status != 200) {
                String errorMessage = '';
                if (state.registerReturnData.error != null) {
                  errorMessage = state.registerReturnData.error!
                      .map((list) => list.join('\n'))
                      .join('\n');
                }
                SnackBarMessage().showErrorSnackBarMessage(
                    message:
                        '${state.registerReturnData.message} \n $errorMessage',
                    context: context);
              } else {
                showDialog<void>(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text(
                        state.registerReturnData.message ?? "",
                        style: AppTextStyle.titleBoldDark,
                      ),
                      actions: [
                        TextButton(
                          onPressed: () {
                            SignInHelper().clearSignInHelper();
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                  builder: (context) => const LogInPage(),
                                ),
                                (Route<dynamic> route) => false);
                          },
                          child: Text(
                            'ok'.tr(context),
                            style: AppTextStyle.subtitleBold,
                          ),
                        ),
                      ],
                    );
                  },
                );
              }
            } else if (state is ErrorRegisterState) {
              SnackBarMessage().showErrorSnackBarMessage(
                  message: state.message == offlineFailureMessage
                      ? 'no_internet'.tr(context)
                      : state.message == serverFailureMessage
                      ? 'try_again'.tr(context)
                      : state.message, context: context);
            }
          },
          builder: (context, state) {
            if (state is LoadingRegisterState) {
              return const LoadingWidget();
            }
            return Form(
              key: formKeyTwoPage,
              child: ListView(
                shrinkWrap: true,
                children: [
                  const SizedBox(height: 20),
                  Text("new_account".tr(context),
                      style: AppTextStyle.titleExtraBoldDark),
                  const SizedBox(height: 10),
                  Text(
                    'create_new_account'.tr(context),
                    style: AppTextStyle.subtitleBold,
                  ),
                  const SizedBox(height: 30),
                  Row(
                    children: [
                      const Icon(
                        Icons.add_business,
                        color: AppColors.iconColor,
                        size: 25,
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'shop_name'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  CustomTextFormField(
                    controller: SignInHelper.shopNameController,
                    focusNode: SignInHelper.shopNameFocusNode,
                    validator: (value) =>
                        value!.isEmpty ? "cant_be_empty".tr(context) : null,
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      const Icon(
                        Icons.location_city,
                        color: AppColors.iconColor,
                        size: 25,
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'city'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  DropdownButton<String>(
                      isExpanded: true,
                      style: AppTextStyle.subtitleBold,
                      value: SignInHelper.selectedCityItem?.code,
                      items: SignInHelper.city
                          .map((item) => DropdownMenuItem<String>(
                              value: item.code, // Use code as value
                              child: Text(
                                LocalStorageService().currentLang == 'ar'
                                    ? item.nameAr
                                    : LocalStorageService().currentLang == 'de'
                                        ? item.nameDe
                                        : item.nameEn,
                                style: AppTextStyle.subtitleBold,
                              )))
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          SignInHelper.selectedCityItem = SignInHelper.city
                              .firstWhere((city) => city.code == value);
                        });
                      }),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      const Icon(
                        Icons.location_on,
                        color: AppColors.iconColor,
                        size: 25,
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'district'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  CustomTextFormField(
                    controller: SignInHelper.districtController,
                    focusNode: SignInHelper.districtFocusNode,
                    validator: (value) =>
                        value!.isEmpty ? "cant_be_empty".tr(context) : null,
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      const Icon(
                        Icons.location_on,
                        color: AppColors.iconColor,
                        size: 25,
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'address'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  CustomTextFormField(
                    controller: SignInHelper.addressController,
                    focusNode: SignInHelper.addressFocusNode,
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            SignInHelper.isObscure = !SignInHelper.isObscure;
                          });
                        },
                        child: SignInHelper.isObscure
                            ? const Icon(
                                Icons.visibility,
                                color: AppColors.iconColor,
                                size: 25,
                              )
                            : const Icon(
                                Icons.visibility_off,
                                color: AppColors.iconColor,
                                size: 25,
                              ),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'password'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  CustomTextFormField(
                    label: '***********',
                    controller: SignInHelper.passwordController,
                    focusNode: SignInHelper.passwordFocusNode,
                    isObscureText: SignInHelper.isObscure,
                    validator: (value) =>
                        value!.isEmpty ? "cant_be_empty".tr(context) : null,
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            SignInHelper.isObscure2 = !SignInHelper.isObscure2;
                          });
                        },
                        child: SignInHelper.isObscure2
                            ? const Icon(
                                Icons.visibility,
                                color: AppColors.iconColor,
                                size: 25,
                              )
                            : const Icon(
                                Icons.visibility_off,
                                color: AppColors.iconColor,
                                size: 25,
                              ),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'confirm_password'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  CustomTextFormField(
                    label: '***********',
                    controller: SignInHelper.confirmPasswordController,
                    focusNode: SignInHelper.confirmPasswordFocusNode,
                    isObscureText: SignInHelper.isObscure2,
                    validator: (value) =>
                        value!.isEmpty ? "cant_be_empty".tr(context) : null,
                  ),
                  const SizedBox(height: 30),
                  Row(
                    children: [
                      Checkbox(
                        activeColor: AppColors.appColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                        side: const BorderSide(color: AppColors.secondColor),
                        value: isChecked,
                        onChanged: (value) {
                          setState(() {
                            isChecked = value ?? false;
                          });
                        },
                      ),
                      Expanded(
                        child: Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'agree_to'.tr(context),
                                style: AppTextStyle.subtitleBold,
                              ),
                              const WidgetSpan(child: SizedBox(width: 8.0)),
                              TextSpan(
                                text: 'privacy_policy'.tr(context),
                                style: AppTextStyle.subtitleBold.copyWith(
                                    color: AppColors.secondaryTextColor,
                                    decoration: TextDecoration.underline,
                                    decorationColor:
                                    AppColors.secondaryTextColor),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                          const TermsOfService())),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 50),
                  Row(
                    children: [
                      Expanded(
                        child: CustomButton(
                          text: 'previous'.tr(context),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: CustomButton(
                          backgroundColor: isChecked
                              ? const MaterialStatePropertyAll(
                                  AppColors.appColor)
                              : MaterialStatePropertyAll(
                                  AppColors.appColor.withOpacity(0.2)),
                          text: 'create_account'.tr(context),
                          onPressed: () {
                            final isValid =
                                formKeyTwoPage.currentState!.validate();
                            if (isValid) {
                              if (SignInHelper.selectedCityItem == null) {
                                SnackBarMessage().showErrorSnackBarMessage(
                                    message: "must_enter_city".tr(context),
                                    context: context);
                              } else if (isChecked == false) {
                                SnackBarMessage().showErrorSnackBarMessage(
                                    message: "must_agree_privacy".tr(context),
                                    context: context);
                              } else {
                                String country = '';
                                String phone = '';
                                if (widget.countryCode.startsWith('+')) {
                                  country =
                                      '00${widget.countryCode.substring(1)}';
                                }
                                if (SignInHelper.phoneController.text
                                    .startsWith('0')) {
                                  phone = SignInHelper.phoneController.text
                                      .substring(1);
                                } else {
                                  phone = SignInHelper.phoneController.text;
                                }
                                final PostCustomerParameters postCustomer =
                                    PostCustomerParameters(
                                        email: widget.email,
                                        firstname: widget.firstName,
                                        lastname: widget.lastName,
                                        mobile: country + phone,
                                        gender: widget.gender.toString(),
                                        store: SignInHelper
                                            .shopNameController.text,
                                        city:
                                            SignInHelper.selectedCityItem!.code,
                                        address:
                                            SignInHelper.addressController.text,
                                        area: SignInHelper
                                            .districtController.text,
                                        password: SignInHelper
                                            .passwordController.text,
                                        locale:
                                            LocalStorageService().currentLang ??
                                                'de');
                                BlocProvider.of<RegisterBloc>(context).add(
                                    RegisterUserEvent(
                                        postCustomerParameters: postCustomer));
                              }
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'have_already_account'.tr(context),
                        style: AppTextStyle.subtitleBold,
                      ),
                      const SizedBox(width: 8),
                      InkWell(
                        onTap: () {
                          SignInHelper().clearSignInHelper();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                  const LogInPage()));
                        },
                        child: Text('login'.tr(context),
                            style: AppTextStyle.subtitleBold.copyWith(
                                color: AppColors.secondaryTextColor,
                                decoration: TextDecoration.underline,
                                decorationColor: AppColors.secondaryTextColor)),
                      ),
                    ],
                  ),
                  const SizedBox(height: 50),
                ],
              ),
            );
          },
        ),
      );
}
