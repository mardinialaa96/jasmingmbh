import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/custom_textfield.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/signup_page_info_two.dart';
import 'package:jasmingmbh/features/auth/signIn_helper.dart';

class SignUpPageInfoOne extends StatefulWidget {
  const SignUpPageInfoOne({super.key});

  @override
  State<SignUpPageInfoOne> createState() => SignUpPageInfoOneState();
}

class SignUpPageInfoOneState extends State<SignUpPageInfoOne> {
  final GlobalKey<FormState> formKeyOnePage = GlobalKey<FormState>();
  String countryCode = '+49';

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: _buildBody(context)),
      bottomNavigationBar:     Padding(
        padding: const EdgeInsets.all(8.0),
        child: CustomButton(
            text: 'next'.tr(context),
            onPressed: () {
              final isValid = formKeyOnePage.currentState?.validate();
              if (isValid == true) {
                if (SignInHelper.selectedItem == null) {
                  SnackBarMessage().showErrorSnackBarMessage(
                      message: "must_enter_gender".tr(context),
                      context: context);
                  return;
                } else
                if (SignInHelper.phoneController.text.isEmpty) {
                  SnackBarMessage().showErrorSnackBarMessage(
                    message: "phone_number_cant_be_empty".tr(context),
                    context: context,
                  );
                  return;
                }
                else {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              SignUpPageInfoTwo(
                                  firstName: SignInHelper
                                      .firstNameController.text,
                                  lastName: SignInHelper
                                      .lastNameController.text,
                                  phone:
                                  SignInHelper.phoneController.text,
                                  countryCode: countryCode,
                                  email:
                                  SignInHelper.emailController.text,
                                  gender: SignInHelper.selectedItem!)));
                }
              }
            }),
      ),
    );
  }

  Widget _buildBody(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: SingleChildScrollView(
          child: Form(
            key: formKeyOnePage,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20),
                Text("new_account".tr(context),
                    style: AppTextStyle.titleExtraBoldDark),
                const SizedBox(height: 10),
                Text(
                  'create_new_account'.tr(context),
                  style: AppTextStyle.subtitleBold,
                ),
                const SizedBox(height: 30),
                Row(
                  children: [
                    const Icon(
                      Icons.person,
                      color: AppColors.iconColor,
                      size: 25,
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        'first_name'.tr(context),
                        style: AppTextStyle.titleDark,
                      ),
                    )
                  ],
                ),
                CustomTextFormField(
                  controller: SignInHelper.firstNameController,
                  focusNode: SignInHelper.firstNameFocusNode,
                  type: TextInputType.name,
                  validator: (value) =>
                      value!.isEmpty ? "cant_be_empty".tr(context) : null,
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    const Icon(
                      Icons.person,
                      color: AppColors.iconColor,
                      size: 25,
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        'last_name'.tr(context),
                        style: AppTextStyle.titleDark,
                      ),
                    )
                  ],
                ),
                CustomTextFormField(
                  controller: SignInHelper.lastNameController,
                  focusNode: SignInHelper.lastNameFocusNode,
                  type: TextInputType.name,
                  validator: (value) =>
                      value!.isEmpty ? "cant_be_empty".tr(context) : null,
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    const Icon(
                      Icons.transgender,
                      color: AppColors.iconColor,
                      size: 25,
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        'gender'.tr(context),
                        style: AppTextStyle.titleDark,
                      ),
                    )
                  ],
                ),
                DropdownButton<int>(
                    style: AppTextStyle.subtitleBold,
                    icon: const Icon(
                      Icons
                          .arrow_drop_down, // Specify the icon for the dropdown button
                      color: AppColors.iconColor,
                    ),
                    isExpanded: true,
                    value: SignInHelper.selectedItem,
                    items: SignInHelper.genders
                        .map((item) => DropdownMenuItem<int>(
                            value: item.id,
                            child: Text(
                              item.id == 0
                                  ? 'male'.tr(context)
                                  : 'female'.tr(context),
                              style: AppTextStyle.subtitleBold,
                            )))
                        .toList(),
                    onChanged: (value) {
                      setState(() => SignInHelper.selectedItem = value);
                    }),
                const SizedBox(height: 10),
                Row(
                  children: [
                    const Icon(
                      Icons.phone,
                      color: AppColors.iconColor,
                      size: 25,
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        'phone_number'.tr(context),
                        style: AppTextStyle.titleDark,
                      ),
                    )
                  ],
                ),
                IntlPhoneField(
                  controller: SignInHelper.phoneController,
                  focusNode: SignInHelper.phoneFocusNode,
                  keyboardType: TextInputType.phone,
                  cursorColor: AppColors.appColor,
                  validator: (value) => value!.completeNumber.isEmpty
                      ? "cant_be_empty".tr(context)
                      : null,
                  decoration: InputDecoration(
                      border: UnderlineInputBorder(
                          borderSide:
                              const BorderSide(color: AppColors.grayColor),
                          borderRadius: BorderRadius.circular(10)),
                      enabledBorder: UnderlineInputBorder(
                          borderSide:
                              const BorderSide(color: AppColors.grayColor),
                          borderRadius: BorderRadius.circular(10)),
                      focusedBorder: UnderlineInputBorder(
                          borderSide:
                              const BorderSide(color: AppColors.grayColor),
                          borderRadius: BorderRadius.circular(10)),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 5.0, horizontal: 5),
                      labelText: 'enter_number'.tr(context),
                      labelStyle: AppTextStyle.subtitleBold),
                  flagsButtonPadding: const EdgeInsets.symmetric(horizontal: 8),
                  dropdownIcon: const Icon(
                    Icons.arrow_drop_down,
                    color: AppColors.appColor,
                  ),
                  textAlign: TextAlign.start,
                  initialCountryCode: 'DE',
                  onChanged: (phone) {
                    setState(() {
                      countryCode = phone.countryCode;
                    });
                  },
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    const Icon(
                      Icons.email,
                      color: AppColors.iconColor,
                      size: 25,
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Text(
                        'email'.tr(context),
                        style: AppTextStyle.titleDark,
                      ),
                    )
                  ],
                ),
                CustomTextFormField(
                  controller: SignInHelper.emailController,
                  focusNode: SignInHelper.emailFocusNode,
                  type: TextInputType.emailAddress,
                  validator: (value) =>
                      SignInHelper().validateEmail(value!, context),
                ),


              ],
            ),
          ),
        ),
      );
}
