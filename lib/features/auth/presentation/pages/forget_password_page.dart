import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/custom_textfield.dart';
import 'package:jasmingmbh/core/widgets/loading_widget.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/forget_password_bloc/forget_password_bloc.dart';

class ForgetPasswordPage extends StatefulWidget {
  const ForgetPasswordPage({super.key});

  @override
  State<ForgetPasswordPage> createState() => ForgetPasswordPageState();
}

class ForgetPasswordPageState extends State<ForgetPasswordPage> {
  final emailController = TextEditingController();
  final emailFocusNode = FocusNode();

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  String? _validateEmail(String value) {
    String emailPattern = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$';
    RegExp regExp = RegExp(emailPattern);

    if (value.isEmpty) {
      return 'empty_email_message'.tr(context);
    } else if (!regExp.hasMatch(value)) {
      return 'valid_email_message'.tr(context);
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppColors.backgroundColor,
        title: Text('resetPassword'.tr(context), style: AppTextStyle.titleBoldDark),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: BlocConsumer<ForgetPasswordBloc, ForgetPasswordState>(
          listener: (context, state) {
            if (state is LoadedForgetPasswordState) {
              if (state.forgetPassword.status != 200) {
                SnackBarMessage().showErrorSnackBarMessage(
                    message: state.forgetPassword.msg ?? "", context: context);
              } else {
                SnackBarMessage().showSuccessSnackBarMessage(
                    message: state.forgetPassword.msg ?? "", context: context);
                emailController.text = '';
              }
            } else if (state is ErrorForgetPasswordState) {
              SnackBarMessage().showErrorSnackBarMessage(
                  message: state.message == offlineFailureMessage
                      ? 'no_internet'.tr(context)
                      : state.message == serverFailureMessage
                      ? 'try_again'.tr(context)
                      : state.message,
                  context: context);
            }
          },
          builder: (context, state) {
            if (state is LoadingForgetPasswordState) {
              return const LoadingWidget();
            }
            return Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("reset_password_message".tr(context),
                      style: AppTextStyle.subtitleGrayBold,textAlign: TextAlign.center,),
                  const SizedBox(height: 30),
                  Row(
                    children: [
                      const Icon(
                        Icons.email,
                        color: AppColors.iconColor,
                        size: 25,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'email'.tr(context),
                        style: AppTextStyle.titleDark,
                      )
                    ],
                  ),
                  CustomTextFormField(
                    controller: emailController,
                    focusNode: emailFocusNode,
                    type: TextInputType.emailAddress,
                    validator: (value) => _validateEmail(value!),
                  ),
                  const SizedBox(height: 50),
                  SizedBox(
                      width: double.infinity,
                      child: CustomButton(
                          text: "confirm".tr(context),
                          onPressed: () {
                            final isValid = _formKey.currentState!.validate();
                            if (isValid) {
                              showDialog<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text(
                                      'reset_password_dialog_message'.tr(context),
                                      style: AppTextStyle.titleBoldDark,
                                    ),
                                    actions: [
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          BlocProvider.of<ForgetPasswordBloc>(
                                                  context)
                                              .add(ForgetPasswordUserEvent(
                                                  email: emailController.text,
                                                  locale:
                                                      '${LocalStorageService().currentLang}'));
                                        },
                                        child: Text(
                                          'confirm'.tr(context),
                                          style: AppTextStyle.subtitleBold,
                                        ),
                                      ),
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        child: Text('cancel'.tr(context),
                                            style: AppTextStyle.subtitleBold
                                                .copyWith(
                                                    color: AppColors.redColor)),
                                      ),
                                    ],
                                  );
                                },
                              );
                            }
                          }))
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
