import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/custom_textfield.dart';
import 'package:jasmingmbh/core/widgets/error_data_widget.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_customer_profile.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/mange_account_bloc/manage_account_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/profile_bloc/profile_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/login_page.dart';
import 'package:jasmingmbh/features/auth/profile_helper.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';

class ProfilePage extends StatefulWidget {
  final bool isTabs;
  const ProfilePage({super.key, this.isTabs = false});

  @override
  State<ProfilePage> createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    BlocProvider.of<ProfileBloc>(context).add(const ProfileUserEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: widget.isTabs
          ? null
          : AppBar(
              backgroundColor: AppColors.backgroundColor,
              title: Text('my_profile'.tr(context),
                  style: AppTextStyle.titleBoldDark),
            ),
      body: Column(
        children: [
          if (widget.isTabs)
            const SizedBox(
              height: 30,
            ),
          if (widget.isTabs)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                children: <Widget>[
                  Text('my_profile'.tr(context),
                      style: AppTextStyle.titleExtraBoldDark),
                ],
              ),
            ),
          Expanded(
              child: BlocListener<LocalCubit, LocalState>(
            listener: (context, state) {
              BlocProvider.of<ProfileBloc>(context)
                  .add(const ProfileUserEvent());
            },
            child: BlocBuilder<ProfileBloc, ProfileState>(
              builder: (context, state) {
                if (state is LoadingProfileState) {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: AppColors.appColor,
                    ),
                  );
                } else if (state is LoadedProfileState) {
                  if (state.userInfo.dataUserInfo != null &&
                      state.userInfo.dataUserInfo?.customer != null) {
                    ProfileHelper.firstNameController.text =
                        state.userInfo.dataUserInfo?.customer?.firstname ?? "";
                    ProfileHelper.lastNameController.text =
                        state.userInfo.dataUserInfo?.customer?.lastname ?? "";
                    ProfileHelper.phoneController.text =
                        state.userInfo.dataUserInfo?.customer?.mobile ?? "";
                    ProfileHelper.emailController.text =
                        state.userInfo.dataUserInfo?.customer?.email ?? "";
                    ProfileHelper.shopNameController.text =
                        state.userInfo.dataUserInfo?.customer?.store ?? "";
                    ProfileHelper.districtController.text =
                        state.userInfo.dataUserInfo?.customer?.area ?? "";
                    ProfileHelper.addressController.text =
                        state.userInfo.dataUserInfo?.customer?.address ?? "";
                    ProfileHelper.selectedItem ??=
                        state.userInfo.dataUserInfo?.customer?.gender;
                    ProfileHelper.selectedCityItem ??=
                        ProfileHelper.city.firstWhere(
                      (city) =>
                          city.nameAr ==
                              state.userInfo.dataUserInfo?.customer?.city ||
                          city.nameDe ==
                              state.userInfo.dataUserInfo?.customer?.city ||
                          city.nameEn ==
                              state.userInfo.dataUserInfo?.customer?.city,
                    );
                  }
                  return Column(
                    children: [
                      Expanded(
                        child: Form(
                          key: _formKey,
                          child: ListView(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 15, vertical: 15),
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 15.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: AppColors.whiteColor,
                                      border: Border.all(
                                          color: AppColors.appColor)),
                                  child: const Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Icon(
                                      Icons.person,
                                      size: 60,
                                      color: AppColors.appColor,
                                    ),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.person,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'first_name'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              CustomTextFormField(
                                controller: ProfileHelper.firstNameController,
                                focusNode: ProfileHelper.firstNameFocusNode,
                                type: TextInputType.name,
                                validator: (value) => value!.isEmpty
                                    ? "cant_be_empty".tr(context)
                                    : null,
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.person,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'last_name'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              CustomTextFormField(
                                controller: ProfileHelper.lastNameController,
                                focusNode: ProfileHelper.lastNameFocusNode,
                                type: TextInputType.name,
                                validator: (value) => value!.isEmpty
                                    ? "cant_be_empty".tr(context)
                                    : null,
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.transgender,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'gender'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              DropdownButton<int>(
                                  style: AppTextStyle.subtitleBold,
                                  icon: const Icon(
                                    Icons
                                        .arrow_drop_down, // Specify the icon for the dropdown button
                                    color: AppColors.iconColor,
                                  ),
                                  isExpanded: true,
                                  value: ProfileHelper.selectedItem,
                                  items: ProfileHelper.genders
                                      .map((item) => DropdownMenuItem<int>(
                                          value: item.id,
                                          child: Text(
                                            item.id == 0
                                                ? 'male'.tr(context)
                                                : 'female'.tr(context),
                                            style: AppTextStyle.subtitleBold,
                                          )))
                                      .toList(),
                                  onChanged: (value) {
                                    setState(() =>
                                        ProfileHelper.selectedItem = value);
                                  }),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.phone,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'phone_number'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              CustomTextFormField(
                                label: 'enter_number_with_code'.tr(context),
                                controller: ProfileHelper.phoneController,
                                focusNode: ProfileHelper.phoneFocusNode,
                                type: TextInputType.phone,
                                validator: (value) => value!.isEmpty
                                    ? "cant_be_empty".tr(context)
                                    : null,
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.email,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'email'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              CustomTextFormField(
                                readOnly: true,
                                controller: ProfileHelper.emailController,
                                focusNode: ProfileHelper.emailFocusNode,
                                type: TextInputType.emailAddress,
                                validator: (value) => ProfileHelper()
                                    .validateEmail(value!, context),
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.add_business,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'shop_name'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              CustomTextFormField(
                                controller: ProfileHelper.shopNameController,
                                focusNode: ProfileHelper.shopNameFocusNode,
                                validator: (value) => value!.isEmpty
                                    ? "cant_be_empty".tr(context)
                                    : null,
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.location_city,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'city'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              DropdownButton<String>(
                                  isExpanded: true,
                                  style: AppTextStyle.subtitleBold,
                                  value: ProfileHelper.selectedCityItem?.code,
                                  items: ProfileHelper.city
                                      .map((item) => DropdownMenuItem<String>(
                                          value: item.code,
                                          child: Text(
                                            LocalStorageService().currentLang ==
                                                    'ar'
                                                ? item.nameAr
                                                : LocalStorageService()
                                                            .currentLang ==
                                                        'de'
                                                    ? item.nameDe
                                                    : item.nameEn,
                                            style: AppTextStyle.subtitleBold,
                                          )))
                                      .toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      ProfileHelper.selectedCityItem =
                                          ProfileHelper.city.firstWhere(
                                              (city) => city.code == value);
                                    });
                                  }),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.location_on,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'district'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              CustomTextFormField(
                                controller: ProfileHelper.districtController,
                                focusNode: ProfileHelper.districtFocusNode,
                                validator: (value) => value!.isEmpty
                                    ? "cant_be_empty".tr(context)
                                    : null,
                              ),
                              const SizedBox(height: 10),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.location_on,
                                    color: AppColors.iconColor,
                                    size: 25,
                                  ),
                                  const SizedBox(width: 8),
                                  Expanded(
                                    child: Text(
                                      'address'.tr(context),
                                      style: AppTextStyle.titleDark,
                                    ),
                                  )
                                ],
                              ),
                              CustomTextFormField(
                                controller: ProfileHelper.addressController,
                                focusNode: ProfileHelper.addressFocusNode,
                                validator: (value) => value!.isEmpty
                                    ? "cant_be_empty".tr(context)
                                    : null,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                          padding: widget.isTabs
                              ? const EdgeInsets.only(
                                  bottom: 8, left: 8, right: 8)
                              : const EdgeInsets.all(8.0),
                          child: BlocConsumer<ManageAccountBloc,
                              ManageAccountState>(
                            listener: (context, state) {
                              if (state
                                  is LoadedUpdateProfileManageAccountState) {
                                if (state.updateProfile.status != 200) {
                                  SnackBarMessage().showErrorSnackBarMessage(
                                      message: state.updateProfile.msg ?? "",
                                      context: context);
                                } else {
                                  SnackBarMessage().showSuccessSnackBarMessage(
                                      message: state.updateProfile.msg ?? "",
                                      context: context);
                                  BlocProvider.of<ProfileBloc>(context)
                                      .add(const ProfileUserEvent());
                                }
                              } else if (state
                                  is LoadedDeleteAccountManageAccountState) {
                                if (state.deleteAccountInfo.status != 200) {
                                  SnackBarMessage().showErrorSnackBarMessage(
                                      message:
                                          state.deleteAccountInfo.msg ?? "",
                                      context: context);
                                } else {
                                  SnackBarMessage().showSuccessSnackBarMessage(
                                      message:
                                          state.deleteAccountInfo.msg ?? "",
                                      context: context);
                                  LocalStorageService().isLoggedIn = null;
                                  LocalStorageService().isFirstName = null;
                                  LocalStorageService().isLastName = null;
                                  BlocProvider.of<CartBloc>(context)
                                      .add(ClearCart());
                                  Navigator.of(context, rootNavigator: true)
                                      .pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const LogInPage()),
                                          (route) => false);
                                }
                              } else if (state
                                  is ErrorUpdateProfileManageAccountState) {
                                SnackBarMessage().showErrorSnackBarMessage(
                                    message: state.message == offlineFailureMessage
                                        ? 'no_internet'.tr(context)
                                        : state.message == serverFailureMessage
                                        ? 'try_again'.tr(context)
                                        : state.message, context: context);
                              } else if (state
                                  is ErrorDeleteAccountManageAccountState) {
                                SnackBarMessage().showErrorSnackBarMessage(
                                    message: state.message == offlineFailureMessage
                                        ? 'no_internet'.tr(context)
                                        : state.message == serverFailureMessage
                                        ? 'try_again'.tr(context)
                                        : state.message, context: context);
                              }
                            },
                            builder: (context, state) {
                              if (state
                                  is LoadingUpdateProfileManageAccountState) {
                                return const Center(
                                  child: CircularProgressIndicator(
                                    color: AppColors.appColor,
                                  ), // Customize as needed
                                );
                              } else if (state
                                  is LoadingDeleteAccountManageAccountState) {
                                return const Center(
                                  child: CircularProgressIndicator(
                                    color: AppColors.appColor,
                                  ), // Customize as needed
                                );
                              } else {
                                return Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Expanded(
                                      child: CustomButton(
                                        text: 'save'.tr(context),
                                        onPressed: () {
                                          final isValid =
                                              _formKey.currentState!.validate();
                                          if (isValid) {
                                            if (ProfileHelper
                                                    .selectedCityItem ==
                                                null) {
                                              SnackBarMessage()
                                                  .showErrorSnackBarMessage(
                                                      message: "must_enter_city"
                                                          .tr(context),
                                                      context: context);
                                            } else if (ProfileHelper
                                                    .selectedItem ==
                                                null) {
                                              SnackBarMessage()
                                                  .showErrorSnackBarMessage(
                                                      message:
                                                          "must_enter_gender"
                                                              .tr(context),
                                                      context: context);
                                              return;
                                            } else {
                                              _showConfirmationDialog(context);
                                            }
                                          }
                                        },
                                      ),
                                    ),
                                    const SizedBox(width: 4),
                                    Expanded(
                                      child: CustomButton(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                AppColors.redColor),
                                        text: 'delete_account'.tr(context),
                                        onPressed: () {
                                          _showDeleteAccountDialog(context);
                                        },
                                      ),
                                    )
                                  ],
                                );
                              }
                            },
                          )),
                    ],
                  );
                } else if (state is ErrorProfileState) {
                  return ErrorDataWidget(
                    text: state.message == offlineFailureMessage
                        ? 'no_internet'.tr(context)
                        : state.message == serverFailureMessage
                        ? 'try_again'.tr(context)
                        : state.message,
                    onTap: () {
                      BlocProvider.of<ProfileBloc>(context)
                          .add(const ProfileUserEvent());
                    },
                  );
                } else {
                  return const SizedBox();
                }
              },
            ),
          )),
        ],
      ),
    );
  }

  void _showConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'edit_profile_message'.tr(context),
            style: AppTextStyle.titleBoldDark,
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                _submitData();
              },
              child: Text(
                'confirm'.tr(context),
                style: AppTextStyle.subtitleBold,
              ),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('cancel'.tr(context),
                  style: AppTextStyle.subtitleBold
                      .copyWith(color: AppColors.redColor)),
            ),
          ],
        );
      },
    );
  }

  void _submitData() {
    final updatedCustomer = PostCustomerProfile(
        firstname: ProfileHelper.firstNameController.text,
        lastname: ProfileHelper.lastNameController.text,
        mobile: ProfileHelper.phoneController.text,
        store: ProfileHelper.shopNameController.text,
        area: ProfileHelper.districtController.text,
        address: ProfileHelper.addressController.text,
        gender: ProfileHelper.selectedItem.toString(),
        city: ProfileHelper.selectedCityItem!.code,
        locale: LocalStorageService().currentLang ?? "");
    BlocProvider.of<ManageAccountBloc>(context)
        .add(UpdateProfileUserManageAccountEvent(customer: updatedCustomer));
  }

  void _showDeleteAccountDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'delete_account_message'.tr(context),
            style: AppTextStyle.titleBoldDark,
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                BlocProvider.of<ManageAccountBloc>(context)
                    .add(const DeleteAccountUserManageAccountEvent());
              },
              child: Text(
                'confirm'.tr(context),
                style: AppTextStyle.subtitleBold,
              ),
            ),
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('cancel'.tr(context),
                  style: AppTextStyle.subtitleBold
                      .copyWith(color: AppColors.redColor)),
            ),
          ],
        );
      },
    );
  }
}
