import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/custom_textfield.dart';
import 'package:jasmingmbh/core/widgets/loading_widget.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/update_password_bloc/update_password_bloc.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({super.key});

  @override
  State<ChangePasswordPage> createState() => ChangePasswordPageState();
}

class ChangePasswordPageState extends State<ChangePasswordPage> {
  final passwordController = TextEditingController();
  final passwordFocusNode = FocusNode();
  late bool _isObscure;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _isObscure = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
        backgroundColor: AppColors.backgroundColor,
        title: Text('change_password'.tr(context),
            style: AppTextStyle.titleBoldDark),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: BlocConsumer<UpdatePasswordBloc, UpdatePasswordState>(
          listener: (context, state) {
            if (state is LoadedUpdatePasswordState) {
              if (state.updatePassword.status != 200) {
                SnackBarMessage().showErrorSnackBarMessage(
                    message: state.updatePassword.msg ?? "", context: context);
              } else {
                SnackBarMessage().showSuccessSnackBarMessage(
                    message: state.updatePassword.msg ?? "", context: context);
                passwordController.text = '';
              }
            } else if (state is ErrorUpdatePasswordState) {
              SnackBarMessage().showErrorSnackBarMessage(
                  message: state.message == offlineFailureMessage
                      ? 'no_internet'.tr(context)
                      : state.message == serverFailureMessage
                          ? 'try_again'.tr(context)
                          : state.message,
                  context: context);
            }
          },
          builder: (context, state) {
            if (state is LoadingUpdatePasswordState) {
              return const LoadingWidget();
            }
            return Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });
                        },
                        child: _isObscure
                            ? const Icon(
                                Icons.visibility,
                                color: AppColors.iconColor,
                                size: 25,
                              )
                            : const Icon(
                                Icons.visibility_off,
                                color: AppColors.iconColor,
                                size: 25,
                              ),
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'new_password'.tr(context),
                        style: AppTextStyle.titleDark,
                      )
                    ],
                  ),
                  CustomTextFormField(
                    label: '***********',
                    controller: passwordController,
                    focusNode: passwordFocusNode,
                    isObscureText: _isObscure,
                    validator: (value) =>
                        value!.isEmpty ? "cant_be_empty".tr(context) : null,
                  ),
                  const SizedBox(height: 50),
                  SizedBox(
                      width: double.infinity,
                      child: CustomButton(
                          text: "confirm".tr(context),
                          onPressed: () {
                            final isValid = _formKey.currentState!.validate();
                            if (isValid) {
                              showDialog<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text(
                                      'password_message'.tr(context),
                                      style: AppTextStyle.titleBoldDark,
                                    ),
                                    actions: [
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                          BlocProvider.of<UpdatePasswordBloc>(
                                                  context)
                                              .add(UpdatePasswordUserEvent(
                                                  password:
                                                      passwordController.text));
                                        },
                                        child: Text(
                                          'confirm'.tr(context),
                                          style: AppTextStyle.subtitleBold,
                                        ),
                                      ),
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        child: Text('cancel'.tr(context),
                                            style: AppTextStyle.subtitleBold
                                                .copyWith(
                                                    color: AppColors.redColor)),
                                      ),
                                    ],
                                  );
                                },
                              );
                            }
                          }))
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
