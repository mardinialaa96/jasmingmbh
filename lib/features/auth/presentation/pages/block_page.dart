import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/login_page.dart';
import 'package:jasmingmbh/features/cart/presentation/bloc/cart_bloc.dart';
import 'package:lottie/lottie.dart';

class BlockPage extends StatelessWidget{
  const BlockPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Lottie.asset(Assets.wifiErrorLottie,width: 300,height: 300,fit: BoxFit.fill)),
          Text("block_user_message".tr(context),
              style: AppTextStyle.subtitleBold,textAlign: TextAlign.center),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: SizedBox(
        width: 180,
        child: CustomButton(
            text: "ok".tr(context), onPressed: () {
          LocalStorageService().isLoggedIn = null;
          LocalStorageService().isFirstName = null;
          LocalStorageService().isLastName = null;
          BlocProvider.of<CartBloc>(context).add(ClearCart());
          Navigator.of(context,rootNavigator: true).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => const LogInPage()), (route) => false);
        }),
      )
    );
  }

}