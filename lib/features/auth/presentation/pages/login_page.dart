import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jasmingmbh/core/app_colors.dart';
import 'package:jasmingmbh/core/app_localization.dart';
import 'package:jasmingmbh/core/app_text_style.dart';
import 'package:jasmingmbh/core/assets/assets.dart';
import 'package:jasmingmbh/core/local_storage/shared_preferences_service.dart';
import 'package:jasmingmbh/core/strings/failures.dart';
import 'package:jasmingmbh/core/utils/agent.dart';
import 'package:jasmingmbh/core/utils/snack_bar_message.dart';
import 'package:jasmingmbh/core/widgets/custom_button.dart';
import 'package:jasmingmbh/core/widgets/custom_textfield.dart';
import 'package:jasmingmbh/core/widgets/global_function.dart';
import 'package:jasmingmbh/core/widgets/loading_widget.dart';
import 'package:jasmingmbh/features/auth/domain/entities/post_login_parametesd.dart';
import 'package:jasmingmbh/features/auth/presentation/bloc/login_bloc/login_bloc.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/forget_password_page.dart';
import 'package:jasmingmbh/features/auth/presentation/pages/signup_page_info_one.dart';
import 'package:jasmingmbh/features/home_category/presentation/pages/home_tabs.dart';
import 'package:jasmingmbh/features/language/local_cubit.dart';
import 'package:jasmingmbh/features/update_app/presentation/pages/update_app_page.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({super.key});

  @override
  State<LogInPage> createState() => LogInPageState();
}

class LogInPageState extends State<LogInPage> {
  final _formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final emailFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();

  late bool _isObscure;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  String allTopic = 'all';

  @override
  void initState() {
    super.initState();
    _isObscure = true;
    subscribeToTopic(allTopic);
  }

  Future<void> subscribeToTopic(String topic) async {
    await _firebaseMessaging.subscribeToTopic(topic);
  }

  String? _validateEmail(String value) {
    // Regular expression for validating an email address
    String emailPattern = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$';
    RegExp regExp = RegExp(emailPattern);

    if (value.isEmpty) {
      return 'empty_email_message'.tr(context);
    } else if (!regExp.hasMatch(value)) {
      return 'valid_email_message'.tr(context);
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SafeArea(child: _buildBody()),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Text.rich(
                textAlign: TextAlign.center,
                TextSpan(
                  children: [
                    TextSpan(
                      text: 'Powered by ',
                      style: AppTextStyle.titleDark,
                    ),
                    TextSpan(
                      text: 'Vision',
                      style: AppTextStyle.drawerText
                          .copyWith(color: AppColors.appColor),
                      recognizer: TapGestureRecognizer()
                        ..onTap = GlobalFunction.launchURL,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: BlocConsumer<LoginBloc, LoginState>(
        listener: (context, state) {
          if (state is LoadedUserInfoState) {
            if (state.userInfo.status != 200) {
              SnackBarMessage().showErrorSnackBarMessage(
                  message: state.userInfo.msg ?? "", context: context);
            } else {
              SnackBarMessage().showSuccessSnackBarMessage(
                  message: state.userInfo.msg ?? "", context: context);
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) =>
                      const UpdateAppPage(child: HomeTabs())));
            }
          } else if (state is ErrorLoginState) {
            SnackBarMessage().showErrorSnackBarMessage(
                message: state.message == offlineFailureMessage
                    ? 'no_internet'.tr(context)
                    : state.message == serverFailureMessage
                        ? 'try_again'.tr(context)
                        : state.message,
                context: context);
          }
        },
        builder: (context, state) {
          if (state is LoadingLoginState) {
            return const LoadingWidget();
          }
          return SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      PopupMenuButton<String>(
                        itemBuilder: (context) => ['de', 'ar', 'en'].map((e) {
                          return PopupMenuItem<String>(
                            value: e,
                            child: ListTile(
                              title: Text(
                                  e == 'de'
                                      ? 'Deutsch'
                                      : e == 'ar'
                                          ? 'العربية'
                                          : 'English',
                                  style: AppTextStyle.drawerText),
                            ),
                          );
                        }).toList(),
                        child: const SizedBox(
                          width: 50,
                          height: 50,
                          child: Icon(
                            Icons.language,
                            color: AppColors.appColor,
                            size: 30,
                          ),
                        ),
                        onSelected: (newValue) {
                          context.read<LocalCubit>().changeLanguage(newValue);
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 4,
                    child: Image.asset(Assets.logo, fit: BoxFit.contain),
                  ),
                  const SizedBox(height: 50),
                  Row(
                    children: [
                      const Icon(
                        Icons.email,
                        color: AppColors.iconColor,
                        size: 25,
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'email'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  CustomTextFormField(
                    controller: emailController,
                    focusNode: emailFocusNode,
                    type: TextInputType.emailAddress,
                    validator: (value) => _validateEmail(value!),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });
                        },
                        child: _isObscure
                            ? const Icon(
                                Icons.visibility,
                                color: AppColors.iconColor,
                                size: 25,
                              )
                            : const Icon(
                                Icons.visibility_off,
                                color: AppColors.iconColor,
                                size: 25,
                              ),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          'password'.tr(context),
                          style: AppTextStyle.titleDark,
                        ),
                      )
                    ],
                  ),
                  CustomTextFormField(
                    label: '***********',
                    controller: passwordController,
                    focusNode: passwordFocusNode,
                    isObscureText: _isObscure,
                    validator: (value) =>
                        value!.isEmpty ? "cant_be_empty".tr(context) : null,
                  ),
                  const SizedBox(height: 25),
                  InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const ForgetPasswordPage())),
                    child: Text('forget_password'.tr(context),
                        textAlign: TextAlign.center,
                        style: AppTextStyle.subtitleBold.copyWith(
                            color: AppColors.secondaryTextColor,
                            decoration: TextDecoration.underline,
                            decorationColor: AppColors.secondaryTextColor)),
                  ),
                  const SizedBox(height: 30),
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: CustomButton(
                      text: 'login'.tr(context),
                      onPressed: () async {
                        final isValid = _formKey.currentState!.validate();
                        String device = await Agent().agents();
                        if (isValid) {
                          onPressLoginButton(device);
                        }
                      },
                    ),
                  ),
                  const SizedBox(height: 25),
                  Text(
                    'no_account'.tr(context),
                    style: AppTextStyle.subtitleBold,
                  ),
                  const SizedBox(width: 8),
                  InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const SignUpPageInfoOne())),
                    child: Text('new_account'.tr(context),
                        style: AppTextStyle.subtitleBold.copyWith(
                            color: AppColors.secondaryTextColor,
                            decoration: TextDecoration.underline,
                            decorationColor: AppColors.secondaryTextColor)),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void onPressLoginButton(String device) {
    final PostLoginParameters postLoginParameters = PostLoginParameters(
        email: emailController.text,
        password: passwordController.text,
        locale: '${LocalStorageService().currentLang}',
        device: device);
    BlocProvider.of<LoginBloc>(context)
        .add(LoginUserEvent(postLoginParameters: postLoginParameters));
  }
}
